module.exports = function(config){
   var re = {}


   var svgCaptcha = require('svg-captcha');


   re.GetCaptcha = function(length) {
        if(!length) length = 8

		// generate random text of length 4
		var r = {}
		r.text = svgCaptcha.randomText(length);
		r.captcha = svgCaptcha(r.text);

		r.type = "image/svg+xml"

		return r
   }

   return re
}
