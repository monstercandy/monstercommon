

var assert = require("chai").assert

describe('simple', function() {    
    "use strict"

    it('get captcha without any config options', function() {

         var captcha = require( "../lib/index.js")()

         var t = captcha.GetCaptcha( 6 )
         assert.property(t, "text")
         assert.property(t, "captcha")
         assert.property(t, "type")
         assert.equal(t.text.length, 6)
    })

})
