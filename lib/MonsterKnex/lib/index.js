(function(){

  const moment = require("MonsterMoment");
  const dotq = require("MonsterDotq");

    var debug = false;
    var connCache = {}
    var pendingCallbacks = {}
    var lib = module.exports = function(config, configKey) {
        if(!configKey) configKey = "db"
        var db = config.get(configKey)
        if(!db) throw new Error(`Database (${configKey} node) is not found in the configuration file`)


        var settings = db
//settings.legacySinglePoolMode = true;

        if((settings.client == "sqlite3")&&
           (settings.connection))
        {

           if(!settings.pool)
               settings.pool = !settings.legacySinglePoolMode ? {min:0, max:5} : {};

           // the default is: settings.pool = {min:1, max:1};

           var newConnectionCounter = 0
           settings.pool.create= function (callback) {
              // console.error("!!!!!!!!!!!!!!!!!!!!! POOL CREATE")
              if(connCache[settings.connection.filename]) {
                if(debug) console.log("returning cached sqlite client instance")
                connCache[settings.connection.filename].connCounter++
                return callback(undefined, connCache[settings.connection.filename])
              }

              if(!pendingCallbacks[settings.connection.filename])
                 pendingCallbacks[settings.connection.filename] = []

              pendingCallbacks[settings.connection.filename].push(callback)
              if(pendingCallbacks[settings.connection.filename].length > 1) {
                 if(debug) console.log("pending callback saved")
                 return
              }

              if(debug) console.log("creating new instance")
              knex.client.acquireRawConnection()
              .tap(function(connection){

                 newConnectionCounter++
                 connCache[settings.connection.filename] = connection
                 connCache[settings.connection.filename].connCounter = 0
                 connection.__knexUid = '__knexUid'+newConnectionCounter

                 const fs = require("fs")
                 var dbFile = `${settings.connection.filename}`
                 Array(dbFile, dbFile+"-shm", dbFile+"-wal").forEach(fn=>{
                    fs.chmod(fn, settings.db_umask ? parseInt(settings.db_umask, 8) : 0600, function(err){})
                 })

                 if(settings.connection.busyTimeout){
                    if(debug) console.log("configuring busyTimeout", settings.connection.busyTimeout)
                    connection.configure("busyTimeout", settings.connection.busyTimeout)
                 }

                 if(Array.isArray(settings.autoExec)){
                    connection.serialize(function(){
                       var last
                       for(var i = 0; i < settings.autoExec.length; i++) {
                          var q = settings.autoExec[i]
                          if(debug) console.log("autoeexecuting query:", q)
                          var cb
                          if(i == settings.autoExec.length -1)
                             cb = ready
                          connection.run(q, cb)
                       }

                    })
                 }
                 else
                   ready()

                 if(debug) console.log("created", connection.__knexUid, "calling", pendingCallbacks[settings.connection.filename].length, "callback")


                 function ready(err){
                   pendingCallbacks[settings.connection.filename].forEach(callback=>{
                      connCache[settings.connection.filename].connCounter++
                      callback(err, connection)
                   })
                   pendingCallbacks[settings.connection.filename] = []

                 }

              })
           };

           settings.pool.destroy = function(connection){
             if(debug) console.log("destroying connection", connection)
             if(!connection) return

             if(debug) console.log("destroying connection", connCache[settings.connection.filename].connCounter)
             connCache[settings.connection.filename].connCounter--
             if(connCache[settings.connection.filename].connCounter <= 0) {
                if(debug) console.log("destroying sqlite3 client")
                delete connCache[settings.connection.filename]
                knex.client.destroyRawConnection(connection)
             }
           }

        }


        var knex = lib.Knex(settings);
        if(!settings.legacySinglePoolMode) {
            const bluePromise = knex.Promise;
            var transactionWaitList = [];
            var originalTransaction = knex.transaction;
            knex.transaction = function(callback){
                if(debug) console.log("!!! transaction was called");

                return new bluePromise(function(resolve,reject){

                    transactionWaitList.push({callback:callback, resolve: resolve, reject: reject});

                    // initial kickoff:
                    if(transactionWaitList.length == 1)
                       fireNextOne();

                });

                function fireNextOne(){
                      // are there any more transactions?...
                      if(transactionWaitList.length <= 0) return;

                      var stuff = transactionWaitList[0];
                      var oex;
                      return originalTransaction(stuff.callback)
                      .catch(ex=>{
                          // console.log("there was an exception!", ex.message)
                          oex = ex;
                      })
                      .then((returnValue)=>{
                         if(debug) console.log("!!! original transaction has finished");
                         transactionWaitList.shift();
                         fireNextOne();

                         if(oex) return stuff.reject(oex)
                         return stuff.resolve(returnValue);
                      })
                }

            }

        }

        return knex

    }

    lib.Knex = require("knex")

    lib.searchRoute = function(options){
       if(typeof options != "object") throw new Error("Invalid options");

       if(!options.table) throw new Error("Table name was not provided");
       if(!options.knex) throw new Error("knex was not provided");

       var vali = require("MonsterValidators").ValidateJs();

       const paginationParams = {
               "limit": {default: 100, numericality:{onlyInteger: true, strict: true, lessThanOrEqualTo: 100}},
               "offset": {default: {value:0}, isInteger:true},
               "order": {inclusion: options.orderFields || [], default: options.defaultOrder},
               "desc": {isBooleanLazy: true},
       };
       const paginationParamNames = Object.keys(paginationParams);
       const constraints = extend({}, options.likeConstraints, options.equalConstraints, paginationParams);

       if(options.defaultSearchFields) {
         constraints.filter = {isString: true};
       }

       if(options.timestampField) {
         constraints.notbefore = {isString: true};
         constraints.notafter = {isString: true};
       }

       var main = function(req){
           return main.logic(req.body.json)
             .then(re=>{
                return req.sendResponse(re);
             })
       }

       main.logic = function(in_data){
          var d;
          var re = {};
          return vali.async(in_data, constraints)
            .then(function(ad){

               d = ad;

               return queryParams(options.knex(options.table).count("*"), d)
            })
            .then(rows=>{
               re.count = rows[0]["count(*)"];

               
               var w = options.knex(options.table);
               w = options.selectFields ? w.select(options.selectFields) : w.select();
               w = w.limit(d.limit).offset(d.offset);
               if(d.order)
                 w = w.orderBy(d.order, d.desc?"desc":undefined);
               return queryParams(w, d);
            })
            .then(rows=>{


               re.rows = rows;
               return re;
            })        
       }

       return main;

       function queryParams(builder, d){

          var qStr = [options.defaultQueryFilter || "1"];
          var qArr = options.defaultQueryFilterParams;
          if(!qArr) qArr = [];
          if(!Array.isArray(qArr))
            qArr = [qArr];

          Object.keys(d).forEach(k=>{
              if(paginationParamNames.indexOf(k) > -1) return;

              var v = d[k];

              if(k == "notbefore") {
                  qStr.push(options.timestampField+">=?");
                  qArr.push(v);
                  return;
              }

              if(k == "notafter") {
                  qStr.push(options.timestampField+"<=?");
                  qArr.push(v);
                  return;
              }

              if(k == "filter") {

                  qStr.push("("+options.defaultSearchFields.map(x => x+" LIKE ?").join(" OR ")+")")
                  options.defaultSearchFields.forEach(x=>{
                     qArr.push("%"+v+"%");
                  })
                  
              }


              if((options.likeConstraints)&&(options.likeConstraints[k])) {
                  qStr.push(k+" LIKE ?");
                  qArr.push('%'+v+'%');
                  return;
              }

              if((options.equalConstraints)&&(options.equalConstraints[k])) {
                  qStr.push(k+"=?");
                  qArr.push(v);
                  return;
              }


              // this should not really happen

          })

          var qStr = qStr.join(" AND ");
          var x = builder.whereRaw(qStr, qArr);
          // console.log(x.toString())
          return x;
       }
    }

    // NOTE: knex already supports this, but it is broken with Sqlite
    lib.filterHash = function(filterHash, forInsert) {
          var sqlFilterParts = []
          var sqlFilterValues = []
          Object.keys(filterHash).forEach(k=>{

             var sign = "="
             var fk = k
             if(k.charAt(0) == "!"){
               sign = "!="
               fk = k.substring(1)
             }
             if(k.charAt(0) == "~"){
               sign = "LIKE";
               fk = k.substring(1);
               filterHash[k] = "%"+filterHash[k]+"%";
             }

             var filterPart = forInsert ? fk : `${fk} ${sign} ?`;
             sqlFilterParts.push(filterPart);
             sqlFilterValues.push(filterHash[k]);
          })

          var sqlFilterStr = sqlFilterParts.join(forInsert ? ", " : " AND ")
          var re = {sqlFilterStr: sqlFilterStr, sqlFilterParts:sqlFilterParts, sqlFilterValues:sqlFilterValues};

          if(forInsert)
             re.sqlValuesStr = Array(sqlFilterParts.length).fill("?").join(", ");

          return re;
      }

    lib.doCleanup = function(trx, rule){
         var days = rule.ageDays;
         if(!days) {
            console.error("No ageDays configured for rule, skipping", rule);
            return;
         }

         if(!rule.table) {
            console.error("Table not configured for rule, skipping", rule)
            return;
         }

         if(!rule.agePrefix) {
            console.error("No agePrfix configured for rule, skipping", rule)
            return;
         }

         var tsField = rule.agePrefix+"timestamp";
         var now = moment().add(-1*days, "days");
         var ts = now.toISOStringWithTimeZone();
         var whereRaw = tsField+" < ?"+(rule.rawFilter ? " AND "+rule.rawFilter : "");

         return trx(rule.table).whereRaw(whereRaw, [ts]).delete().return()

    }
    lib.installCleanup = function(knex, cleanupRules){
      if(!cleanupRules) return;

      const croner = require("Croner");

      var cspExpressions = Object.keys(cleanupRules);
      cspExpressions.forEach(csp=>{
         croner.schedule(csp, function(){
            var rules = cleanupRules[csp];
            if(rules.length <= 0) return;

            console.log("Executing", rules.length, "cleanup rules");
            return knex.transaction(trx=>{
               return dotq.linearMap({array:rules, action: function(rule){
                   return lib.doCleanup(trx, rule);
               }})
            })

         });
      });

      console.log("Database cleanup rules installed");

    }


})();
