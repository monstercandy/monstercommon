require( "../../MonsterDependencies")(__dirname)

var assert = require("chai").assert;


describe('cleanup', function() {
	var lib = require("../lib/index.js");

	it("invalid params (no table)", function(){
		var re = lib.doCleanup(undefined, {agePrefix: "l_", ageDays: 14});
		assert.isUndefined(re);
	})
	it("invalid params (no ageDays)", function(){
		var re = lib.doCleanup(undefined, {table: "foo", agePrefix: "l_"});
		assert.isUndefined(re);
	})
	it("invalid params (no agePrefix)", function(){
		var re = lib.doCleanup(undefined, {table: "foo", ageDays: 14});
		assert.isUndefined(re);
	})

	it("should generate the proper query", function(){
		const finalResult = "fooobar";
		var rule = {table: "loginlog", agePrefix: "l_", ageDays: 14, rawFilter: "l_successful=0 AND l_subsystem!='accountapi'"};
		var re = lib.doCleanup(function(table){
			assert.equal(table, rule.table)
			return {
				whereRaw: function(q, p) {
					assert.equal(q, "l_timestamp < ? AND l_successful=0 AND l_subsystem!='accountapi'")
					assert.ok(Array.isArray(p));
					assert.equal(p.length, 1);

					return {
						 delete: function(){
						 	return {
						 		return: function(){
						 			return finalResult;
						 		}
						 	}
						 }
					}
				}
			}
		}, rule);

		assert.equal(re, finalResult);

	})


})
