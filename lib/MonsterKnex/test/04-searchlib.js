require( "../../MonsterDependencies")(__dirname)

var assert = require("chai").assert;


describe('basic', function() {
	var lib = require("../lib/index.js");
	const databaseFile = "./test/test.db";
	var knex = lib({get: function(){
	  	  return {
	  	      "client": "sqlite3",
			  "connection": {
		  	    "filename": databaseFile
		 	    },
		        "useNullAsDefault": true,
	      };
	  }})

	it("basic full list", function(){
		var searcher = lib.searchRoute({
			knex: knex,
			table: "eventlog",
		})
		return searcher.logic({})
		  .then(rows=>{
		  	  assert.deepEqual(rows, {
		  	  	 count: 2,
		  	  	 rows: [
                     {
                     	"e_id": 1,
			            "e_something": "foobar",
			            "e_ts": "now",
			          },
                     {
                     	"e_id": 2,
			            "e_something": "foobar2",
			            "e_ts": "now",
			          }
		  	  	 ]
		  	  })
		  })
	});

	it("basic list with default filter", function(){
		var searcher = lib.searchRoute({
			knex: knex,
			table: "eventlog",
			defaultQueryFilter: "e_id=?",
			defaultQueryFilterParams: "2",
		})
		return searcher.logic({})
		  .then(rows=>{
		  	  assert.deepEqual(rows, {
		  	  	 count: 1,
		  	  	 rows: [
                     {
                     	"e_id": 2,
			            "e_something": "foobar2",
			            "e_ts": "now",
			          }
		  	  	 ]
		  	  })
		  })
	});

	it("filtering stuff (equality)", function(){
		var searcher = lib.searchRoute({
			knex: knex,
			table: "eventlog",
			selectFields: ["e_ts", "e_something"],
			equalConstraints: { "e_something": {presence: true, isString: true} }
		})
		return searcher.logic({e_something: "foobar"})
		  .then(rows=>{
		  	  assert.deepEqual(rows, {
		  	  	 count: 1,
		  	  	 rows: [
                     {
			            "e_something": "foobar",
			            "e_ts": "now",
			          }
		  	  	 ]
		  	  })
		  })
	});

	it("filtering stuff (like)", function(){
		var searcher = lib.searchRoute({
			knex: knex,
			table: "eventlog",
			selectFields: ["e_ts", "e_something"],
			likeConstraints: { "e_something": {presence: true, isString: true} }
		})
		return searcher.logic({e_something: "foobar"})
		  .then(rows=>{
		  	  assert.deepEqual(rows, {
		  	  	 count: 2,
		  	  	 rows: [
                     {
			            "e_something": "foobar",
			            "e_ts": "now",
			          },
                     {
			            "e_something": "foobar2",
			            "e_ts": "now",
			          }
		  	  	 ]
		  	  })
		  })
	});

	it("ordering stuff", function(){
		var searcher = lib.searchRoute({
			knex: knex,
			table: "eventlog",
			selectFields: ["e_ts", "e_something"],
			orderFields: ["e_something"],
			likeConstraints: { "e_something": {presence: true, isString: true} }
		})
		return searcher.logic({e_something: "foobar", order: "e_something", desc: true})
		  .then(rows=>{
		  	  assert.deepEqual(rows, {
		  	  	 count: 2,
		  	  	 rows: [
                     {
			            "e_something": "foobar2",
			            "e_ts": "now",
			          },
                     {
			            "e_something": "foobar",
			            "e_ts": "now",
			          },
		  	  	 ]
		  	  })
		  })
	});

})
