require( "../../MonsterDependencies")(__dirname)

var assert = require("chai").assert;


describe('basic', function() {
	var lib = require("../lib/index.js");

	it("all features in one test", function(){
		var re = lib.filterHash({a:"1", "!b": "2", "~c": 3});
		assert.deepEqual(re,  {sqlFilterStr: 'a = ? AND b != ? AND c LIKE ?',
  sqlFilterParts: [ 'a = ?', 'b != ?', 'c LIKE ?' ],
  sqlFilterValues: [ '1', '2', '%3%' ] });
	});

	it("and the new insert feature", function(){
		var re = lib.filterHash({a:"1", "b": "2", "c": 3}, true);
		// console.log(re);
		assert.deepEqual(re,  {sqlFilterStr: 'a, b, c',
  sqlFilterParts: [ 'a', 'b', 'c' ],
  sqlFilterValues: [ '1', '2', 3 ],
  sqlValuesStr: "?, ?, ?",
   });
	});

})
