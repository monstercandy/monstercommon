
var exports = module.exports = {}

exports.passwordEnforcements = {presence: true, isString: {ascii:true, trim:true}, length: { "minimum": 6}}


 exports.WebhostingTemplateValidators = {
        "t_name": {isString: {strictName: true}},

        "t_storage_max_quota_hard_mb": { isInteger: {greaterThanOrEqualTo:0}, },
        "t_storage_max_quota_soft_mb": {isInteger: {greaterThanOrEqualTo:0}, },
        "t_domains_max_number_attachable": { isInteger: {greaterThanOrEqualTo:0}, },

        "t_git_max_repos": { isInteger: {greaterThanOrEqualTo:0}, },
        "t_git_max_branches": { isInteger: {greaterThanOrEqualTo:0}, },

        "t_email_max_number_of_accounts": { isInteger: {greaterThanOrEqualTo:0}, },
        "t_email_max_number_of_aliases": { isInteger: {greaterThanOrEqualTo:0}, },
        "t_email_max_number_of_bccs": { isInteger: {greaterThanOrEqualTo:0}, },

        "t_ftp_max_number_of_accounts": { isInteger: {greaterThanOrEqualTo:0}, },
        "t_awstats_allowed": {isBooleanLazy: true, },
        "t_cron_max_entries": { isInteger: {greaterThanOrEqualTo:0}, },

        "t_allowed_dynamic_languages": { inclusionCombo: [
           "ssh", "shell", "webshell","php","python","nodejs",".net","ruby","java", "memcache",
        ] },
        "t_max_container_apps": { isInteger: {greaterThanOrEqualTo:0}, },

        "t_webapp_email_quota_policy": { isString: true },

        "t_bandwidth_limit": {isSimpleObject: true},

        "t_max_number_of_firewall_rules": { isInteger: {greaterThanOrEqualTo:0}, },

        "t_db_max_number_of_databases": { isInteger: {greaterThanOrEqualTo:0}, },
        "t_db_max_databases_size_mb": { isInteger: {greaterThanOrEqualTo:0}, },
        "t_db_max_number_of_users": { isInteger: {greaterThanOrEqualTo:0}, },
        "t_db_keep_backup_for_days": { isInteger: {greaterThanOrEqualTo:0}, },

        "t_x509_certificate_external_allowed": { isBooleanLazy: true, },
        "t_x509_certificate_letsencrypt_allowed": { isBooleanLazy: true, },

        "t_installatron_allowed": {isBooleanLazy: true, },

        "t_public": {isBooleanLazy: true, },
      }
exports.forcePresence = function(hash, objectEmptinessAllowed) {
   var re = simpleCloneObject(hash)
   Object.keys(re).forEach(q=>{
       if((objectEmptinessAllowed)&&(re[q].isSimpleObject))
         return;
       re[q].presence = true;
   })
   return re
}

exports.array = function() { return require("./array.js") }

exports.IsValidIPv4 = function(ip) {
  return ip.match(/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/)
}

exports.IsNumericString = function(n){
  if(typeof n == "number")
     n = ""+n;

  if(typeof n == "string") {
     return n.match(/^[0-9]+$/) ? true : false;
  }
  else
    return false;
}

exports.IsValidIPv6 = function(ip) {
  var Address6 = require('ip-address').Address6;

  var address = new Address6(ip);

  if(!address.isValid()) return false

  var re = address.toUnsignedByteArray()
  while(re.length < 16) {
    re = [0].concat(re);
  }
  return re
}

exports.IsValidHostWildcard = function(host) {
  if(host == "*")
    return true
  return host.match(/^(\*\.)?(?:[a-z0-9_](?:[a-z0-9-]{0,61}[a-z0-9])?\.)*([a-z0-9_][a-z0-9-]{0,61}[a-z0-9]|[a-z0-9])$/)
}
exports.TurnIntoValidHostWildcard = function(host) {
  if(host == "*")
    return host
  host = host.toLowerCase()
  if(!host.match(/^(\*\.)?(?:[a-z0-9_](?:[a-z0-9-]{0,61}[a-z0-9])?\.)*([a-z0-9_][a-z0-9-]{0,61}[a-z0-9]|[a-z0-9])$/))
     return false
   return host
}
exports.isSimpleUrl = function(url) {

  var myRegexp = new RegExp(/^([^:]+:\/\/[^\/]+)\/?$/)

  var match = myRegexp.exec(url);
  if(match) return match[1]

  return false
}
exports.IsValidHost = function(host) {
  return host.match(/^(?:[a-z0-9_](?:[a-z0-9-]{0,61}[a-z0-9])?\.)*([a-z0-9_][a-z0-9-]{0,61}[a-z0-9]|[a-z0-9])$/)
}
exports.TurnIntoValidHost = function(host) {
  host = host.toLowerCase()
  if(!host.match(/^(?:[a-z0-9_](?:[a-z0-9-]{0,61}[a-z0-9])?\.)*([a-z0-9_][a-z0-9-]{0,61}[a-z0-9]|[a-z0-9])$/))
     return false
  return host
}
exports.IsValidDomain = function(dom) {
  return dom.match(/^(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]$/)
}
exports.IsValidDomainFile = function(dom) {
  return dom.match(/^(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]\.(z|d)$/)
}
exports.LooksValidDataFile = function(dom) {
  return dom.match(/\.(z|d)$/)
}
exports.IsString = function(s) {
   return 'string' === typeof s
}
exports.IsStrictName = function(s) {
   if(!exports.IsString(s)) return false
   return s.match(/^[a-zA-Z0-9\-_]+$/)
}
exports.IsAsciiString = function(str) {
   if(!exports.IsString(str)) return false;
   for (var i = 0, len = str.length; i < len; i++) {
      var d = str.charCodeAt(i)
      if((d<32)||(d > 126))
        return false;
   }
   return true
}
exports.IsLowerCase = function(str) {
   if(!exports.IsString(str)) return false;

   return str == str.toLowerCase()
}
exports.IsUpperCase = function(str) {
   if(!exports.IsString(str)) return false;

   return str == str.toUpperCase()
}
exports.StringHasHexaDecimalCharactersOnly = function(str) {
   if(!exports.IsString(str)) return false;

   return str.match(/^[a-fA-F0-9]*$/)
}

exports.IsDefined = function(s) {
   return ('undefined' !== typeof s && s !== null)
}

exports.ValidateJs = function(){
    var vali = require("validate.js")




  if(!vali.origAsync) {
      vali.origAsync = vali.async


    vali.validators.isValidUserId = function(value, options, key, attributes) {
       if(!value) return
       if(value === "0") return "invalid user id"

       var pre = vali.validators.isString(value, {lazy: true}, key, attributes)
       if(pre) return pre

        if((!vali.isObject(options))||(!options.info))
          return

        // we also need to validate against a remote userdb

        return options.info.GetInfo(value)
              .then((account)=>{
                 if(options.returnLookup)
                   attributes[key] = account
                 return Promise.resolve()
              })
              .catch(ex=>{
                 return Promise.resolve(ex.message || ex)
              })

    }

    vali.validators.isMajorPhpVersion = function(value, options, key, attributes) {

      if(!exports.IsDefined(value))
          return
      if(!exports.IsString(value))
          return "is not a string"

      if((!value)&&(vali.isObject(options))&&(options.emptyStringAccepted))
         return

      var testAgainst = Array("test","5.3","5.4","5.5","5.6","7.0","7.1","7.2","7.3");

      if(testAgainst.indexOf(value) <= -1)
        return "invalid PHP major version"

    }

    vali.validators.isValidIPv4 = function(value, options, key, attributes) {
      if(value == "0/0") value ="0.0.0.0/0"
      if((!options)||(options === true)) options = {} // if options is true, the subproperties wouldnt be assignable

      if(!options.rejectIpsFromRange) {
         // https://en.wikipedia.org/wiki/Reserved_IP_addresses
         options.rejectIpsFromRange = ["192.18.","192.19.","192.0.0.","192.168.","10.","127."]
         for(var i= 16; i <= 31; i++)
            options.rejectIpsFromRange.push(`172.${i}.`)
         for(var i= 224; i <= 239; i++)
            options.rejectIpsFromRange.push(`${i}.`)
         for(var i= 64; i <= 127; i++)
            options.rejectIpsFromRange.push(`100.${i}.`)
      }

      if(!exports.IsDefined(value))
          return
      if(!exports.IsString(value))
          return "is not a string"

      var ip = value
      if(options.canBeRange) {
           if(options.typicalRanges)
              options.onlyRanges = ["16","24","32"]

           var r = /^(.+)\/(\d+)$/
           var m = r.exec(value)
           if(m) {
              ip = m[1]
              range = m[2]
              if((range < 0)||(range > 32))
                return "invalid range"

              if((options.onlyRanges)&&(-1 >= options.onlyRanges.indexOf(range)))
                return "unsupported range"
           }
      }

      if(!exports.IsValidIPv4(ip))
        return "not a valid IPv4"

      if((ip.match(/^0\./))&&(!options.canBeZero))
        return "zero ip is not supported"

      if((ip.match(/\.255$/))&&(!options.canBeBroadcast))
        return "broadcast ip is not supported"

      var re
      options.rejectIpsFromRange.some(r=>{
         if(ip.substring(0, r.length) == r) {
            re = "ip from unsupported range"
            return true
         }
      })

      if(!re)
        attributes[key] = value

      return re
    }

    vali.validators.isString = function(value, options, key, attributes) {

      if(!exports.IsDefined(value)) {
          if(key in attributes){
             delete attributes[key]
          }
          return
      }

      if((!options)||(!vali.isObject(options))) options = {}


      if(!exports.IsString(value)) {
          if((options.lazy)&&(vali.isInteger(value)))
             value = attributes[key] = ""+value
          else
             return "is not a string"
      }

          if(options.trim)
              attributes[key] = value = value.trim()

          if(options.endsWith) {
             if(!value.endsWith(options.endsWith))
              return "does not end with "+options.endsWith
          }

          if(options.startsWith) {
             if(!value.startsWith(options.startsWith))
              return "does not start with "+options.startsWith
          }

          if(options.ascii) {
             if(!exports.IsAsciiString(value))
              return "invalid characters in the string"
          }

          if(options.strictName) {
             if(!exports.IsStrictName(value)) {
                if((options.emptyOk)&&(value === "")) {
                }
                else
                  return "invalid characters in the string"
             }

          }

          if(options.lowerCase) {
             if(!exports.IsLowerCase(value))
              return "not lower cased"
          }

          if(options.upperCase) {
             if(!exports.IsUpperCase(value))
              return "not upper cased"
          }

          if(options.hex) {
             if(!exports.StringHasHexaDecimalCharactersOnly(value))
              return "not a valid hex string"
          }

      if(!options.noXssFiltering) {
         const xssFilters = require('xss-filters');
         var newValue = xssFilters.inHTMLData(value)
         if(value != newValue)
           return "invalid characters in the input string"
      }


    };


    vali.validators.isSimpleFilename = function(value, options, key, attributes){
       return vali.validators.isPath(value, {lazy:true, maxPathLevels:1, minPathLevels:1,stripHeadingSlash:true,stripTailingSlash:true}, key, attributes)
    }
    vali.validators.isPath = function(value, options, key, attributes) {

      if(!exports.IsDefined(value))
          return
      if(!exports.IsString(value))
          return "is not a string"

      if(!vali.isObject(options)) options = {}

      if((value == "")&&(options.acceptEmptyString))
         return;

      value = value.trim()

      if(options.lazy) {
         value = "/" + value
      }

      if((!options.noPathCharacterRestrictions)&&(!exports.IsAsciiString(value)))
        return "invalid characters in the path"

      var prevValue = value
      while(true) {
        value = value.replace(/\/{2,}/, "/")
        value = value.replace("/./", "/")
        if(value == prevValue) break;
        prevValue = value
      }

      for(var q of ["/..","../", "\\..", "..\\"]) {
        if(value.indexOf(q) != -1)
           return "invalid characters in the path"

      }

      if(!options.noPathCharacterRestrictions) {
         if (!value.match(/^(\/[A-Za-z0-9\-._]*)+$/)) {
           console.error("Invalid path, not allowed characters", value);
           return "invalid path";
         }
      } else {
         if (!value.match(/^(\/.*)+$/)) {
           console.error("Invalid path, relaxed, how could this go wrong?", value);
           return "invalid path"
         }
      }



      if(!value.match(/\/$/)) value += "/"

        // options.minPathLevels
        if((options.minPathLevels) || (options.maxPathLevels)) {
            var count = (value.match(/\//g) || []).length;

            if(options.minPathLevels) {
                if(count < options.minPathLevels + 1)
                  return "path should have at least "+options.minPathLevels+" levels (/level1/level2/...)"
            }
            if(options.maxPathLevels) {
                if(count > options.maxPathLevels + 1)
                  return "path should have at most "+options.maxPathLevels+" levels (/level1/level2/...)"
            }
        }

        if(options.stripHeadingSlash) {
            value = value.substr(1)
        }

        if(options.stripTailingSlash) {
          value = value.substr(0,value.length-1)
        }

      attributes[key] = value

    };

    vali.validators.isFilename = extendValidator({lazy:true,minPathLevels:1,maxPathLevels:1,stripHeadingSlash:true,stripTailingSlash:true}, "isPath")

    vali.validators.isObject = function(value, options, key, attributes) {
      if(!options) options = {};

      if(!exports.IsDefined(value))
        return;
      var t = typeof value;
      if(
        ((t == "string")&&((options.acceptString)||(options.acceptScalar)))
         ||
        ((t == "number")&&(options.acceptScalar))
        )
      {
        value = {data:value};
        attributes[key] = value;
        return;
      }
      if(t != "object") return "not an object";
      if((Array.isArray(value)) && (!options.acceptArray))
         return "array not allowed";

    }

    vali.validators.isSimpleObject = function(value, options, key, attributes) {
      if(!options) options = {};

      if(!exports.IsDefined(value))
          return
      var t = typeof value;
      if(
        ((t == "string")&&((options.acceptString)||(options.acceptScalar)))
         ||
        ((t == "number")&&((options.acceptString)||(options.acceptScalar)))
        )
      {
         value = {data:value};
      }
      if(Array.isArray(value))
          return "is not an object"
      if(!vali.isObject(value))
          return "is not an object"

      var err
      Object.keys(value).some(q=>{

        var av = value[q]

        if(Array.isArray(av)) {
            av.some(x=>{
              if((!exports.IsString(x)) && (!exports.IsInteger(x))) {
                 err =   "key '"+q+"'' is not simple"
                 return true
              }
            })

        }
        else
        if(vali.isObject(av)) {
          err = "key '"+q+"'' is not simple"
          return true
        }

      })

      if(!err)
        attributes[key] = value;

      return err

    }

    vali.validators.isArray = function(value, options, key, attributes) {

      value = attributes[key]

      if(!exports.IsDefined(value))
          return
      if(!Array.isArray(value))
          return "is not an array"

      if(!options) options =  {};

      if(options.lazyString) {
         for(let i = 0; i < value.length; i++) {
           var v = value[i];
           var t = typeof v;
           if(t == "string");
           else if(t == "number")
             value[i] = ""+v;
           else
             return "invalid value";
         }
      }

      if((options.minLength)&&(value.length < options.minLength))
          return "should have at least: "+options.minLength
      if((options.maxLength)&&(value.length < options.maxLength))
          return "should have at most: "+options.maxLength
    };


    vali.validators.isBooleanLazy = function(value, options, key, attributes) {

      if(!exports.IsDefined(value))
          return
      if(value === "") {
         value = attributes[key] = undefined
         return
      }
      if(vali.isBoolean(value))
          return
      if((value === 0)||(value === "0")||(value === "false")) {
          value = attributes[key] = false
          return
      }
      if((value === 1)||(value === "1")||(value === "true")) {
          value = attributes[key] = true
          return
      }

      return "invalid boolean"

    };
    vali.validators.isBoolean = function(value, options, key, attributes) {

      if(!exports.IsDefined(value))
          return
      if(!vali.isBoolean(value))
          return "is not a boolean"
    };
    vali.validators.isInteger = numera({onlyInteger: true, strict: true})
    vali.validators.isDecimal = numera({strict: true})

    function extendValidator(baseConstraint, baseValidatorName, sucCallback)
    {
       return function(value, options, key, attributes) {
          var attrs = simpleCloneObject(baseConstraint)
          if(vali.isObject(options))
            for(var q of Object.keys(options))
              attrs[q] = options[q]
          var re = vali.validators[baseValidatorName](value, attrs, key, attributes)
          if((!re)&&(sucCallback))
             re = sucCallback(value, attrs, key, attributes)
          return re

       }
    }
    function numera(baseConstraint)
    {
       return extendValidator(baseConstraint, "numericality", function(value,attrs,key,attributes){
          if(attrs.returnInt)
            attributes[key] = parseInt(value,10)
       })
    }

    vali.validators.objectToString = function(value, options, key, attributes) {

      if(!exports.IsDefined(value))
          return
      if((vali.isArray(value))||(vali.isObject(value))) {
          attributes[key] = JSON.stringify(value)
          return

      }

      attributes[key] = "" + value
    };

    vali.validators.default = function(value, options, key, attributes) {
      if(exports.IsDefined(value)) return

      var defValue = options
      if(vali.isObject(options)) defValue = options.value
      else
      if(options == "[[empty]]") defValue = ""
      else
      if(options == "[[]]") defValue = ""
      else
      if(options == "[[false]]") defValue = false
      else
      if(options == "[[0]]") defValue = 0

      value = attributes[key] = defValue

    };

    vali.validators.inclusionCombo = function(value, options, key, attributes) {
       if(!exports.IsDefined(value))
          return

       if(!Array.isArray(value)) {
          if(typeof value !== "string")
            return "invalid input"
          value = value.split(" ")
       }

       var re = {}
       for (var q of value) {
          if(0 > options.indexOf(q))
            return "invalid value: "+q
          re[q] = 1
       }

       value = attributes[key] = Object.keys(re)

    }
    vali.validators.isHostWildcard = function(value, options, key, attributes) {

       if(!exports.IsDefined(value))
          return


       var strict = false
       if((typeof options == "object")&&(options.strict))
         strict = true

       if(!strict)
         value = value.toLowerCase()

        if(!exports.IsValidHostWildcard(value))
          return "invalid host"

        attributes[key] = value
    }
    vali.validators.isSimpleWebUrl = function(value, options, key, attributes) {

       if(!exports.IsDefined(value))
          return

        if(!value.match(/^https?:\/\/(?:[a-z0-9_](?:[a-z0-9-]{0,61}[a-z0-9])?\.)*([a-z0-9_][a-z0-9-]{0,61}[a-z0-9]|[a-z0-9])(\/[a-zA-Z0-9_\-\.\?=%\/]*)*$/))
          return "invalid url"

        if(-1 < value.indexOf(".."))
          return "invalid url"
    }

    function commonDomainHostCanonical(re, value, options, key, attributes) {
       if(typeof options == "object"){
         if((options.canonical)||(options.enforceCanonical)) {
           if((value != re.d_host_canonical_name)&&(value != re.d_domain_canonical_name))
             return "not canonical"
         }

         if((options.canonical)||(options.returnCanonical)) {
            re = re.d_host_canonical_name || re.d_domain_canonical_name;
         }

       }

       attributes[key] = re
    }

    vali.validators.isHost = function(value, options, key, attributes) {

       if(!exports.IsDefined(value))
          return

       const punycode = require('punycode');

       var strict = false
       if((typeof options == "object")&&(options.strict))
         strict = true

       if(!strict)
         value = value.toLowerCase()

       var re = {}
       if(exports.IsValidHost(value, strict)) {

          re.d_host_canonical_name = value
          re.d_host_name = punycode.toUnicode(value)
       }else{
          re.d_host_name = value
          re.d_host_canonical_name =  punycode.toASCII(value)

          if(!exports.IsValidHost(re.d_host_canonical_name, strict))
            return "invalid host name"
       }

       if((typeof options == "object")&&(options.minLevel)) {
          var count = (re.d_host_canonical_name.match(/\./g) || []).length;
          if(count + 1 < options.minLevel) {
            return "invalid hostname (min leve: "+options.minLevel+")";
          }
       }

       return commonDomainHostCanonical(re, value, options, key, attributes)

    };


    vali.validators.isDomain = function(value, options, key, attributes) {

       if(!exports.IsDefined(value))
          return

       const punycode = require('punycode');

       var re = {}
       if(exports.IsValidDomain(value)) {

          re.d_domain_canonical_name = value
          re.d_domain_name = punycode.toUnicode(value)
       }else{
          re.d_domain_name = value
          re.d_domain_canonical_name =  punycode.toASCII(value)

          if(!exports.IsValidDomain(re.d_domain_canonical_name))
            return "invalid domain name"

       }

       return commonDomainHostCanonical(re, value, options, key, attributes)
    };

    vali.validators.mcEmail = function(value, options, key, attributes) {

       if(!exports.IsDefined(value))
          return

       if(!exports.IsString(value))
          return "not a string"

       value = value.toLowerCase()

       if((!options)||(typeof options != "object")) options = {}


       // NOTE: this email validation regexp does not comply to the standards purposefully. Some weird characters are not accepted by purpose.
       var re
       if(!value.match(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/))
          re = "invalid email"

       if(re) {
           // fallback cases in case they are allowed
           if(options.catchDomainAllowed) {
               // email: @domain.tld

               var m = /^@(.+)/.exec(value)
               if((m)&&(exports.IsValidHost(m[1])))
                  re = null

           }

           if(options.catchUserAllowed) {
               // email: postmaster

               if(exports.IsStrictName(value))
                  re = null
           }

       }

       if(!re)
          attributes[key] = value // passing through the lowercased version
       else
          return re

    }
    vali.validators.mcEmailList = function(value, options, key, attributes) {

       if(!exports.IsDefined(value))
          return

       if(!exports.IsString(value))
          return "not a string"

       var values = value.split(",").map(x=>{return x.trim()})
       var re = []
       var err
       values.some(av=>{
          var x = {}
          err = vali.validators.mcEmail(av, options, key, x)
          if(err)
            return true

          re.push(x[key])
       })

       attributes[key] = re

       return err
    }

    function arrayOfKindValidator(validatorFunction) {
       return function(value, options, key, attributes) {
          if(!value) return // if its not present...

          var err = vali.validators.isArray(value, options, key, attributes)
          if(err) return err

          var re = []
          value.some(function(m){
              attributes[key] = m
              err = validatorFunction(m, options, key, attributes)
              if(err)
                return true

              re.push(attributes[key])
          })

          attributes[key] = re

          return err
       }
    }

    vali.validators.isEmailArray= arrayOfKindValidator(vali.validators.mcEmail)
    vali.validators.isHostArray = arrayOfKindValidator(vali.validators.isHost)
    vali.validators.isPathArray = arrayOfKindValidator(vali.validators.isPath)
    vali.validators.isIPv4Array = arrayOfKindValidator(vali.validators.isValidIPv4)


    var moment = require("MonsterMoment")
    vali.extend(vali.validators.datetime, {
      // The value is guaranteed not to be null or undefined but otherwise it
      // could be anything.
      parse: function(value, options) {
        if(!value.match(/^\d{4}-\d\d-\d\d \d\d:\d\d:\d\d$/))
          return "invalid datetime (yyyy-mm-dd hh:mm:ss)"
        return +moment.utc(value);
      },
      // Input is a unix timestamp
      format: function(value, options) {
        var format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
        return moment.utc(value).format(format);
      }
    });


      vali.async = function(data, constraints, global) {
         if(!global) global = {fullMessages:false, data:data, constraints:{}}
         return vali.origAsync(data, constraints, global)
         .then(d=>{
            if(!Object.keys(global.constraints).length)
              return Promise.resolve(d)

            return vali.async(data, global.constraints)
              .then(sd=>{
                 extend(d, sd)
                 return Promise.resolve(d)
              })

         }).catch(function(err){

               const MError = require("MonsterError");
               console.error("Validation error:", err);
               var x = new MError("VALIDATION_ERROR", err);
               //console.log("!!!!!!!!!!!!", x); process.reallyExit();
               throw x;

         })

      }
  }



    return vali
}
