
const MError = require('MonsterError')


var re = module.exports = {}

re.SEPARATOR = " "

re.EnsureSingleRowReturned = function(rows, err_if_not_one, error_parameters){

	if(rows.length > 1)
        throw new MError("INTERNAL_ERROR", null, rows)

    if(rows.length <= 0)
    	throw new MError(err_if_not_one || "INTERNAL_ERROR", error_parameters)

    return rows[0]
}

re.ListToString = function(arr) {
	return re.SEPARATOR+arr.join(re.SEPARATOR)+re.SEPARATOR
}
re.StringToList = function(str) {
  var t = str.trim()
  if(!t) return []
	return t.split(re.SEPARATOR)
}
re.ForceEnumerableToList = function(v){
    if(!v) v = []
    if(typeof v === "string")
         v = re.StringToList(v)

    return v
}
re.ForceEnumerableToString = function(v){
   if(Array.isArray(v))
   	 v = re.ListToString(v)

   return v || ""
}
re.AddCleanupSupportForArray=function(rows){
   rows.Cleanup = function(){ return re.CleanupArray(rows) }
}
re.CleanupArray = function(rows){
  for(q of rows) {
    q.Cleanup()
  }
  return rows
}
