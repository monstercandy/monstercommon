require( "../../MonsterDependencies")(__dirname)

var Lib =require( "../lib/validators.js")
var vali = Lib.ValidateJs()

var assert = require("chai").assert

var arr = Lib.array()

describe("IsNumericString", function(){
	 Array("foo","0foo", "foo0", 3.14).forEach(n=>{
	        it('should return false with invalid numberic strings', function() {
	        	  assert.isFalse(Lib.IsNumericString(n));
	        })

	 })

	 Array("100", 0, 314).forEach(n=>{
	        it('should return true with good numberic strings', function() {
	        	  assert.isTrue(Lib.IsNumericString(n));
	        })

	 })

})

describe("nested validation", function(){
	        it('returning extra validations based on the type', function(done) {
	        	 var vali = Lib.ValidateJs()
			     vali.validators.validType = function(value, options, key, attributes, global) {
			     	if(!value) return
			     	extend(global.constraints, {url: {presence:true, isString:true}})
  		         }

		         vali.async(
		         	{"type":"url","url":"http://index.hu"},
		            {"type":{presence:true, inclusion:["url"], validType:true}}
		         )
  	             .then(d=>{
	           	    assert.deepEqual(d, {type:"url", url: "http://index.hu"})
	           	    done()
	             })
	             .catch(done)
		    })


	        it('when a subvalidator throws, it should be delegated', function(done) {
	        	 var vali = Lib.ValidateJs()
			     vali.validators.validType = function(value, options, key, attributes, global) {
			     	if(!value) return
			     	extend(global.constraints, {url: {presence:true, isInteger:true}})
  		         }

		         vali.async(
		         	{"type":"url","url":"http://index.hu"},
		            {"type":{presence:true, inclusion:["url"], validType:true}}
		         )
  	             .then(d=>{
  	             	throw new Error("it should have been an error")
	             })
	             .catch(err=>{
	             	assert.propertyVal(err, "message", "VALIDATION_ERROR")
	             	done()
	             })
	             .catch(done)
		    })


})

describe("string to list", function(){
	it("empty string should be empty array", function(){
		var x = arr.StringToList("")
		assert.deepEqual(x, [])

		x = arr.StringToList(" ")
		assert.deepEqual(x, [])
	})

	it("otherwise space should be the separator", function(){
		var x = arr.StringToList(" val1 val2 ")
		assert.deepEqual(x, ["val1","val2"])
	})

})


describe("isSimpleUrl", function(){
	"use strict"

    var ourl = 'http://index.hu'
    for(let aurl of [ourl, ourl+"/"]) {
	    it("should return "+ourl+" for "+aurl, function(done) {
	    	assert.equal(Lib.isSimpleUrl(aurl), ourl)
	    	done()
	    })

    }


})


describe("emails", function(){

	Array("some@valid.email", "SOME@VALID.EMAIL").forEach(email=>{
		it("valid email should be accepted: "+email, function(){
			var re = {}
			assert.isUndefined(vali.validators.mcEmail(email, null, "foobar", re))
			assert.equal(re.foobar, "some@valid.email")
		})
	})

	Array("some@inv|alid.email", "so/me@invalid.email", "some@inv/alid.email", "so|me@invalid.email",  "SOME@INVA|LID.EMAIL").forEach(email=>{
		it("invalid emails should be rejected: "+email, function(){
			assert.isString(vali.validators.mcEmail(email, null, "foobar", {}))
		})
	})


	Array("@valid.email", "@VALID.EMAIL").forEach(email=>{
		it("valid catch domain emails should be accepted: "+email, function(){
			var re = {}
			assert.isUndefined(vali.validators.mcEmail(email, {catchDomainAllowed: true}, "foobar", re))
			assert.equal(re.foobar, "@valid.email")
		})
	})

	Array("@in|valid.email", "@IN|VALID.EMAIL").forEach(email=>{
		it("invalid catch domain emails should be rejected: "+email, function(){
			assert.isString(vali.validators.mcEmail(email, {catchDomainAllowed: true}, "foobar", {}))
		})
	})

	Array("postmaster", "root", "ABUSE").forEach(email=>{
		it("valid generic email users should be accepted: "+email, function(){
			var re = {}
			assert.isUndefined(vali.validators.mcEmail(email, {catchUserAllowed: true}, "foobar", re))
			assert.equal(re.foobar, email.toLowerCase())
		})
	})

	Array("postmaster", "hostmaster", "root", "ABUSE").forEach(email=>{
		it("valid generic email users should be accepted: "+email, function(){
			var re = {}
			assert.isUndefined(vali.validators.mcEmail(email, {catchUserAllowed: true, catchDomainAllowed: true}, "foobar", re))
			assert.equal(re.foobar, email.toLowerCase())
		})
	})

	Array("ab|use", "r0`ot").forEach(email=>{
		it("invalid generic email users should be rejected: "+email, function(){
			assert.isString(vali.validators.mcEmail(email, {catchUserAllowed: true}, "foobar", {}))
		})
	})

	Array("email1@email1.hu, email2@email2.hu", "email1@email1.hu,email2@email2.hu").forEach(email=>{
		it("valid email lists should be accepted: "+email, function(){
			var re = {}
			assert.isUndefined(vali.validators.mcEmailList(email, null, "foobar", re))
			assert.deepEqual(re.foobar, ["email1@email1.hu", "email2@email2.hu"])
		})

		it("invalid emails should be rejected: "+email, function(){
			assert.isString(vali.validators.mcEmail(email, null, "foobar", {}))
		})
	})

})


describe("IPv4", function(){
	Array("123.123.123.123", "8.8.8.8","123.123.123.0").forEach(ip=>{
		it("valid ipv4 should be accepted: "+ip, function(){
			assert.isUndefined(vali.validators.isValidIPv4(ip, null, "foobar", {}))
		})
	})
	Array("1x3.123.123.123", " 123.123.123.123", "1.2.3.256", "123", "123.123.123.0/24", "0", "0/0").forEach(ip=>{
		it("invalid ipv4 should be rejected: "+ip, function(){
			assert.equal(vali.validators.isValidIPv4(ip, null, "foobar", {}), "not a valid IPv4")
		})

	})
	Array("1.1.1.255", "255.255.255.255").forEach(ip=>{
		it("broadcast addresses should be rejected by default: "+ip, function(){
			assert.equal(vali.validators.isValidIPv4(ip, null, "foobar", {}), "broadcast ip is not supported")
		})
	})
	Array("1.1.1.255", "255.255.255.255").forEach(ip=>{
		it("broadcast ip should be accepted if explicitly asked for it: "+ip, function(){
			assert.isUndefined(vali.validators.isValidIPv4(ip, {canBeBroadcast:true}, "foobar", {}))
		})
	})
	Array("0.0.0.0").forEach(ip=>{
		it("zero ip should be rejected by default: "+ip, function(){
			assert.equal(vali.validators.isValidIPv4(ip, null, "foobar", {}), "zero ip is not supported")
		})
	})
	Array("0.0.0.0").forEach(ip=>{
		it("zero ip should be accepted if explicitly asked for it: "+ip, function(){
			assert.isUndefined(vali.validators.isValidIPv4(ip, {canBeZero:true}, "foobar", {}))
		})
	})

	Array("192.168.1.1", "10.0.0.1", "127.0.0.1", "172.16.1.1", "172.31.1.1").forEach(ip=>{
		it("ip addresses from private ranges should be rejected by default: "+ip, function(){
			assert.equal(vali.validators.isValidIPv4(ip, null, "foobar", {}), "ip from unsupported range")
		})
	})
	Array("192.168.1.1", "10.0.0.1", "127.0.0.1", "172.16.1.1", "172.31.1.1").forEach(ip=>{
		it("ip addresses from private ranges should be accepted if explicitly asked for it: "+ip, function(){
			assert.isUndefined(vali.validators.isValidIPv4(ip, {rejectIpsFromRange:[]}, "foobar", {}))
		})
	})

	Array("1.1.1.1","123.123.123.0/24").forEach(ip=>{
		it("ipv4 with range should be accepted: "+ip, function(){
			assert.isUndefined(vali.validators.isValidIPv4(ip, {canBeRange: true}, "foobar", {}))
		})
	})
	Array("123.123.123.0/helo", "123.123.123.123/!", "123.123.123.123/-1").forEach(ip=>{
		it("ipv4 with range should be rejected if invalid syntax: "+ip, function(){
			assert.equal(vali.validators.isValidIPv4(ip, {canBeRange: true}, "foobar", {}), "not a valid IPv4")
		})
	})
	Array("123.123.123.0/35").forEach(ip=>{
		it("ipv4 with range should be rejected if invalid syntax: "+ip, function(){
			assert.equal(vali.validators.isValidIPv4(ip, {canBeRange: true}, "foobar", {}), "invalid range")
		})
	})
	Array("123.123.123.0/25").forEach(ip=>{
		it("ipv4 with range should be rejected if invalid syntax: "+ip, function(){
			assert.equal(vali.validators.isValidIPv4(ip, {canBeRange: true, typicalRanges:true}, "foobar", {}), "unsupported range")
		})
	})
	Array("1.1.1.1/25","123.123.123.0/24").forEach(ip=>{
		it("ipv4 with range should be accepted: "+ip, function(){
			assert.isUndefined(vali.validators.isValidIPv4(ip, {canBeRange: true, onlyRanges:["24","25"]}, "foobar", {}))
		})
	})
	Array("123.123.123.0/25").forEach(ip=>{
		it("ipv4 with range should be rejected if invalid syntax: "+ip, function(){
			assert.equal(vali.validators.isValidIPv4(ip, {canBeRange: true, onlyRanges:["32"]}, "foobar", {}), "unsupported range")
		})
	})

	        it('isIPv4Array validator', function(done) {
		         vali.async({"c":["1.1.1.1"]}, {"c":{isIPv4Array:true}})
			           .then(d=>{
			           	   assert.deepEqual(d.c, ["1.1.1.1"])
			           	   done()
			           })
			           .catch(done)
		    })

		    it('isIPv4Array validator rejection', function(done) {
		         vali.async({"c":["1.1.1.1", ["/invalid/"]]}, {"c":{isIPv4Array:true}})
			           .then(d=>{
			           	   throw new Error("Should have been an exception.")
			           })
			           .catch(function(ex){
			           	  if(ex.message != "VALIDATION_ERROR")
			           	  	 return done(ex)

			           	  // seems to be correct
			           	  done()
			           })
		    })
})


describe("valid User Id", function(){
	"use strict"

    for(let user_id of ["foobar", 1234, "u8weru8/wer=="]) {
	    it("generic strings should be ok: "+user_id, function(done) {
                vali.async({"c":user_id}, {"c":{isValidUserId: true}})
			           .then(d=>{
			           	   done()
			           })
			           .catch(done)

	    })
    }

	    it("should be good with remote validation", function(done) {
	    	 const expected_user_id = 12345
	    	 var infoOk = {
	    	 	GetInfo: function(user_id) {
	    	 		 assert.equal(user_id, expected_user_id)
	    	 		 return Promise.resolve("ok")
	    	 	}
	    	 }
                vali.async({"c":expected_user_id}, {"c":{isValidUserId: {info: infoOk}}})
			           .then(d=>{
			           	   done()
			           })
			           .catch(done)
	    })

	    it("should be good with remote validation (returning account)", function(done) {
	    	 const expected_user_id = 12345
	    	 const account = {u_id:12345, u_locale: "hu"}
	    	 var infoOk = {
	    	 	GetInfo: function(user_id) {
	    	 		 assert.equal(user_id, expected_user_id)
	    	 		 return Promise.resolve(account)
	    	 	}
	    	 }
                vali.async({"c":expected_user_id}, {"c":{isValidUserId: {returnLookup: true, info: infoOk}}})
			           .then(d=>{
			           	   assert.deepEqual(d.c, account)
			           	   done()
			           })
			           .catch(done)
	    })

	    it("should be rejected when remote db rejects", function(done) {
	    	 const expected_user_id = 12345
	    	 var infoOk = {
	    	 	GetInfo: function(user_id) {
	    	 		 assert.equal(user_id, expected_user_id)
	    	 		 return Promise.reject("not found")
	    	 	}
	    	 }
                vali.async({"c":expected_user_id}, {"c":{isValidUserId: {info: infoOk}}})
			           .then(d=>{
			           	   throw new Error("should have failed")
			           })
			           .catch(ex=>{
			           	  assert.propertyVal(ex, "message", "VALIDATION_ERROR")
			           	  assert.property(ex, "errorParameters")
			           	  assert.property(ex.errorParameters, "c")
			           	  assert.ok(Array.isArray(ex.errorParameters.c))
			           	  assert.equal(ex.errorParameters.c.length, 1)
			           	  assert.equal(ex.errorParameters.c[0], "not found")
			           	  done()
			           })
			           .catch(done)
	    })

	    it("should be rejected for zero", function(done) {
	    	 const expected_user_id = '0'
	    	 var infoOk = {
	    	 	GetInfo: function(user_id) {
	    	 		throw new Error("Should have not been called")
	    	 	}
	    	 }
                vali.async({"c":expected_user_id}, {"c":{isValidUserId: {info: infoOk}}})
			           .then(d=>{
			           	   throw new Error("should have failed")
			           })
			           .catch(ex=>{
			           	  assert.propertyVal(ex, "message", "VALIDATION_ERROR")
			           	  assert.property(ex, "errorParameters")
			           	  assert.property(ex.errorParameters, "c")
			           	  assert.ok(Array.isArray(ex.errorParameters.c))
			           	  assert.equal(ex.errorParameters.c.length, 1)
			           	  assert.equal(ex.errorParameters.c[0], "invalid user id")
			           	  done()
			           })
			           .catch(done)
	    })
})

describe("path/filename", function(){
	"use strict"

	   for(let p of ["something", "foo.something"]) {
		    it('simple filenames should be good: '+p, function(done) {
		         vali.async({"c":p}, {"c":{isSimpleFilename:true}})
			           .then(d=>{
			           	   assert.equal(d.c, p)
			           	   done()
			           })
			           .catch(done)
		    })
		}

	   for(let p of ["something", "foo.something", "*.something", "*", "/sdf/../sdf", "/../", "some\\..\\foobar", "some/..\\foobar", "/árvíz", "/|", "/;", "/ /"]) {
		    it('invalid path: '+p, function(done) {
		         vali.async({"c":p}, {"c":{isPath:true}})
			           .then(d=>{
			           	   throw new Error("Should have been an exception.")
			           })
			           .catch(function(ex){
			           	  if(ex.message != "VALIDATION_ERROR")
			           	  	 return done(ex)

			           	  // seems to be correct
			           	  done()
			           })
		    })
		}


		    it('dbimport parameters should be accepted', function(done) {
		    	 var data = {"filename":"mm_devmid_orig.sql","filemanTask":"b2fdeb2328fd46a027e3d204803e6df6"}
		         vali.async(
		         	data,
		         	{filename:{presence:true, isFilename: true}, filemanTask: {format:{pattern:/^[a-f0-9]+$/}}}
		         )
			           .then(d=>{
			           	   assert.deepEqual(d, data)
			           	   done()
			           })
			           .catch(done)
		    })

		    it('path without heading slash', function(done) {
		         vali.async({"c":"/some/"}, {"c":{isPath:{stripHeadingSlash:true}}})
			           .then(d=>{
			           	   assert.equal(d.c, "some/")
			           	   done()
			           })
			           .catch(done)
		    })

		    it('path without tailing slash', function(done) {
		         vali.async({"c":"/some/"}, {"c":{isPath:{stripTailingSlash:true}}})
			           .then(d=>{
			           	   assert.equal(d.c, "/some")
			           	   done()
			           })
			           .catch(done)
		    })

		    it('path without tailing slash', function(done) {
		         vali.async({"c":"/some/"}, {"c":{isPath:{stripHeadingSlash:true,stripTailingSlash:true}}})
			           .then(d=>{
			           	   assert.equal(d.c, "some")
			           	   done()
			           })
			           .catch(done)
		    })

		    it('invalid path due to required level', function(done) {
		         vali.async({"c":"/some/valid/"}, {"c":{isPath:{minPathLevels:3}}})
			           .then(d=>{
			           	   throw new Error("Should have been an exception.")
			           })
			           .catch(function(ex){
			           	  if(ex.message != "VALIDATION_ERROR")
			           	  	 return done(ex)

			           	  // seems to be correct
			           	  done()
			           })
		    })

		    it('valid path with the required level', function(done) {
		         vali.async({"c":"/some/valid////"}, {"c":{isPath:{minPathLevels:2}}})
			           .then(d=>{
			           	   assert.equal(d.c, "/some/valid/")
			           	   done()
			           })
			           .catch(done)
		    })


		    it('invalid path due to required level (max)', function(done) {
		         vali.async({"c":"/some/valid/"}, {"c":{isPath:{maxPathLevels:1}}})
			           .then(d=>{
			           	   throw new Error("Should have been an exception.")
			           })
			           .catch(function(ex){
			           	  if(ex.message != "VALIDATION_ERROR")
			           	  	 return done(ex)

			           	  // seems to be correct
			           	  done()
			           })
		    })

		    it('valid path with the required level (max)', function(done) {
		         vali.async({"c":"/some/////"}, {"c":{isPath:{maxPathLevels:1}}})
			           .then(d=>{
			           	   assert.equal(d.c, "/some/")
			           	   done()
			           })
			           .catch(done)
		    })

		    it('valid path with some /./ should be rewritten', function(done) {
		         vali.async({"c":"/some/././"}, {"c":{isPath:{maxPathLevels:1}}})
			           .then(d=>{
			           	   assert.equal(d.c, "/some/")
			           	   done()
			           })
			           .catch(done)
		    })


		    it('invalid path due to missing heading slash', function(done) {
		         vali.async({"c":"something"}, {"c":{isPath:true}})
			           .then(d=>{
			           	   throw new Error("should have failed")
			           })
			           .catch(x=>{
			           	  if(x.message == "VALIDATION_ERROR") return done()
			           	  	done(x)
			           })
		    })


		    it('invalid path (no heading slash) accepted due to lazy mode', function(done) {
		         vali.async({"c":"something"}, {"c":{isPath:{lazy:true}}})
			           .then(d=>{
			           	   assert.equal(d.c, "/something/")
			           	   done()
			           })
			           .catch(done)
		    })


	   validPathTests("/", ["/", "///"])
	   validPathTests("/some/valid/path/", ["/some/valid/path", "///some/valid/path", "/some/valid/path///"])

		    it('filename', function(done) {
		         vali.async({"c":"some-filename.tgz"}, {"c":{isFilename:true}})
			           .then(d=>{
			           	   assert.equal(d.c, "some-filename.tgz")
			           	   done()
			           })
			           .catch(done)
		    })

            it("isPathArray real life", function(){
   	            var constraints = {
                   pathes: {presence:true, isPathArray: {lazy:true, stripTailingSlash:true, stripHeadingSlash:true, noPathCharacterRestrictions: true}},
                };

            	return vali.async({pathes: ["pages", "README.md"]}, constraints)
            	  .then(d=>{
			          assert.deepEqual(d.pathes, ["pages", "README.md"]);
            	  })
            })

	        it('isPathArray validator', function(done) {
		         vali.async({"c":["/some/foobar/"]}, {"c":{isPathArray:true}})
			           .then(d=>{
			           	   assert.deepEqual(d.c, ["/some/foobar/"])
			           	   done()
			           })
			           .catch(done)
		    })

		    it('isPathArray validator rejection', function(done) {
		         vali.async({"c":["/some/valid/", ["/foo/"]]}, {"c":{isPathArray:true}})
			           .then(d=>{
			           	   throw new Error("Should have been an exception.")
			           })
			           .catch(function(ex){
			           	  if(ex.message != "VALIDATION_ERROR")
			           	  	 return done(ex)

			           	  // seems to be correct
			           	  done()
			           })
		    })


		    it('real life example #1', function(done) {
		         vali.async({"path":"/dbbackup/","mtime":"+3","delete":true},
		         	{
		         	  path:{
		         		 presence:true,
		         		 isPath: {noPathCharacterRestrictions: true},
		         	     format: {pattern: /^\/dbbackup\//}
		         	  }
		         	}
		         )
			           .then(d=>{
			           	   assert.equal(d.path, "/dbbackup/")
			           	   done()
			           })
			           .catch(done)
		    })



		    it('real life example #2', function(done) {
		         vali.async({"dst_full_path":"/dbbackup/bloodymary-20170311180546.sql.gz"},
		         	{
		         	  dst_full_path:{
		         		 presence:true,
		         	     format: {pattern: /^\/dbbackup\/.+/},
		         		 isPath: {noPathCharacterRestrictions: true},
		         	  }
		         	}
		         )
			           .then(d=>{
			           	   assert.equal(d.dst_full_path, "/dbbackup/bloodymary-20170311180546.sql.gz/")
			           	   done()
			           })
			           .catch(done)
		    })


		function validPathTests(shouldBe, testCases) {

		   for(let p of testCases) {
			    it('valid path: '+p, function(done) {
			         vali.async({"c":p}, {"c":{isPath:true}})
				           .then(d=>{
				           	   assert.equal(d.c, shouldBe)
				           	   done()
				           })
				           .catch(done)
			    })
			}
		}

})


describe("domain/host", function(){
	"use strict"

    it('host (no accents)', function(done) {

         vali.async({"str":"hostname"}, {"str":{presence: true, isHost:true}})
           .then(d=>{
               assert.equal(d.str.d_host_canonical_name, "hostname")
               done()
           })
           .catch(done)
    })

    it('host (uppercased, default settings)', function(done) {

         vali.async({"str":"HOSTNAME"}, {"str":{presence: true, isHost:true}})
           .then(d=>{
               assert.equal(d.str.d_host_canonical_name, "hostname")
               done()
           })
           .catch(done)
    })

    it('host (uppercased, strict settings)', function(done) {

         vali.async({"str":"HOSTNAME"}, {"str":{presence: true, isHost:{strict:true}}})
	           .then(d=>{
	               throw new Error("should have failed")
	           })
	           .catch(ex=>{
	           	  assert.equal(ex.message, "VALIDATION_ERROR")
	           	  done()
	           })
	           .catch(done)
    })

    it('host (accents)', function(done) {

         vali.async({"str":"hosztném"}, {"str":{presence: true, isHost:true}})
           .then(d=>{
               assert.equal(d.str.d_host_canonical_name, "xn--hosztnm-gya")
               done()
           })
           .catch(done)
    })

    it('host (accents, returnCanonical)', function(done) {

         vali.async({"str":"hosztném"}, {"str":{presence: true, isHost:{returnCanonical:true}}})
           .then(d=>{
               assert.equal(d.str, "xn--hosztnm-gya")
               done()
           })
           .catch(done)
    })

    it('host array (accents, returnCanonical)', function(done) {

         vali.async({"str":["hosztném"]}, {"str":{presence: true, isHostArray:{returnCanonical:true}}})
           .then(d=>{
               assert.deepEqual(d.str, ["xn--hosztnm-gya"])
               done()
           })
           .catch(done)
    })

    Array("enforceCanonical", "canonical").forEach(x=>{
	    it('host (accents, '+x+')', function(done) {
	    	 var c = {presence: true, isHost:{}}
	    	 c.isHost[x] = true

	         vali.async({"str":"hosztném"}, {"str":c})
	           .then(d=>{
	           	  throw new Error("should have failed")
	           })
	           .catch(err=>{
	           	  assert.equal(err.errorParameters.str[0], "not canonical")
	           	  done()
	           })
	    })

    })

    for(let q of ["host..name", "host/name", "host|name", "host;name", "host,name", "host name"]) {
	    it('host (invalid, should be rejected)', function(done) {

	         vali.async({"str":q}, {"str":{presence: true, isDomain:true}})
	           .then(d=>{
	               throw new Error("should have failed")
	           })
	           .catch(ex=>{
	           	  assert.equal(ex.message, "VALIDATION_ERROR")
	           	  done()
	           })
	           .catch(done)
	    })
    }

    it('domain (should be rejected)', function(done) {

         vali.async({"str":"hostname"}, {"str":{presence: true, isDomain:true}})
           .then(d=>{
               throw new Error("should have failed")
           })
           .catch(ex=>{
           	  assert.equal(ex.message, "VALIDATION_ERROR")
           	  done()
           })
    })

    it('domain (should be ok)', function(done) {

         vali.async({"str":"hostname.hu"}, {"str":{presence: true, isDomain:true}})
           .then(d=>{
               assert.equal(d.str.d_domain_canonical_name, "hostname.hu")
               done()
           })
           .catch(done)
    })

    it('hostname min level 1', function(done) {

         vali.async({"str":"hostname"}, {"str":{presence: true, isHost:{minLevel: 1}}})
           .then(d=>{
               assert.equal(d.str.d_host_canonical_name, "hostname")
               done()
           })
           .catch(done)
    })

    it('hostname min level 2', function(done) {

         vali.async({"str":"hostname"}, {"str":{presence: true, isHost:{minLevel: 2}}})
           .then(d=>{
               throw new Error("should have failed")
           })
           .catch(ex=>{
           	  assert.equal(ex.message, "VALIDATION_ERROR")
           	  done()
           })
    })

})

describe('integers', function() {
	"use strict"

    it('isInteger', function(done) {

         vali.async({"str":"0"}, {"str":{presence: true, isInteger:true}})
           .then(d=>{
               assert.equal(d.str, 0)
               done()
           })
           .catch(done)

    })



    it('isInteger with string', function(done) {

         vali.async({"str":"1234"}, {"str":{presence: true, isInteger:true}})
           .then(d=>{
               assert.equal(d.str, 1234)
               done()
           })
           .catch(done)

    })

    it('isInteger with string, returning int', function(done) {

         vali.async({"str":"0"}, {"str":{presence: true, isInteger:{lazy:true,returnInt: true}}})
           .then(d=>{
               assert.equal(d.str, 0)
           	   assert.equal(typeof d.str, "number")
               done()
           })
           .catch(done)

    })

    it('isInteger min 1 should fail with 0', function(done) {

         vali.async({"str":"0"}, {"str":{presence: true, isInteger:{greaterThan: 0}}})
           .then(d=>{
               throw new Error("should have failed")
           })
           .catch(ex=>{
           	  assert.equal(ex.message, "VALIDATION_ERROR")
           	  done()
           })

    })
    it('isInteger min 1 should be ok with 1', function(done) {

         vali.async({"str":"1"}, {"str":{presence: true, isInteger:{greaterThan: 0}}})
           .then(d=>{
               assert.equal(d.str, 1)
               done()
           })
           .catch(done)

    })

	it('isDecimal', function(done) {

         vali.async({"str":"3.14"}, {"str":{presence: true, isDecimal:true}})
           .then(d=>{
               assert.equal(d.str, 3.14)
               done()
           })
           .catch(done)

    })

    it('sInteger orEqualTo zero should fail with -1', function(done) {

         vali.async({"t_cron_max_entries":-1}, {"t_cron_max_entries": { isInteger: {greaterThanOrEqualTo:0}, }})
           .then(d=>{
               throw new Error("should have failed")
           })
           .catch(ex=>{
           	  assert.equal(ex.message, "VALIDATION_ERROR")
           	  done()
           })

    })
    it('isInteger orEqualTo zero', function(done) {

         vali.async({"t_cron_max_entries":0}, {"t_cron_max_entries": { isInteger: {greaterThanOrEqualTo:0} }})
           .then(d=>{
               assert.equal(d.t_cron_max_entries, 0)
               done()
           })
           .catch(done)

    })
 })


describe("host prefix", function(){
	"use strict"

	   for(let host of ["something", "foo.something", "*.something", "*"]) {
		    it('valid host prefix: '+host, function(done) {
		         vali.async({"c":host}, {"c":{isHostWildcard:true}})
		           .then(d=>{
		           	   assert.equal(d.c, host)
		           	   done()
		           })
	               .catch(done)
		    })
		}

	    it('invalid host prefix (; in the host), should be rejected', function(done) {

	    	 var host = "some;thing"
	         vali.async({"c":host}, {"c":{isHostWildcard:true}})
	           .then(d=>{
	           	   throw new Error("Should have been an exception.")
	           })
	           .catch(function(ex){
	           	  if(ex.message != "VALIDATION_ERROR")
	           	  	 return done(ex)

	           	  // seems to be correct
	           	  done()
	           })

	    })
})


describe("urls", function(){
	"use strict"

   for(let url of [
   	"http://some.web.site/index.php?foo=bar", 
   	"http://index.hu/", 
   	"http://chronomeeting.blog.hu/2017/03/20/ii_chronomeeting_kiallitas_es_vasar",
    "https://drive.google.com/drive/folders/1B1xJA1mfLTH-bnRQN1RS0DhLbjQ",
   ]) {
	    it('valid http url: '+url, function(done) {
	         vali.async({"c":url}, {"c":{isSimpleWebUrl:true}})
	           .then(d=>{
	           	   assert.equal(d.c, url)
	           	   done()
	           })
               .catch(done)
	    })

   }

   for(let url of ["http://some.web.site/index.php?foo=bar;and=somethingelse", "http://some.web.site/index.php?foo=bar..and=somethingelse"]) {

	    it('invalid http url, should be rejected: '+url, function(done) {

	         vali.async({"c":url}, {"c":{isSimpleWebUrl:true}})
	           .then(d=>{
	           	   throw new Error("Should have been an exception.")
	           })
	           .catch(function(ex){
	           	  if(ex.message != "VALIDATION_ERROR")
	           	  	 return done(ex)

	           	  // seems to be correct
	           	  done()
	           })

	    })
	}

})

describe('strings', function() {
	"use strict"

    it('undefined should not be accepted', function(done) {

         vali.async({"str":undefined}, {"str":{isString: {ascii:true, trim:true}, length: { "minimum": 6}}})
           .then(d=>{
               assert.equal(Object.keys(d).length, 0)
               done()
           })
           .catch(done)

    })


    it('trim', function(done) {

         vali.async({"str":"  hello  world!   "}, {"str":{presence: true, isString:{"trim": true}}})
           .then(d=>{
               assert.equal(d.str, "hello  world!")
               done()
           })
           .catch(done)

    })

    it('lazy', function(done) {

         vali.async({"str":1234}, {"str":{presence: true, isString:{"lazy": true}}})
           .then(d=>{
               assert.equal(d.str, "1234")
               done()
           })
           .catch(done)

    })

	    it('string with endsWith constraint', function(done) {

	         vali.async({"c":"foobar@domain.tld"}, {"c":{isString:{endsWith:"@domain.tld"}}})
	           .then(d=>{
	           	   assert.equal(d.c, 'foobar@domain.tld')
	           	   done()
	           })
               .catch(done)
	    })

	    it('string with endsWith constraint, invalid ending', function(done) {

	         vali.async({"c":"foobar@domain.tld"}, {"c":{isString:{endsWith:"@d0main.tld"}}})
	           .then(d=>{
	           	   throw new Error("Should have been an exception.")
	           })
	           .catch(function(ex){
	           	  if(ex.message != "VALIDATION_ERROR")
	           	  	 return done(ex)

	           	  // seems to be correct
	           	  done()
	           })

	    })


	    it('string with startsWith constraint', function(done) {

	         vali.async({"c":"foobar@domain.tld"}, {"c":{isString:{startsWith:"foobar@"}}})
	           .then(d=>{
	           	   assert.equal(d.c, 'foobar@domain.tld')
	           	   done()
	           })
               .catch(done)
	    })

	    it('string with startsWith constraint, invalid ending', function(done) {

	         vali.async({"c":"foobar@domain.tld"}, {"c":{isString:{startsWith:"f00bar@"}}})
	           .then(d=>{
	           	   throw new Error("Should have been an exception.")
	           })
	           .catch(function(ex){
	           	  if(ex.message != "VALIDATION_ERROR")
	           	  	 return done(ex)

	           	  // seems to be correct
	           	  done()
	           })

	    })


	    it('string with ascii attribute', function(done) {

	         vali.async({"c":"s tr {ssha}"}, {"c":{isString:{ascii:true}}})
	           .then(d=>{
	           	   assert.equal(d.c, 's tr {ssha}')
	           	   done()
	           })
               .catch(done)
	    })

	    it('string with ascii attribute, invalid character', function(done) {

	         vali.async({"c":"st\nr"}, {"c":{isString:{ascii:true}}})
	           .then(d=>{
	           	   throw new Error("Should have been an exception.")
	           })
	           .catch(function(ex){
	           	  if(ex.message != "VALIDATION_ERROR")
	           	  	 return done(ex)

	           	  // seems to be correct
	           	  done()
	           })

	    })

	    it('string with strictName attribute', function(done) {

	         vali.async({"c":"str"}, {"c":{isString:{strictName:true}}})
	           .then(d=>{
	           	   assert.equal(d.c, 'str')
	           	   done()
	           })
               .catch(done)
	    })

	    it('empty string with strictName attribute (wo noEmpty)', function(done) {

	         vali.async({"c":""}, {"c":{isString:{strictName:true}}})
	           .then(d=>{
	           	   throw new Error("Should have been an exception.")
	           })
	           .catch(function(ex){
	           	  if(ex.message != "VALIDATION_ERROR")
	           	  	 return done(ex)

	           	  // seems to be correct
	           	  done()
	           })
	    })

	    it('empty string with strictName attribute (w noEmpty)', function(done) {

	         vali.async({"c":""}, {"c":{isString:{strictName:true, emptyOk: true}}})
	           .then(d=>{
	           	   assert.equal(d.c, '')
	           	   done()
	           })
               .catch(done)
	    })

        Array("st\nr", "st nr", "sdf..sdf", "a\\s", "a/x").forEach(input=>{
		    it('string with strictName attribute, invalid character: '+input, function(done) {

		         vali.async({"c":input}, {"c":{isString:{strictName:true}}})
		           .then(d=>{
		           	   throw new Error("Should have been an exception.")
		           })
		           .catch(function(ex){
		           	  if(ex.message != "VALIDATION_ERROR")
		           	  	 return done(ex)

		           	  // seems to be correct
		           	  done()
		           })

		    })

        })

    it('xss prevention', function(done) {

         vali.async({"str":"  hello <script>alert('x')</script> world!   "}, {"str":{presence: true, isString:true}})
           .then(d=>{
               throw new Error("Should have been an exception.")
           })
           .catch(function(ex){
           	  if(ex.message != "VALIDATION_ERROR")
           	  	 return done(ex)

           	  // seems to be correct
           	  done()
           })

    })


    it('xss prevention turned off', function(done) {

         vali.async({"str":"  hello <script>alert('x')</script> world!   "}, {"str":{presence: true, isString:{"trim": true, "noXssFiltering": true}}})
           .then(d=>{
               assert.equal(d.str, "hello <script>alert('x')</script> world!")

               done()
           })
           .catch(done)

    })



    Array("hello world", "hello world!").forEach(w=>{
		    it('lower case test: should be ok: '+w, function(done) {

		         vali.async({"str":w}, {"str":{presence: true, isString:{"lowerCase": true}}})
		           .then(d=>{
		               assert.equal(d.str, w)
		               done()
		           })
		           .catch(done)
		    })
    })
    Array("hellO world", "hello World!").forEach(w=>{
		    it('lower case test: should be rejected: '+w, tester("lowerCase"))
		    it('lower case test: should be rejected: '+w, tester("upperCase"))

		    function tester(test) {
		    	return function() {
		    	 var constraint = {"str":{presence: true, isString:{}}}
		    	 constraint.str.isString[test] = true

		         return vali.async({"str":w}, constraint)
		           .then(d=>{
		           	  throw new Error("should not happen")
		           })
		           .catch(x=>{
		           	  assert.equal(x.message,"VALIDATION_ERROR")
		           })
		       }
		    }
    })

    Array("HELLO WROLD", "HELLO WORLD!").forEach(w=>{
		    it('upper case test: should be ok: '+w, function(done) {

		         vali.async({"str":w}, {"str":{presence: true, isString:{"upperCase": true}}})
		           .then(d=>{
		               assert.equal(d.str, w)
		               done()
		           })
		           .catch(done)
		    })

    })

    Array("abcdefABCDEF0123456789").forEach(w=>{
		    it('lower case test: should be ok: '+w, function(done) {

		         vali.async({"str":w}, {"str":{presence: true, isString:{"hex": true}}})
		           .then(d=>{
		               assert.equal(d.str, w)
		               done()
		           })
		           .catch(done)
		    })
    })

    it('hex should be rejected', function(done) {

         vali.async({"str":"hello world"}, {"str":{presence: true, isString:{hex: true}}})
           .then(d=>{
               throw new Error("Should have been an exception.")
           })
           .catch(function(ex){
           	  if(ex.message != "VALIDATION_ERROR")
           	  	 return done(ex)

           	  // seems to be correct
           	  done()
           })

    })
 })

describe("php", function(){

    Array("5.6","7.0","test","").forEach(w=>{
		    it('php: '+w, function(done) {

		         vali.async({"str":w}, {"str":{isMajorPhpVersion:{"emptyStringAccepted": true}}})
		           .then(d=>{
		               assert.equal(d.str, w)
		               done()
		           })
		           .catch(done)
		    })
    })

    Array("foo","7.01","").forEach(w=>{
	    it('php should be rejected:'+w, function(done) {

	         vali.async({"str":w}, {"str":{presence: true, isMajorPhpVersion:true}})
	           .then(d=>{
	               throw new Error("Should have been an exception.")
	           })
	           .catch(function(ex){
	           	  if(ex.message != "VALIDATION_ERROR")
	           	  	 return done(ex)

	           	  // seems to be correct
	           	  done()
	           })

	    })
   })

})


describe('default', function() {
	"use strict"

    it('default', function(done) {

         vali.async({}, {"str":{isString:{"trim": true}, "default": "xxx"}})
           .then(d=>{
               assert.equal(d.str, "xxx")
               done()
           })
           .catch(done)

    })

    it('default empty', function(done) {

         vali.async({}, {"str":{isString:{"trim": true}, "default": "[[empty]]"}})
           .then(d=>{
               assert.equal(d.str, "")
               done()
           })
           .catch(done)

    })

    it('default empty 2', function(done) {

         vali.async({}, {"str":{isString:{"trim": true}, "default": "[[]]"}})
           .then(d=>{
               assert.equal(d.str, "")
               done()
           })
           .catch(done)

    })
	it('default false', function(done) {

         vali.async({}, {"b":{isBooleanLazy: true, "default": "[[false]]"}})
           .then(d=>{
               assert.equal(d.b, false)
               done()
           })
           .catch(done)

    })

	it('default 0', function(done) {

         vali.async({}, {"b":{isInteger: true, "default": "[[0]]"}})
           .then(d=>{
               assert.equal(d.b, 0)
               done()
           })
           .catch(done)
    })

	it('default empty string as an option value', function(done) {

         vali.async({}, {"b":{isString: true, "default": {value:""}}})
           .then(d=>{
               assert.equal(d.b, "")
               done()
           })
           .catch(done)
    })
	it('default 0 as an option value', function(done) {

         vali.async({}, {"b":{isInteger: true, "default": {value:0}}})
           .then(d=>{
               assert.equal(d.b, 0)
               done()
           })
           .catch(done)
    })


	it('default false as an option value', function(done) {

         vali.async({}, {"b":{isBooleanLazy: true, "default": {value: false}}})
           .then(d=>{
               assert.equal(d.b, false)
               done()
           })
           .catch(done)

    })
 })

describe('datetime', function() {
	"use strict"

    for (let q of ["foobar","2011-01-0x 10:10:10"]){
		    it('invalid datetime rejected: '+q, function(done) {

		         vali.async({"dt":q}, {"dt":{presence: true, datetime: true}})
		           .then((d)=>{
		           	  done(new Error("invalid date was accepted, we expected validation error"))
		           })
		           .catch((ex)=>{
		           	  done()
		           })

		    })
    }


    for (let q of ["2000-01-01 00:00:00"]){
	    it('correct datetime: '+q, function(done) {

	         vali.async({"dt":q}, {"dt":{presence: true, datetime: true}})
	           .then(d=>{
	               assert.equal(d.dt, q)
	               done()
	           })
	           .catch(done)

	    })
    }
 })

describe('exclusionNg', function() {
	    it('exclusion simply', function(done) {

	         vali.async({"name":"Backup"}, {"name":{presence: true, exclusion: ["backup"]} })
	           .then(d=>{
	               assert.deepEqual(d.name, "Backup")
	               done()
	           })
	           .catch(x=>{
	           	  console.log(x)
	           	  done(x)
	           })

	    })

	    it('string lower case filter', function(done) {

	         vali.async({"name":"Backup"}, {"name":{presence: true, isString: {lowerCase: true}} })
	           .then(d=>{
	           	   throw new Error("shouldnt be here");
	           })
	           .catch(x=>{
	           	  assert.propertyVal(x, "message", "VALIDATION_ERROR")
	           	  done()
	           })

	    })

	    it('exclusion lower case', function(done) {

	         vali.async({"name":"backup"}, {"name":{presence: true, isString: {lowerCase: true}, exclusion: ["backup"]} })
	           .then(d=>{
	           	   throw new Error("shouldnt be here");
	           })
	           .catch(x=>{
	           	  assert.propertyVal(x, "message", "VALIDATION_ERROR")
	           	  done()
	           })

	    })

});


describe('inclusionCombo', function() {
	"use strict"

    for (let q of [{i:"PRIMARY TECH",o:["PRIMARY","TECH"]},{i:["PRIMARY","TECH"],o:["PRIMARY","TECH"]},{i:"PRIMARY PRIMARY",o:["PRIMARY"]}]){
	    it('inclusionCombo with valid: '+q.i, function(done) {

	         vali.async({"c":q.i}, {"c":{presence: true, inclusionCombo: ["PRIMARY","TECH","BILLING","PROMOTION","OTHER"]} })
	           .then(d=>{
	               assert.deepEqual(d.c, q.o)
	               done()
	           })
	           .catch(x=>{
	           	  console.log(x)
	           	  done(x)
	           })

	    })
    }

    for (let q of ["PRIMARY TCH", {}]){
	    it('inclusionCombo with invalid: '+q, function(done) {

	         vali.async({"c":q}, {"c":{presence: true, inclusionCombo: ["PRIMARY","TECH","BILLING","PROMOTION","OTHER"]} })
	           .then(d=>{
	               done(new Error("should have been rejected"))
	           })
	           .catch(x=>{
	           	  assert.equal(x.message,"VALIDATION_ERROR")
	           	  done()
	           })

	    })
    }
 })


describe('Array', function() {
	"use strict"

        var arr = ["val1","val2"]
	    it('isArray', function(done) {

	         vali.async({"c":arr}, {"c":{isArray:true}})
	           .then(d=>{
	               assert.deepEqual(d.c, arr)
	               done()
	           })
	           .catch(done)

	    })
	    it('isArray with constraint', function(done) {

	         vali.async({"c":arr}, {"c":{isArray:{minLength: 2}}})
	           .then(d=>{
	               assert.deepEqual(d.c, arr)
	               done()
	           })
	           .catch(done)

	    })
	    it('isArray with constraint reject', function(done) {

	         vali.async({"c":arr}, {"c":{isArray:{minLength: 3}}})
	           .then(d=>{
	           	   done(new Error("should have failed"))
	           })
	           .catch(x=>{
	           	  assert.equal(x.message, "VALIDATION_ERROR")
	           	  done()
	           })

	    })
	    it('isArray with type constraint', function(done) {

	         vali.async({"c":arr}, {"c":{isArray:{lazyString: true}}})
	           .then(d=>{
	               assert.deepEqual(d.c, arr)
	               done()
	           })
	           .catch(done)

	    })

	    it('isArray with type constraint (negative)', function(done) {

	         vali.async({"c":[{a:"b"}]}, {"c":{isArray:{lazyString: true}}})
	           .then(()=>{
	           	  return done(new Error("Should have failed"));
	           })
	           .catch(er=>{
	           	  done();
	           })

	    })
})


describe('SimpleObject', function() {
	"use strict"

	    it('isObject', function(done) {

	         vali.async({"c":"foobar"}, {"c":{isObject:{acceptString:true}}})
	           .then(d=>{
	               assert.deepEqual(d.c, {data:"foobar"})
	               done()
	           })
	           .catch(done)

	    })

	    it('isObject and strings', function(done) {

	         vali.async({"c":"foobar"}, {"c":{isObject:true}})
	           .then(d=>{
	               done(new Error("should have failed"))
	           })
	           .catch(()=>{
	           	done();
	           })

	    })

	    it('isObject and arrays', function(done) {

	         vali.async({"c":["foobar"]}, {"c":{isObject:{acceptArray:true}}})
	           .then(d=>{
	               assert.deepEqual(d.c, ["foobar"])
	               done()
	           })
	           .catch(done)

	    })

        var h = {"k1":"v1","k2":"v2"}
	    it('isSimpleObject', function(done) {

	         vali.async({"c":h}, {"c":{isSimpleObject:true}})
	           .then(d=>{
	               assert.deepEqual(d.c, h)
	               done()
	           })
	           .catch(done)

	    })


        var h2 = {24: 100};
	    it('isSimpleObject', function(done) {

	         vali.async({"c":h2}, {"c":{isSimpleObject:true}})
	           .then(d=>{
	               assert.deepEqual(d.c, h2)
	               done()
	           })
	           .catch(done)

	    })
	    it('isSimpleObject, array with strings is still simple', function(done) {

             var data = {foo:["bar","bar2"]}
	         vali.async({"c":data}, {"c":{isSimpleObject:true}})
	           .then(d=>{
	               assert.deepEqual(d.c, data)
	               done()
	           })
	           .catch(done)

	    })

	    Array({k:{k:"v"}}, ["a"], "sdf").forEach(bad=>{

		    it('isSimpleObject with constraint reject: '+JSON.stringify(bad), function(done) {

		         vali.async({"c":bad}, {"c":{isSimpleObject:true}})
		           .then(d=>{
		           	   done(new Error("should have failed"))
		           })
		           .catch(x=>{
		           	  assert.equal(x.message, "VALIDATION_ERROR")
		           	  done()
		           })

		    })


	    })

	    it('isSimpleObject should accept strings when instructed to', function(done) {

	         vali.async({"c":"foobar"}, {"c":{isSimpleObject:{acceptString:true}}})
	           .then(d=>{
	               assert.deepEqual(d.c, {data:"foobar"})
	               done()
	           })
	           .catch(done)

	    })

	    it('isSimpleObject should reject strings when not instructed to', function(done) {

	         vali.async({"c":"foobar"}, {"c":{isSimpleObject:true}})
	           .then(d=>{
	               done(new Error("Should have failed."))
	           })
	           .catch(function(){
	           	  done()
	           })

	    })

	    it('isSimpleObject should accept numbers when instructed to', function(done) {

	         vali.async({"c":10004}, {"c":{isSimpleObject:{acceptScalar:true}}})
	           .then(d=>{
	               assert.deepEqual(d.c, {data:10004})
	               done()
	           })
	           .catch(done)

	    })

	    it('isSimpleObject should reject numbers when not instructed to', function(done) {

	         vali.async({"c":1007}, {"c":{isSimpleObject:true}})
	           .then(d=>{
	               done(new Error("Should have failed."))
	           })
	           .catch(function(){
	           	  done()
	           })

	    })

})


describe('booleanLazy', function() {
	"use strict"

    var lazyTests = [{"shouldbe": true, tests: ["true", "1", true]},{"shouldbe": false, tests: ["false", "0", false]}]
    for (let lazyTest of lazyTests) {
    	for (let lazyTestCase of lazyTest.tests) {

			    it('lazy boolean: '+lazyTestCase, function(done) {

			         vali.async({"b":lazyTestCase}, {"b":{presence: true, isBooleanLazy:true}})
			           .then(d=>{
			               assert.equal(d.b, lazyTest.shouldbe)
			               done()
			           })
			           .catch(done)

			    })

    	}

    }

    for (let lazyTestCase of ["some", "crap", "TRUE","FALSE"]) {
	    it('lazy boolean: '+lazyTestCase, function(done) {

	         vali.async({"b":lazyTestCase}, {"b":{presence: true, isBooleanLazy:true}})
	           .then(d=>{
	           	  throw new Error("SHOULD_BE_AN_ERROR")
	           })
	           .catch(function(ex){
	           	  if(ex.message == "VALIDATION_ERROR") return done()
	           	  done(ex)
	           })

	    })
    }

       it('lazy boolean and empty string', function(done) {

			         vali.async({"b":""}, {"b":{isBooleanLazy:true}})
			           .then(d=>{
			               assert.isUndefined(d.b)
			               done()
			           })
			           .catch(done)

			    })
})



describe('objectToString', function() {
	"use strict"


	    it('objectToString array test', function(done) {

	         vali.async({"c":[1,2,3]}, {"c":{objectToString:true}})
	           .then(d=>{
	           	   assert.equal(d.c, "[1,2,3]")
	           	   done()
	           })
	    })

	    it('objectToString object test', function(done) {

	         vali.async({"c":{a:"b"}}, {"c":{objectToString:true}})
	           .then(d=>{
	           	   assert.equal(d.c, '{"a":"b"}')
	           	   done()
	           })
	    })

	    it('objectToString boolean test', function(done) {

	         vali.async({"c":false}, {"c":{objectToString:true}})
	           .then(d=>{
	           	   assert.equal(d.c, 'false')
	           	   done()
	           })
	    })


	    it('objectToString str test', function(done) {

	         vali.async({"c":"str"}, {"c":{objectToString:true}})
	           .then(d=>{
	           	   assert.equal(d.c, 'str')
	           	   done()
	           })
	    })



})
