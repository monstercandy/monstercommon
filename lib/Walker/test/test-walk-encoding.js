(function () {
  "use strict";

  var walk = require('../lib/walk').walk
    , path = require('path')
    , dirname = process.argv[2] || './'
    , walker
    ;

  walker = walk(dirname, {encoding: "buffer"});

  walker.on('directories', function (root, stats, next) {
    stats.forEach(function (stat) {
      console.log('[ds]', root, stat.name.toString());
    });
    next();
  });

  walker.on('directory', function (root, stat, next) {
    console.log('[d]', root, stat.name);
    next();
  });

  walker.on('file', function (root, stat, next) {
    console.log('[f]', root, stat.name.toString(), walker.join("foo", stat.name));
    next();
  });
  
  walker.on("names", function (root, nodeNamesArray) {
    console.log('[ns]', root, nodeNamesArray);
  })
  

  walker.on('end', function () {
    console.log('All Done!');
  });
}());
