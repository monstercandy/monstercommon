module.exports = function(basedir, filters, fs) {

    fs = fs || require("MonsterDotq").fs(); // for mocking
    filters = filters || {}

    var minMilliSeconds
	if(filters.olderThanDays)
	   minMilliSeconds = filters.olderThanDays * 86400 * 1000

    const path = require("path")

    var deleted = []
	return new Promise(function(resolve,reject){

        var dirsToProcess = [basedir]	
	    processNextDir()

        function processNextDir(){
		    var dir = dirsToProcess.shift()
			if(!dir) return resolve()
			
				return fs.readdirAsync(dir)
				  .then(files=>{
					  var ps = []

					  var now = new Date().getTime()
					  files.forEach(file=>{
						 var skip = false
						 Object.keys(filters).some(key=>{
							 var v = filters[key]
							 if((key == "pattern")&&(!file.match(v)))
							 {
								skip = true
								return true
							 }
						 })
						 if(skip) return

						 var full = path.join(dir, file)
						  ps.push(
							 fs.lstatAsync(full)	      	  	 
							   .then(s=>{
									// console.log("xx", full, s)
									 if(s.isDirectory()){
									 
									   if(filters.recursive)
									     dirsToProcess.push(full)
										 
									   return
									 }
									
									 var skip = false
									 Object.keys(filters).some(key=>{
										 var v = filters[key]
										 if((key == "setuid")&&(s.uid != v))
										 {
											skip = true
											return true
										 }
										 if((key == "statPattern")&&(!file.match(v)))
										 {
											skip = true
											return true
										 }
										 
									 })
									 if(skip) return

									 if(typeof minMilliSeconds != "undefined"){
										   var dif = now - s.mtime.getTime()
  									       // console.log("older then: ", dif, now, minMilliSeconds)
										   if(dif < minMilliSeconds) return
								     }

									return fs.unlinkAsync(full)
									  .then(()=>{
										   deleted.push(file)
									  })
							   })
						  )

					  })
					  return Promise.all(ps)
				  })
				  .catch(ex=>{
					 console.error("unable to delete", ex)
					 if(filters.doThrowError)
					   return reject(ex)
					 return Promise.resolve()
				  })
				  .then(()=>{
				     processNextDir()
				  })

		
        }		


	
	})
    .then(()=>{
	  return Promise.resolve(deleted)
    })

}