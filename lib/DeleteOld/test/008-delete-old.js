require( "../../MonsterDependencies")(__dirname)

var assert = require("chai").assert

	describe('delete old', function() {

      const deleteOld = require("delete-old.js")
      const path = require("path")


        it('should delete files matching those filters only', function() {
           
           const stuffs = {
           	 "pattern.xxx": null,
           	 "owner.gz": {uid: 10001},
           	 "age.gz": {uid:10000, mtime: new Date()}, 
           	 "directory.gz": {uid:10000, mtime: new Date(2011,10,30)}, 
           	 "fine.gz": {uid:10000, mtime: new Date(2011,10,30)}, 
           }

           var unlinked = 0

           var fs = {
              readdirAsync: function(path){
                 return Promise.resolve(Object.keys(stuffs))
              },
              lstatAsync: function(n){
              	 // console.log("statAsync",n)
				 // assert.equal(n, "crap")
              	 var b = path.basename(n)
              	 if(b == "pattern.xxx") throw new Error("Should not have stated this one")
				 var x = stuffs[b]
				 if(!x) assert.fail()
				 x.isDirectory = function(){
				   return b == "directory.gz" ? true : false
				 }
              	 return Promise.resolve(x)
              },
              unlinkAsync: function(f){
              	 // console.log("unlinking", f)
              	 unlinked++
              	 return Promise.resolve()
              }
           }

           return deleteOld(
              "basedir", 
              {olderThanDays: 3, setuid: 10000, pattern: /\.gz$/},
              fs)
             .then((deleted)=>{

                assert.deepEqual(deleted, ["fine.gz"])
                assert.equal(unlinked, 1)
             })

        })




        it('should work recursively as well', function() {
           
           const stuffs = {
		     "basedir": ["directory","fine.gz"], 
			 "basedir\\directory": ["other.gz"],
           }

           var unlinked = 0

           var fs = {
              readdirAsync: function(path){
			     console.log("readdir", path)
				 if(!stuffs[path]) throw new Error("not expected")
                 return Promise.resolve(stuffs[path])
              },
              lstatAsync: function(n){
              	 // console.log("statAsync",n)
				 // assert.equal(n, "crap")
              	 var b = path.basename(n)

				 var x = {}
				 if(!x) assert.fail()
				 x.mtime = new Date(2011,10,30)
				 x.isDirectory = function(){
				   return b == "directory" ? true : false
				 }
              	 return Promise.resolve(x)
              },
              unlinkAsync: function(f){
              	 // console.log("unlinking", f)
              	 unlinked++
              	 return Promise.resolve()
              }
           }

           return deleteOld(
              "basedir", 
              {olderThanDays: 3, recursive: true, statPattern: /\.gz$/},
              fs)
             .then((deleted)=>{

                assert.deepEqual(deleted, ["fine.gz","other.gz"])
                assert.equal(unlinked, 2)

             })

        })


	})


