
/*

This:
fs.createReadStream('test.tgz').pipe(require("zlib").createGunzip()).pipe(require("tar-fs").extract('./dest-directory'))

Is the same as this:
fs.createReadStream('test.tgz').pipe(streams.ExtractTgz('./dest-directory'))

*/
module.exports = function() {

	var gzipStream = require("zlib").createGunzip()
	gzipStream.tarStream = require("tar-fs").extract.apply(null, arguments)
	gzipStream.pipe(gzipStream.tarStream)
    return gzipStream

}
