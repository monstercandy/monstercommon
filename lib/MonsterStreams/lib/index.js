
"use strict"

var exports = module.exports = {}

var path = require("path")

exports.Base64ToBuffer = function(data) {

  return new Buffer(data, 'base64');
}

var supportedMethods = {
	"PackTgz": "tgz-pack",
	"ExtractTgz": "tgz-extract",
	"StreamToBase64": "base64",
}

for(let q of Object.keys(supportedMethods)) {
	exports[q] = function() {
		var f = require(path.join(__dirname,supportedMethods[q]+".js"))
	    return f.apply(null, arguments)	
	}
}


exports.BufferStream= require(path.join(__dirname,"BufferStream.js"))
