
/*
// example:
streams.StreamToBase64( process.stdin, function(b64) {
  console.log(b64)
})

*/
module.exports = function(stream, callback){

	  var chunks = [];
	  stream.on("data", function(chunk) {
	    chunks.push(chunk);
	  }).on("end", function(){
	    var result = Buffer.concat(chunks);
	    callback( result.toString('base64') )
	  });

}
