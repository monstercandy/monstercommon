/*
//example:
var w = fs.createWriteStream('test.tgz')
w.on("finish", function(){ console.log("writestream ended!")})
streams.PackTgz("../MonsterStreams").pipe(w)
*/


module.exports = function(dir, options) {
  
  var tarStream = require("tar-fs").pack.apply(null, arguments)
  var gzipStream = require("zlib").createGzip( options )
  tarStream.pipe(gzipStream)

  return gzipStream
}
