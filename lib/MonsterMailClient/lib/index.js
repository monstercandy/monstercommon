module.exports = function(options) {

    "use strict"

    const MError = require("MonsterError")
    if(typeof(options) !== "object") throw new MError("INVALID_PARAM")
    if(!options.server) throw new MError("MISSING_SERVER")

    var MonsterClientLib = require("MonsterClient")

	var re = {}

	re.SendMailAsync = function(details) {
        // TODO:
        // logic to send to /accountapi/account/:account_id/sendmail/:email_category when toAccount or localeAsUser is present
        return Promise.resolve()
          .then(()=>{
                if(typeof(details) !== "object") throw new MError("INVALID_PARAM");
                if(!details.template) throw new MError("MISSING_TEMPLATE");
                // we dont need this validity checking here anymore since the subject might come from the template
                //if(!details.subject) return throw new MError("MISSING_SUBJECT");

                if(Array.isArray(details.attachments)) {
                    for(var q of details.attachments) {
                        if(!q["name"]) throw new MError("ATTACHMENT_NAME_MISSING");
                        if(!q["type"]) throw new MError("ATTACHMENT_TYPE_MISSING");
                        if (typeof(q["inline"]) === 'undefined')
                           q["inline"] = true
                    }
                }
                else
                    delete details.attachments

              var pool = MonsterClientLib.getMapiPoolCached(options.server);

              // localeAsUser was never in use
              // toAccount might look like this: x.toAccount = {user_id: webhosting.wh_user_id, emailCategory: "TECH"}
              // or this: "1235";
              var accountId;
              var email_category = "";
              if(typeof details.toAccount == "object") {
                email_category = details.toAccount.emailCategory;
                accountId = details.toAccount.user_id;
                delete details.toAccount;
              }
              else 
                accountId = details.toAccount || details.localeAsUser;

              if(accountId)
                return pool.postAsync("/accountapi/account/"+accountId+"/sendmail/"+email_category, details);
              else
                return pool.postAsync("/s/[this]/mailer/sendmail", details);

          })


    }


    require("MonsterDotq")(re)

	return re


}
