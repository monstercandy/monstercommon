
var swig = require( "../lib/index.js")()

var assert = require("chai").assert

const templateFilename = "templates/test.html"
const templateLngFilename = "templates/test-lng.html"
const templateSubjectFilename = "templates/test-subject.html"

const expectedLngHtml = "<html>\n\t<head>\n\t\t<meta charset=\"utf-8\">\n\t</head>\n\t<body>\n\t  Hey Joe this is just a simple test: alright?\n\t</body>\n</html>\n"

const expectedHtml = "<html>\n\t<head>\n\t\t<meta charset=\"utf-8\">\n\t</head>\n\t<body>\n\t  Hey Joe this is just a simple test.\n\t</body>\n</html>\n"
const expectedBoldedHtml = "<html>\n\t<head>\n\t\t<meta charset=\"utf-8\">\n\t</head>\n\t<body>\n\t  Hey &lt;b&gt;Joe&lt;/b&gt; this is just a simple test.\n\t</body>\n</html>\n"
const expectedText = "Hey Joe this is just a simple test."
const expectedWrappedText = "Hey Joe\nthis is\njust a\nsimple\ntest."

const expectedSubject = "This is Joe subject!"

describe('simple', function() {    
    "use strict"

    it('html test', function(done) {

         swig.renderFileAsync( templateFilename, { "name": "Joe" })
           .then((res)=>{
              assert.equal(expectedHtml, res.html) 
              assert.notProperty(res, "text")

              done()
           })
           .catch((ex)=>{
              done(ex)
           })


    })

    it('html test (bolded/autoescaping)', function(done) {

         swig.renderFileAsync( templateFilename, { "name": "<b>Joe</b>" })
           .then((res)=>{
              assert.equal(expectedBoldedHtml, res.html) 
              assert.notProperty(res, "text")

              done()
           })
           .catch((ex)=>{
              done(ex)
           })


    })



    for(let q of Array({texthash:{}}, {texthash: true})) {
            it('html/text test', function(done) {

                 swig.renderFileAsync( templateFilename, { "name": "Joe" }, q)
                   .then((res)=>{
                      assert.equal(expectedHtml, res.html) 
                      assert.equal(expectedText, res.text)

                      done()
                   })
                   .catch((ex)=>{
                      done(ex)
                   })


            })
    }


    it('html/text test, word wrapped', function(done) {

         swig.renderFileAsync( templateFilename, { "name": "Joe" }, {texthash:{wordwrap: 10}})
           .then((res)=>{
              assert.equal(expectedHtml, res.html) 
              assert.equal(expectedWrappedText, res.text)

              done()
           })
           .catch((ex)=>{
              done(ex)
           })


    })


    it('html test, language test', function(done) {

         swig.renderFileAsync( templateLngFilename, { "name": "Joe" })
           .then((res)=>{
              assert.equal(expectedLngHtml, res.html) 
              assert.notProperty(res, "text")

              done()
           })
           .catch((ex)=>{
              done(ex)
           })


    })


    it('subject test', function(done) {

         swig.renderFileAsync( templateSubjectFilename, { "name": "Joe" }, { subject: true })
           .then((res)=>{
              assert.propertyVal(res, "subject", expectedSubject)
              assert.equal(expectedHtml, res.html) 

              done()
           })
           .catch((ex)=>{
              done(ex)
           })


    })

	
    it('render string', function(done) {

	     swig.renderAsync('{{ tacos }}', { tacos: 'Tacos!!!!' })		 
           .then((res)=>{
              assert.propertyVal(res, "html", "Tacos!!!!") 

              done()
           })
           .catch((ex)=>{
              done(ex)
           })


    })

    it('render string, without stripping angular params', function(done) {

	     swig.renderAsync('{{ "lng" | __ :(foo) }}', { tacos: 'Tacos!!!!' })		 
           .then((res)=>{
		      done(new Error("Should have failed (swig doesnt understand angular filter parameters)"))
           })
           .catch((ex)=>{
              done()
           })
    })
	
    it('render string, stripping angular params', function(done) {

	     swig.renderAsync('{{ "lng" | __ :(foo) }}', { tacos: 'Tacos!!!!' }, {stripAngularParams: true})		 
           .then((res)=>{
              assert.propertyVal(res, "html", "alright?") 

              done()
           })
           .catch((ex)=>{
              done(ex)
           })
    })


    it('i18n filter should return safe strings (no additional escaping), but it should escape the parameters themselves internally', function(done) {

       swig.renderAsync('{{ "<b>lng</b> %s" | __(tacos) }}', { tacos: 'Ta<b>c</b>os!!!!' })     
           .then((res)=>{
              assert.propertyVal(res, "html", "<b>lng</b> Ta&lt;b&gt;c&lt;/b&gt;os!!!!") 

              done()
           })
           .catch((ex)=>{
              done(ex)
           })
    })

})
