module.exports = function(i18noptions) {

    // Load Module and Instantiate
    if(!i18noptions) i18noptions = {}
    if(!i18noptions.locales) i18noptions.locales = ["en"]
    var i18n = new (require('i18n-2'))(i18noptions);

    var htmlToText
    var swig  = require('swig');

    var escapeTpl = swig.compile('{{ tacos }}');

    function escaper(hashref) {
        Object.keys(hashref).forEach(k=>{
            var v = hashref[k]
            if(!v) return
            hashref[k] = escapeTpl({tacos: v})
        })
        return hashref
    }

    function i18nNormal(input, p1, p2, p3) {
       var escaped = escaper({p1:p1,p2:p2,p3:p3})
       var re = i18n.__(input, escaped.p1, escaped.p2, escaped.p3)
       return re
    }    
    i18nNormal.safe = true

    function i18nN(one, other, count, p1, p2, p3) {
       var escaped = escaper({p1:p1,p2:p2,p3:p3})
       var re = i18n.__n(one, other, count, escaped.p1, escaped.p2, escaped.p3)
       return re
    }
    i18nN.safe = true

    swig.setFilter('__',  i18nNormal);
    swig.setFilter('__n', i18nN);

    swig.renderAsync = function(str, renderHash, options) {

		return new Promise(function(resolve, reject) {
		
		   options = optionsPreset(options)
		   if(options.stripAngularParams)		
              str = str.replace(/\|\s*__\s*:[^}]+/i, "|__"); 
		   
		   var re = swig.render(str, {locals:renderHash})
           swigFinisher(resolve,reject, options)(null, re)
		})
	}

	function optionsPreset(options){
        if(typeof options === "undefined") options = {}
        if(options === true) options = {"texthash":{}}
		if(options.locale)
			i18n.setLocale(options.locale)
		
		return options
	}
	
	function swigFinisher(resolve,reject, options) {
        return function(err, output){
		
        		if(err)return reject(err)

                var re = {}
                if(options.subject) {
                    var i = output.indexOf("\n");
                    re.subject = output.slice(0,i)
                    output = output.slice(i+1);
                }
                re.html = output
                if(options.texthash)
                {
                    if(!htmlToText)
                        htmlToText = require('html-to-text');

                    if(typeof options.texthash === "boolean")
                       options.texthash = {}
                    // console.log("xxxx",typeof textHash, textHash)

                    re.text = htmlToText.fromString(output, options.texthash)
                }

                return resolve(re)
            }	
	}
	
    swig.renderFileAsync = function(path, renderHash, options) {

        return new Promise(function(resolve,reject){

            swig.renderFile(path, renderHash, swigFinisher(resolve,reject, optionsPreset(options)))
        })		
	
    }


    return swig
}

