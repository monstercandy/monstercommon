var lib = module.exports = function(app, mainRouter, wie) {

  mainRouter.use("/wie", lib.getSubrouter(app, {wie:wie}));

  lib.transformWie(app, wie);
}


lib.getSubrouter = function(app, wieRef){

  var router = app.ExpressPromiseRouter()

  // get all settings per webstorages
  router.get( "/", function (req, res, next) {
      return req.sendPromResultAsIs(wieRef.wie.BackupAll(req));
  })

  // restore settings for multiple webstorages
  router.post( "/", function (req, res, next) {
      return req.sendOk(wieRef.wie.RestoreAll(req.body.json, req));
  })

  // get all settings for the specified webhosting
  router.get( "/:wh_id", function (req, res, next) {
      return req.sendPromResultAsIs(wieRef.wie.Backup(req.params.wh_id, req));
  })

  // restore settings for the specified webhosting
  router.post( "/:wh_id", function (req, res, next) {
      return req.sendOk(wieRef.wie.Restore(req.params.wh_id, req.body.json, req));
  })

  return router;
}

lib.transformWie = function(app, wie) {
  const dotq = require("MonsterDotq");


    if(!wie.RestoreAll) {
      wie.RestoreAll = function(in_data, req){

         var whs = {};
           return dotq.linearMap({array: Object.keys(in_data), action: function(whId){
              return app.MonsterInfoWebhosting.GetInfo(wh_id)
                .then(wh=>{
                    whs[wh_id] = wh;
                })
       }})
       .then(()=>{
              return wie.RestoreAllWhs(whs, in_data, req)
       })

      }
    }

  wie.GenericRestoreAllWhs = function(whs, in_data, req){

         return dotq.linearMap({array: Object.keys(in_data), action: function(whId){
           return wie.RestoreWh(whs[whId], in_data[whId], req)
         }})
  }

    if(!wie.RestoreAllWhs) {
      wie.RestoreAllWhs = wie.GenericRestoreAllWhs;
    }

    if(!wie.BackupAll) {
      wie.BackupAll = function(in_data, req){

        var re = {};
        return wie.GetAllWebhostingIds()
          .then(whIds=>{
             return dotq.linearMap({array: whIds, catch: true, action: function(whId){
               return wie.Backup(whId, req)
                 .then(data=>{
                    re[whId] = data;
                 })
             }})

          })
          .then(()=>{
             return re;
          })

      }
    }

    if(!wie.GetInfoAboutWebhostingById) {
        wie.GetInfoAboutWebhostingById = function(wh_id) {
           return app.MonsterInfoWebhosting.GetInfo(wh_id)
        }
    }


    if(!wie.Restore) {
       wie.Restore = function(wh_id, in_data, req) {

        var wh;
        return wie.GetInfoAboutWebhostingById(wh_id)
          .then(wh=>{
              return wie.RestoreWh(wh, in_data, req);
          })
       }
    }
   if(!wie.Backup) {
       wie.Backup = function(wh_id, req) {

        var wh;
        return wie.GetInfoAboutWebhostingById(wh_id)
          .then(wh=>{
              return wie.BackupWh(wh, req);
          })
       }
    }

}
