require( "../../MonsterDependencies")(__dirname)

var requestLib = require( "../lib/index.js")
var assert = require("chai").assert

describe('fetching some resources', function() {

    it("just a simple .get", function(done){
	   this.timeout(10000)
	   return requestLib.get("https://httpbin.org/get?a=1")
				.on('response', function(response) {

				    done();
				  })
				.on('error', function(err) {

				    done(err);
				  })
	})

    it("fetching some get data", function(){
	   this.timeout(10000)
	   return requestLib.PostForm("https://httpbin.org/get?a=1", null, {asJson: true})
	     .then(d=>{
		    assert.propertyVal(d.args, "a", '1')
		 })
	})

    it("posting some data", function(){
	   this.timeout(10000)
	   return requestLib.PostForm("https://httpbin.org/post", {a:"1"}, {asJson:true})
	     .then(d=>{
		    assert.propertyVal(d.form, "a", '1')
		 })
	})

    it("custom headers", function(){
	   this.timeout(10000)
	   return requestLib.PostForm("https://httpbin.org/user-agent", null, {asJson:true,headers:{'User-Agent':'foo'}})
	     .then(d=>{
		    assert.propertyVal(d, "user-agent", 'foo')
		 })
	})

    it("response size limit", function(){
	   this.timeout(10000)
	   return requestLib.PostForm("https://httpbin.org/get?a=1", null, {responseLimit: 10})
	     .then(d=>{

			throw new Error("should have failed");
		 })
	     .catch(ex=>{
		   assert.propertyVal(ex, "message", "RESPONSE_TOO_LARGE")
		 })
	})

})
