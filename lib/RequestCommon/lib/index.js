(function(){

	var ca_certificates = []
	try{
	   // the default is told to be only 5 so we MUST increase this
       const https = require("https");
	   https.globalAgent.maxSockets = Infinity;
       https.globalAgent.options.ca = require("fs").readFileSync('/etc/ssl/certs/ca-certificates.crt');

	}catch(x) {
		// swallow error (because of windows)
		if(!/^win/.test(process.platform))
			throw x;
	}

	const request = module.exports = require("request");

	request.PostForm = function(url, formData, options) {
		  if(!options) options = {};
	      var thereIsAnyData = (formData && Object.keys(formData).length) ? true : false;
	      if((options.method == "get")&&(thereIsAnyData)) {
	      	 const buildUrl = require("build-url");
	      	 url = buildUrl(url, {queryParams: formData});
	      }

	      var o = extend({}, {
	          url: url,
	          method: options.method || (thereIsAnyData ? "post" : "get"),
	          form: formData,
	        },
	        options
	      )
	      return new Promise(function(resolve,reject){
	         var r
	         if(o.devNull) {
	         	o.pipe = function(){
	         		const fs = require("fs")
	         		return fs.createWriteStream("/dev/null");
	         	}
	         }

	         if(o.pipe) {
	            r = request(o)
	            r.on("response", function(resp){
	               resp.on("error", function(err){
				      reject(err)
				   })
	               resp.pipe(o.pipe())
	               resp.on("end", function(){
	               	  return resolve()
	               })
	            })
	         }
	         else
	         if(o.responseLimit) {
	            r = request(o)
	            r.on("response", function(resp){
	               var rawData = ""
	               var rawLength = 0
	               resp.on("error", function(err){
				      reject(err)
				   })
	               resp.on("data", function(chunk){
	                  if(rawLength + chunk.length > o.responseLimit) {
	                  	 r.abort()
	                     return reject(new Error("RESPONSE_TOO_LARGE"))
	                  }
	                  rawData += chunk.toString()
	                  rawLength = rawData.length
	               })
	               resp.on("end", function(){
	               	   return resolve(rawData)
	               })

	            })
	         } else {
	           r = request(o, function(err,response,body){
	                if(err) return reject(err)
	                return resolve(body)
	           })
	         }

	         r.on("error", function(err){
	            console.error("error while reading", url, err)
	            reject(err)
	         })

	      })
		  .then(d=>{
		     if(o.asJson) d = JSON.parse(d)
			 return Promise.resolve(d)
		  })
	    }


})()
