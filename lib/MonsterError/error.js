var util = require("util")
const MError = module.exports = function(pErrorMessage, pErrorParameters, pUnderlyingException) {
  Error.call(this);
  Error.captureStackTrace(this, this.constructor);
  this.message = pErrorMessage
  this.errorParameters = pErrorParameters
  this.underlyingException = pUnderlyingException
}
util.inherits(MError, Error)
