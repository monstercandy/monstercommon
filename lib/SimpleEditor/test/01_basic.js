
require("../../MonsterDependencies")(__dirname);
var assert = require("chai").assert

describe('simple', function() {    
    "use strict"

    const lib = require( "../lib/index.js");

    it('listing files', function() {

         var editor = lib({baseDir: "test"});
         return editor.getFiles()
           .then(files=>{
           	  assert.deepEqual(files, ["01_basic.js"]);
           })
    })

    it('listing files with regexp filter (matches)', function() {

         var editor = lib({baseDir: "test", fileFilter: /\.js$/});
         return editor.getFiles()
           .then(files=>{
           	  assert.deepEqual(files, ["01_basic.js"]);
           })
    })

    it('listing files with regexp filter (not-matches)', function() {

         var editor = lib({baseDir: "test", fileFilter: /\.jsx$/});
         return editor.getFiles()
           .then(files=>{
           	  assert.deepEqual(files, []);
           })
    })

    it('fetch content of a file (when file filter does not permit it)', function() {

         var editor = lib({baseDir: "test", fileFilter: /\.jsx$/});
         return editor.fetch({filename: "01_basic.js"})
           .catch(x=>{
           	  assert.propertyVal(x, "message", "VALIDATION_ERROR");
           })
    })

    it('fetch content of a file (when file filter permits it)', function() {

         var editor = lib({baseDir: "test", fileFilter: /\.js$/});
         return editor.fetch({filename: "01_basic.js"})
           .then(d=>{
           	  assert.property(d, "data");
           	  assert.ok(d.data.length > 100);
           })
    })

    it('fetch content of a file (when file filter permits it but it does not exist)', function() {

         var editor = lib({baseDir: "test", fileFilter: /\.js$/});
         return editor.fetch({filename: "01_basicx.js"})
           .catch(x=>{
           	  assert.propertyVal(x, "message", "INVALID_FILE");
           })
    })

    it('trying to fetch a file via directory traversal', function() {

         var editor = lib({baseDir: "test"});
         return editor.fetch({filename: "../test/01_basic.js"})
           .catch(x=>{
           	  assert.propertyVal(x, "message", "VALIDATION_ERROR");
           })
    })

    it('saving a file and then fetching it back and then removing it', function() {
    	 var fn = "test.xxx";
    	 var data = "cotnent";

         var editor = lib({baseDir: "node_modules"});
         return editor.save({filename: fn, data: data})
           .then(()=>{
           	  return editor.fetch({filename: fn});
           })
           .then((d)=>{
           	  assert.deepEqual({data: data}, d)
           	  return editor.delete({filename: fn})
           })

    })

    it('trying to save a file via directory traversal', function() {

         var editor = lib({baseDir: "node_modules"});
         return editor.save({filename: "../node_modules/test.xxx", data: "data"})
           .catch(x=>{
           	  assert.propertyVal(x, "message", "VALIDATION_ERROR");
           })
    })

    it('trying to fetch a file from a fileFilter based editor', function() {

         var editor = lib({baseDir: "node_modules", fileFilter: /\.cf$/});
         return editor.fetch({filename: "01-always.cf"})
           .catch(x=>{
              assert.propertyVal(x, "message", "INVALID_FILE");
           })
    })

})
