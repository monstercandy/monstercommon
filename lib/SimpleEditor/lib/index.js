module.exports = function(config){
   if(!config) config = {};

   if(!config.baseDir) throw new Error("baseDir is missing");

   const path = require("path");
   const fs = require("MonsterDotq").fs();
   const MError = require("MonsterError");
   var ValidatorsLib = require("MonsterValidators")
   var vali = ValidatorsLib.ValidateJs()

   vali.validators.isNonFilteredFilename = function(value, options, key, attributes, glob) {
      if(!value) return;
      if(!isNonFilteredFilename(value, options.config)) {
         return "invalid filename";
      }
   }

   var re = {};

   re.getRouter = function(app) {
	   var router = app.ExpressPromiseRouter();

	   router.post("/list", function(req,res){
	   	  return req.sendPromResultAsIs(re.getFiles());
	   });
	   router.post("/fetch", function(req,res){
	   	  return req.sendPromResultAsIs(
	   	  	 re.fetch(req.body.json)
	   	  );
	   });
	   router.post("/save", function(req,res){
	   	  return req.sendOk(
	   	  	  re.save(req.body.json)
	   	  );
	   });
     router.post("/delete", function(req,res){
        return req.sendOk(
            re.delete(req.body.json)
        );
     });
     if(config.exposeMainConfig) {
       router.post("/config/defaults", function(req,res){
          return req.sendResponse(app.config.defaults());
       });
       router.post("/config/file", function(req,res){
          return req.sendPromResultAsIs(fs.readFileAsync(app.config.getConfigfilename(), "utf8").then((d)=>{
             return JSON.parse(d);
          }));
       });
      
     }
	   return router;
   }

   re.save = function(in_data) {
      var d;
      return isValidFilenameAsync(in_data.filename)
	  	  .then(ad=>{
            d = ad;
	  	  	  return fs.writeFileAsync(path.join(config.baseDir, d.filename), in_data.data);
	  	  })
        .then(()=>{
          if(config.onChange)
             return config.onChange(d);
        })
   }

   re.delete = function(in_data) {
      var d;
      return isValidFilenameAsync(in_data.filename)
        .then(ad=>{
            d = ad;
            return fs.unlinkAsync(path.join(config.baseDir, d.filename));
        })
        .then(()=>{
          if(config.onChange)
             return config.onChange(d);
        })

   }

   re.fetch = function(in_data) {
  	  return getFile(in_data.filename)
  	  .then(file=>{
  	  	  return fs.readFileAsync(path.join(config.baseDir, file), "utf8");
  	  })
  	  .then(d=>{
  	  	  return {data: d};
  	  })   	
   }

   re.getFiles = function(){
   	  return fs.readdirAsync(config.baseDir)
   	    .then(files=>{
   	    	if(!config.fileFilter) return files;

            var re = Array();
   	    	files.forEach(function(file){
   	    	   if(isNonFilteredFilename(file)) {
               	  re.push(file);
               }
   	    	})
   	    	return re;
   	    })
   }


   return re;


   function getFile(filename){
   	  var d;
      return isValidFilenameAsync(filename)
        .then((ad)=>{
        	d = ad;
        	return re.getFiles();
        })
        .then(files=>{
            if(files.indexOf(d.filename) < 0) throw new MError("INVALID_FILE");

            return d.filename;
        })
   }

   function isValidFilenameAsync(filename){
   	  return vali.async({filename: filename}, {filename: {presence: true, isSimpleFilename: true, isNonFilteredFilename: {config:config}}})

   }

   function isNonFilteredFilename(filename, aConfig) {
      if(!aConfig) aConfig = config;
   	  if(!aConfig.fileFilter) return true;
   	  var m = aConfig.fileFilter.exec(filename);
      return m != null;
   }

}
