
module.exports = function(config, serverName, maxServers) {

   const MError = require("MonsterError")
   const RecordCommon = require('MonsterValidators')

   var express = require('express');
   var re = express.Router();

   var volumeName = serverName + "-servers"

   function getVolume(){
      return config.get("volume") || []
   }

   re.FindServers = function(hostnameAndPort, callback) {
     var n = new Array()
     var f = new Array()
     var o = re.GetRawServers()

     for(q of o) {
        if(q["host"] + ":" + (q["port"]||"") == hostnameAndPort) {
           f.push(q)
        } else {
           n.push(q)
        }
     }

     callback(f, n)
  }


  re.FirstServer = function() {
    var o = re.GetRawServers()
    for(q of o) {
         return q
    }
  }
  re.FirstServerOrThrow = function() {
    var x = re.FirstServer()
    if(!x) throw new MError("SERVICE_NOT_CONFIGURED")
    return x
  }

  re.GetRawServers = function() {
     return  getVolume()[volumeName] || []
  }

  re.GetVolumeName = function(){
    return volumeName;
  }


  re.use("/", function(req,res,next){

      if(!req.validationInternal)
         return next(new MError("INTERNAL_VALIDATION_REQUIRED"))

      next()
  })


  re.delete('/:tapiname', function(req, res, next){

     var toDelete = req.params.tapiname

     re.FindServers(toDelete, (matching, nonMatching)=>{

           if(!matching.length) return next(new MError("NOT_FOUND"))

           var volume = getVolume()
           volume[volumeName] = nonMatching

           config.saveVolume(volume, (err)=>{
              if (err) return next(new MError("CANT_SAVE_VOLUME_CONF", null, err))

              req.sendOk()
           });


     })

  })

  function putter(req, res, next) {

       if(maxServers) {
           var servers = re.GetRawServers()
           if(servers.length >= maxServers)
             return next(new MError("SERVER_LIMIT_REACHED"))
       }

       var j = req.body.json
       var r = {}
       if((!j["host"])||(!RecordCommon.IsValidHost(j["host"])))
          return next(new MError("HOST_NOT_SPECIFIED"))
        r["host"] = j["host"]
       if(j["port"]){
          if(!String(j["port"]).match(/^\d+$/))
            return next(new MError("PORT_IS_INVALID"))
          r["port"] = j["port"]
       }
       if(!j["scheme"]) j["scheme"] = "http"
       if(!j["scheme"].match(/^https?$/))
           return next(new MError("SCHEME_IS_INVALID"))
       r["scheme"] = j["scheme"]

       if(j["api_prefix"])
           r["api_prefix"] = j["api_prefix"]

       if(j["name"])
           r["name"] = j["name"]

       if((j["username"])&&(j["password"])) {
          // public api mode
          r["username"] = j["username"]
          r["password"] = j["password"]
       }
       else
       if((j["auth_token"])) {
          // relayer mode (whatever it is)
          r["auth_token"] = j["auth_token"]
       }
       else
       if((j["api_secret"])&&(require("util").isString(j["api_secret"]))) {
          // classic internal api mode
          r["api_secret"] = j["api_secret"]
       }
       else
          return next(new MError("API_SECRET_NOT_SPECIFIED"))

       re.FindServers(r["host"]+":"+(r["port"]||""), (matching, nonMatching)=>{

           if(matching.length) return next(new MError("SERVER_ALREADY_ADDED"))

           nonMatching.push(r)

           var volume = getVolume()
           volume[volumeName] = nonMatching

           try{
              config.saveVolume(volume)
              req.sendOk()

           }catch(ex){
              return next(new MError("CANT_SAVE_VOLUME_CONF", null, ex))
           }


       })


   }

  re.route("/")
   .get(function(req, res, next) {

         req.sendResponse(re.GetRawServers())

     })
   .put(putter)
   .post(function(req, res, next) {
         var volume = getVolume()
         volume[volumeName] = []
         config.set("volume", volume)

         putter(req, res, next)
   })


   return re

}
