module.exports = function(config) {
  return function (req, res, next) {

  	    if(req.dontParseBody) return next()

	    const MError = require("MonsterError")

	    var max_request_body_size = config.get("max_request_body_size")
	    if(req.path.match(/^\/[\w\d]+\/(import|export)\//)) 
	       max_request_body_size = config.get("max_request_body_size_import_export")

        req.body = {raw:'', json: {}};
	    req.setEncoding('utf8');

	    req.on('data', function(chunk) { 

	      if(req.body.raw.length + chunk.length > max_request_body_size)
	        return next(new MError("REQUEST_BODY_TOO_LARGE"))
	      req.body.raw += chunk;

      	  if(req.specialBodyParser)
    		  req.specialBodyParser(chunk)

	    });

	    req.on('end', function(d) {

	    	if(req.body.raw) {

	    		try{
	    		   req.body.json = JSON.parse(req.body.raw)
	    	    } catch(ex){
	    	    	return next(new MError("JSON_PAYLOAD_ERROR", null, ex))
	    	    }
	    	}

            var r
	    	if(req.specialBodyParser)
	    		r = req.specialBodyParser(null)

	    	next(r)
	    });


  }	
} 
