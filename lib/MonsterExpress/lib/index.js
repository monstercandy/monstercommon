const util = require("util")
const path = require('path')
require(path.join(__dirname, "date-unix.js"))
var express = require('express');

const xMcSecretHeaderName = "x-mc-secret";

var connectionsCounter = 0

var exports = module.exports = {}


const MError = exports.Error = require("MonsterError")

exports.Express = function(config, moduleOptions) {
  if(!config)
      throw new Error("config not provided")
  moduleOptions = moduleOptions || {}

  var moment

  var app = express();
  app.Error = MError
  app.express = express
  app.ExpressPromiseRouter = require("express-promise-router");
  app.config = config
  app.Mapi = require("MonsterClient")
  var pathes = {}
  pathes.fullPublicApiPrefixes = ["/ping"]
  pathes.dontParseRequestBodyPrefixes = []

  // cleanupOlderSec should be way lower than session_expiration in MonsterAccount
  app.config.defaults({
    "cleanupOlderSec": 900,
    "cleanupIntervalMs": 1*60*1000, // we cleanup every minute and delete cache items older than 15 minutes
  })

  const server_name = app.config.get("server_name")

  const bodyParser = require(path.join(__dirname, "body-parser.js"))(config)

  function headingMiddleware() {
      var morgan = require('morgan')

      const mcForwardedForSecret = app.config.get("mc_forwarded_for_secret");


      app.use(function(req,res,next){
        req.userAgent = req.headers['user-agent'] || ""

        req.GetMonsterTime = function(){
           if(!req.MonsterTime) {
              req.MonsterTime = req.headers["monstertime"] || new Date().unixtime();
           }

           return req.MonsterTime;
        }


        if((mcForwardedForSecret)&&(req.headers["monsterforwardedfor"])) {

           if(req.headers["monsterforwardedforsecret"] == mcForwardedForSecret) {
              req.clientIp = req.headers["monsterforwardedfor"];
              req.clientServer = req.headers["monsterserver"];
           }
           else
              console.error("MonsterForwardedFor secret is incorrect!", req.headers["monsterforwardedfor"], req.headers["monsterforwardedforsecret"]);
        } 

        if(!req.clientIp) {
           req.clientIp = req.ip;          
        }

        if(!req.clientServer) {
           req.clientServer = server_name;
        }

        var fullPath = req.path // this one is required here, since express routers override the path
        req.sendResponse = function(re) {
           return app.sendResponse(req, res, 200, re, fullPath)
        }
        req.sendTask = function(prom) {
           return prom.then(h=>{
              if(h.executionPromise) {
                h.executionPromise.catch(ex=>{
                   console.error("Task operation failed:", ex);
                })
              }

              return req.sendResponse({id: h.id});
           })
        }
        req.sendOk = function(prom) {
           const ok = "ok"
           if(!prom) return req.sendResponse(ok)
           return prom.then(()=>{
              return req.sendResponse(req.result || ok)
           })
        }
        req.sendPromResultAsIs = function(prom, toDelete, keepOnly) {
           return prom.then((d)=>{
               if(toDelete)
                  toDelete.forEach(q=>{
                   delete d[q]
                  })
               if(keepOnly)
                 Object.keys(d).forEach(q=>{
                    if(keepOnly.indexOf(q) < 0)
                      delete d[q]
                 })

              return req.sendResponse(d)
           })
        }

        connectionsCounter++;
        next()
      })

      if(!moduleOptions.NoMorgan) {
              // logging
            app.use(morgan('combined'))

             if(config.get("complete_log"))
             {

                var FileStreamRotator = require('mc-file-stream-rotator')

                // create a rotating write stream
                var accessLogStream = FileStreamRotator.getStream({
                  date_format: 'YYYYMMDD',
                  filename: path.join(config.get("complete_log") , 'access-%DATE%.log'),
                  frequency: 'daily',
                  verbose: false,
                  createWriteStreamOptions: {"flags":"a","mode":0o600}
                })


                morgan.token('rawBody', function getId(req) {
                  var re = (req.body || {}).raw || ""
                  if(!re) return ""
                  return removeSensitive(re)
                })
                morgan.token('result', function getId(req) {
                  if(!req.result) return ""
                  var re = JSON.stringify(req.result)
                  return removeSensitive(JSON.stringify(req.result))
                })

                // setup the logger
                app.use(morgan(':date[iso] :remote-addr :method :url :rawBody :status :result', {stream: accessLogStream}))

                function removeSensitive(re) {
                   return re.replace(/(privateKey|password|token)":"([^"]*)/g, function(str, group1, group2){ return `${group1}":"***` } )
                }
             }
       }


      app.use(isPathInListLogic("fullPublicApiPrefixes", "isPublic"))
      if(app.config.tokenapi)  {

         // we need this for global now()
         moment = require("MonsterMoment")

         // CORS support: MC-TICKET #14305
         const service_primary_domain = app.config.get("service_primary_domain");
         const server_full_hostname = app.config.get("server_name")+"."+service_primary_domain;
         app.use(function(req,res,next){
            if(req.method != "OPTIONS") return next();

            // we got a preflight request from proper origin.
            console.error("OPTIONS preflight request received!", req.path, req.headers);

            var origin = req.headers["origin"];
            if(!origin) throw new MError("ORIGIN_MISSING");
            var found = false;
            Array("https://", "http://", "").some(p=>{
              if(found) return true;
              Array(server_full_hostname, service_primary_domain).some(d=>{
                 var expected = p+d;
                 if(origin == expected){
                    found = true;
                    return true;
                 }
              })
            })
            if(!found)
              throw new MError("ORIGIN_INVALID");
            const corsResponseHeaders = {
                  'Access-Control-Allow-Origin': origin,
                  'Access-Control-Allow-Methods': "POST, DELETE, PUT, SEARCH, HEAD, GET",
                  'Access-Control-Allow-Headers': 'Content-Type, MonsterToken',
            };

            res.writeHead(200, corsResponseHeaders);
            return res.end("ok");
         });

         app.use(validateTokenBasedRequest);
      }
      else
         app.use(validateInternalRequest);


      // compiling raw request body
      app.use(isPathInListLogic("dontParseRequestBodyPrefixes", "dontParseBody"))
      app.use(bodyParser);

  }

  function isPathInListLogic(nameOfList, nameOfFlag) {
     return function(req,res,next){

        // the token based APIs might have some methods which can be called even without a token
        req[nameOfFlag] = isPathInList(req.path, pathes[nameOfList])

        next()

     }
  }




  function trailingMiddleware() {

    function shouldBeInternal(req,res,next){
        if(!req.validationInternal) return next(new MError("PERMISSION_DENIED"))
        next()
    }

    // valid response
    app.get("/ping", function(req, res, next) {
        req.sendResponse("pong")
    });

    // valid response
    app.get("/stats/connections", shouldBeInternal, function(req, res, next) {

        app.server.getConnections(function(error, current) {

            req.sendResponse({"all": connectionsCounter, "current": current })

        });

    });
    app.get("/stats/memory", shouldBeInternal, function(req, res, next) {

        app.server.getConnections(function(error, current) {

            req.sendResponse({"memoryUsage": process.memoryUsage() })

        });

    });

    if((app.commander)&&(app.commander.routerStats))
       app.use("/stats/tasks", shouldBeInternal, app.commander.routerStats)

    app.post("/gc", shouldBeInternal, function(req, res, next) {

        gc(req.body.json ? req.body.json.force : undefined)
        req.sendOk()
    })

    var editor = require("SimpleEditor")({baseDir: config.getVolumePath(), fileFilter: /\.json$/, exposeMainConfig: true});
    app.use("/editor", shouldBeInternal, editor.getRouter(app));


    // valid response
    app.use(function(req, res, next) {

      if (typeof req.result === 'undefined')
        return next()

      app.sendResponse(req, res, 200, req.result)
    });

    // exception handler
    app.use(function(err, req, res, next) {

      if((app.InsertException)&&(app.config.tokenapi)) {
         // we log exceptions in the relayer only (they were logged twice)
         // note: this is also needed since the backend services are stateless (they dont know about logins)
         app.InsertException(req, err);
      }

      console.error("Error stack:", err.stack);

      if(err.underlyingException)
         console.error(err.underlyingException)

      var merr = { "message" : (err instanceof MError ? err.message : "INTERNAL_ERROR") }

      if((err.errorParameters)&&((Object.keys(err.errorParameters).length)))
        merr["params"] = err.errorParameters

      app.sendResponse(req, res, 500, {"error": merr})
    });

  }

  app.sendResponse = function(req, res, respCode, re, forcePath)
  {
    var r = {}
    r["ts"] = new Date().unixtime()
    r["result"] = re;

    // this is for logging
    if(!req.result) req.result = re

    if((!app.config.tokenapi)||(req.validationInternal)) {
       // r["sig"] = app.calculateSignature(null, r["ts"], forcePath || req.path, re)      
    }

    if(req.additionalResponseHeaders)
        Object.keys(req.additionalResponseHeaders).forEach(key=>{
            res.append(key, req.additionalResponseHeaders[key])
        })

    res.status(respCode)
    res.json(r);
  }

  function doesPathMatch(path, prefix){
         if(prefix instanceof RegExp) {
            if(path.match(prefix)) {
               return true
            }
         }
         else
         if(path.indexOf(prefix) === 0)
         {
            return true
         }

         return false

  }

  function isPathInList(path, list){
      // the token based APIs might have some methods which can be called even without a token
      var matches = false
      list.some(function(prefix){
         if(doesPathMatch(path, prefix))
         {
           matches = true
           return true
         }

      })
      return matches
  }

  function validateTokenBasedRequest(req, res, next) {

      if(req.isPublic) return next()

      // even with token based apis, some requests (eg the service configuration ones) would be signature protected
      if((req.query.sig)||(req.headers[xMcSecretHeaderName]))
        return validateInternalRequest(req, res, next)

      var token = req.token = req.get("MonsterToken")
      if(!token)
         return next(new MError("MISSING_TOKEN"))


      return app.MonsterInfoSession.GetInfo(token)
        .then(userDetails=>{

          if((!userDetails.session.s_expires)||(userDetails.session.s_expires < moment.now()))
            return next(new MError("SESSION_EXPIRED"))

          if((!userDetails.account)||(!userDetails.account.u_id))
            return next(new MError("INTERNAL_ERROR")) // this should not happen, its just safety

          if((userDetails.session.c_server)&&(server_name != userDetails.session.c_server))
            return next(new MError("CREDENTIAL_NOT_ENTITLED_TO_THIS_SERVER"));


          req.TokenUser = userDetails

          setupFunction();

          function setupFunction(){
              if(req.TokenUser.session.IsGranted) return;

              // lets setup the helper functions and do it only once

              req.TokenUser.refetchToken = function(){
                 req.TokenUser.uncache();
                 return app.MonsterInfoSession.GetInfo(token)
                   .then(userDetails=>{
                      req.TokenUser = userDetails;
                      setupFunction();
                   })
              }

              req.TokenUser.uncache = function(){
                 app.MonsterInfoSession.Delete(token);
              }

              req.TokenUser.session.IsGranted = function(grantCandidate) {
                return req.TokenUser.session.IsAnyGranted([grantCandidate])
              }
              req.TokenUser.session.IsAnyGranted = function(grantCandidate) {
                 var g = grantCandidate
                 if(arguments.length > 1) g = arguments
                 if(!Array.isArray(g)) g = [grantCandidate]
                 for(var q of g) {
                    if(-1 < req.TokenUser.session.s_grants.indexOf(q))
                      return true
                 }
                 return false
              }
              req.TokenUser.session.IsAllGranted = function(grantCandidate) {
                 var g = grantCandidate
                 if(arguments.length > 1) g = arguments
                 if(!Array.isArray(g)) g = [grantCandidate]
                 if(g.length <= 0) return false
                 for(var q of g) {
                    if(-1 >= req.TokenUser.session.s_grants.indexOf(q))
                      return false
                 }
                 return true
              }
              req.TokenUser.session.IsSuperUser = function() {
                 return req.TokenUser.session.IsAnyGranted("SUPERUSER")
              }
              req.TokenUser.session.IsAnyKindOfAdmin = function() {
                 for(var q of req.TokenUser.session.s_grants) {
                    if(q == "SUPERUSER")
                      return true
                    if(q.match(/^ADMIN_/))
                      return true
                 }
                 return false
              }
          }


          req.validationToken = true

          next()

        })
        .catch(err=>{
          return next(new MError("INVALID_TOKEN_OR_SESSION_EXPIRED",null, err))
        })



  }

  function validateInternalRequest(req, res, next) {

    if(req.isPublic) return next()

    const secret = req.headers[xMcSecretHeaderName];
    if(secret) {
        var apiSecret = config.getApiSecret();
        if(secret != apiSecret)
        {
           return next(new MError("SEC_INVALID_SECURITY_HEADER"))
        }

        // all good
        req.validationInternal = true
        return next();
    }

    var sig = req.query.sig;
    if(!sig)
       return next(new MError("SEC_MISSING_SIGNATURE"))

    var ts = req.query.ts;
    if(!ts)
       return next(new MError("SEC_TIMESTAMP_MISSING"))
    if(!String(ts).match(/^\d+$/))
       return next(new MError("SEC_TIMESTAMP_INVALID"))

    var now = new Date().unixtime();
    if(now - ts > config.get("request_validity_sec")) {
       console.error("Request too old.", {now: now, ts: ts});
       return next(new MError("SEC_REQUEST_TOO_OLD"));      
    }



    const hmac = config.getHmac();
    if(!hmac)
       return next(new MError("API_SECRET_NOT_CONFIGURED"))

    hmac.update(ts+ " "+ req.path+" ");

    req.specialBodyParser = function(chunk) {
       if(chunk === null) {
            var digest = hmac.digest('hex');

            if(digest != sig) {
               console.error(util.format("Invalid signature received (%s instead of %s)", sig, digest))
               if(process.env.NODE_ENV != "development")
                  return new MError("SEC_INVALID_SIGNATURE")
            }

            req.validationInternal = true

            return
       }

       hmac.update(chunk);
    }

    next()

  }

  headingMiddleware()
  function startServer(doNotHandleSignals) {

      trailingMiddleware()

      return new Promise(function(res,rej){

            app.server = app.listen(config.get("listen_port"), config.get("listen_host"), config.get("listen_backlog"), function () {
            app.server.timeout = 0

            if((!doNotHandleSignals)) {//&&(!/^win/.test(process.platform))
               function ex(){
                  console.log("Signal received, exiting")
                  process.reallyExit();
               }
               process.on("SIGINT", ex)
               process.on("SIGTERM", ex)
            }


            var re = {"server": app.server}
            re.host = app.server.address().address
            re.port = app.server.address().port

            console.log("MonsterExpress is listening at http://%s:%s", re.host, re.port)

            app.config.writePid()

            return res(re)
          })


      })

  }

  app.PromiseRouter = function(requestPath) {
     if(!requestPath) requestPath = "/"

     var router = app.ExpressPromiseRouter();

     function registerPath(pathListName, args) {
         if(!Array.isArray(args)) args = [args]
         pathes[pathListName] = pathes[pathListName].concat( args.map((x)=>{
           if(x instanceof RegExp) return x
           return requestPath+x
         }) )
     }

     router.RegisterDontParseRequestBody=function(args){
         registerPath("dontParseRequestBodyPrefixes", args)
     }

     router.RegisterPublicApis=function(args){
         registerPath("fullPublicApiPrefixes", args)
     }

     router.ServersMiddleware = function(serverName, maxServers){
       var fn = require(path.join(__dirname,"servers.js"))
       var serversRouter = fn(config, serverName, maxServers)
       router.use("/service/"+serverName, serversRouter)
       return serversRouter
     }

     app.use(requestPath, router)

	  // this stuff will be useful for internal microservices that are willing to send some IPC requests to another node
	  app.ServersRelayer = router.ServersMiddleware("relayer", 0);

     if(!app.config.tokenapi) {

          var poolCache
          app.GetRelayerMapiPool = function(){
             if(!poolCache)
                poolCache = app.Mapi.getMapiPoolCached(app.ServersRelayer)

             return poolCache
          }

     }

      app.MonsterMailer = require("MonsterMailClient")({server:app.ServersRelayer})
      app.GetMailer = function() {
         return app.MonsterMailer
      }

      app.EscalateEvent = function(type, params, underlyingException, req) {
         return app.InsertException(req, extend({message:type, }, params), underlyingException);
      }

      app.InsertException = function(req, params, underlyingException) {
           var e_other = extend({},params);
           if(req) {
              e_other.req = {
                path: req.path,
                method: req.method
              };
           }
           
		       if(underlyingException) e_other.exception = underlyingException;

           return app.InsertEvent(req, {
             e_event_type: "exception",
             e_other: e_other,
           });
      }

     if(moduleOptions.subsystem) {
         var eventInternalApi = Array("accountapi","relayer").indexOf(moduleOptions.subsystem) > -1 ? true : false;
         app.ServersEvent =  eventInternalApi ? router.ServersMiddleware("eventapi", 0) : app.ServersRelayer;
         var EventClientLib = moduleOptions.EventClientLib || require("MonsterEventClient")
         app.MonsterEvent = EventClientLib({eventInternalApi: eventInternalApi, subsystem: moduleOptions.subsystem, server:app.ServersEvent})

          app.GetEvent = function() {
             return app.MonsterEvent
          }

          app.InsertEvent = function(req, d, dontSwallowException) {
           if(req){
              if(!d.e_ip) d.e_ip = req.clientIp;
              if(!d.e_agent) d.e_agent = req.userAgent
              if(!d.e_user_id) {
                 if(!req.headers) req.headers = {};
                 d.e_user_id = (req.TokenUser && req.TokenUser.account) ? req.TokenUser.account.u_id : req.headers['monsteraccountid'];
              }
                 

              if(d.e_other === true)
                d.e_other = extend({}, req.body.json, req.params);
              if(typeof d.e_other == "undefined")
                d.e_other = req.params;

           }
           else if(d.e_other === true)
             delete d.e_other;


           return app.GetEvent().LogAsync(d).catch(ex=>{
              console.error("Couldnt log event:", d, ex)
              if(dontSwallowException)
                 throw ex
           })

        }

     }

     if(!app.InsertEvent)
        app.InsertEvent = function(){};


     return router
  }


  app.Prepare = function() {
      return Promise.resolve()
  }
  app.Cleanup = function() {
      return Promise.resolve()
  }


  app.calculateSignature = function(secret, ts, path, payload) {

    const hmac = config.getHmac(secret)
    if(hmac) {
      hmac.update(ts+ " "+ path +" ");
      hmac.update(util.isString(payload) ? payload : JSON.stringify(payload) );
      return  hmac.digest('hex');
    }
  }

  app.Start = function(flag) {
     var xargs = arguments

     return app.Prepare()
       .then(function(){

          if(app.InsertEvent) app.InsertEvent(null, {e_event_type: "startup"})

          return startServer.apply(null, xargs)
       })
  }

  return app
}


