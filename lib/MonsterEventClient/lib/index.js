module.exports = function(options) {
    "use strict"

    const MError = require("MonsterError")
    if(typeof(options) !== "object") throw new MError("INVALID_PARAM")
    if(!options.server) throw new MError("MISSING_SERVER")

    var MonsterClientLib = require("MonsterClient")
    var poolCache;

    var re = {};

    re.LogAsync = function(details){
        return Promise.resolve()
          .then(()=>{
                if(typeof(details) !== "object") throw new MError("INVALID_PARAM")
                // IP is not required! (think about events like startup eg)
                // if(!details.e_ip) return callback(new MError("MISSING_IP"))
                if(!details.e_event_type) throw new MError("MISSING_EVENT_TYPE")
                if(!details.e_subsystem) details.e_subsystem = options.subsystem
                if(!details.e_subsystem) throw new MError("MISSING_SUBSYSTEM")


                var uri = "/eventapi/events";
                var mapi = GetRelayerMapiPool();
                if(mapi.getApiType() == "public")
                   uri = "/s/[this]"+uri;

                return mapi.putAsync(uri, details);
          })
    }

    return re;


    function GetRelayerMapiPool(){
      if(!poolCache) {
        if(options.eventInternalApi)
          poolCache = MonsterClientLib.getMapiPool(options.server);        
        else
          poolCache = MonsterClientLib.getMapiPoolCached(options.server);        
      }

      return poolCache
    }


}
