#!/bin/bash

set -e

MAIN_DIR=/opt/MonsterCommon/lib/

for i in $MAIN_DIR/*
do
  if [ -d "$i" ]; then
    cd "$i"
    npm install --production
  fi
done
