#!/bin/bash

set -e

for i in /opt/MonsterCommon/lib/*/test
do
  cd "$i/.."
  mocha
done
