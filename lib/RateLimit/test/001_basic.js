require( "../../MonsterDependencies")(__dirname)

//var app = require("../testapp.js")

var config = require('MonsterConfig');
var me = require('MonsterExpress');
config.appRestPrefix = ""

var app = me.Express(config);

var RateLimit = require("RateLimit")


var limiter = new RateLimit({
  windowMs: 15*60*1000, // 15 minutes 
  max: 3, // limit each IP to 100 requests per windowMs 
});

app.use(limiter)
app.get("/foo", function(req,res,next){
	req.result = "ok"
	next()
})


require("ExpressTester")(app, function(mapi, assert){
	describe('rate limit', function() {
	    it('basic rate limit', function() {


	        return assert.isFulfilled(
	                mapi.getAsync("/foo")
	                .then((res)=>{
	                	assert.equal(limiter.query("::ffff:127.0.0.1"), 2)
		             	assert.equal(res.httpResponse.statusCode, 200)
		             	return mapi.getAsync("/foo")
	                }))
	                .then((res)=>{
	                	assert.equal(limiter.query("::ffff:127.0.0.1"), 3)
		             	assert.equal(res.httpResponse.statusCode, 200)
		             	return mapi.getAsync("/foo")
	                })
	                .catch((ex)=>{
	                	assert.equal(limiter.query("::ffff:127.0.0.1"), 4)	                	
		             	assert.equal(ex.message, "RATE_LIMIT_REACHED")
	                })

        })
    })
})
