// should do app.enable("trust proxy") if the app is behind a reverse proxy

var RateLimit = require('express-rate-limit')
var MemoryStoreQuery = require('./memory-store-with-query.js');
const MError = require("MonsterError")
module.exports = function(config){	

	if(typeof config.delayMs === "undefined")
		  config.delayMs = 0
	config.store = new MemoryStoreQuery(config.windowMs)
	config.handler = function(req,res,next){
       next(new MError("RATE_LIMIT_REACHED"))
	}
	config.keyGenerator =  function(req) {
	    return req.clientIp || req.ip;
	}
	var re = new RateLimit(config)
    re.query = function(key) {
    	return config.store.query(key)
    }
	return re
}
