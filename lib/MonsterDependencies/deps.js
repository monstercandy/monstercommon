(function(){
    const amp = require('app-module-path')

    module.exports = function(callerDir) {
        amp.addPath(__dirname + '/../'); // MonsterCommon

    	// these two should(?) be removed in the future:
    	amp.addPath(callerDir + '/../'); // project specific common libs
    	amp.addPath(callerDir + '/../node_modules');

    	amp.addPath(callerDir); // and local dir
    }

    global.simpleCloneObject = function(src){
      return JSON.parse(JSON.stringify(src));

    }

    global.extend = function(target) {
        var sources = [].slice.call(arguments, 1);
        sources.forEach(function (source) {
            for (var prop in source) {
                target[prop] = source[prop];
            }
        });
        return target;
    }

    if(!global.Object.values) {
         global.Object.values = function (obj) {
            var vals = [];
            for( var key in obj ) {
                if ( obj.hasOwnProperty(key) ) {
                    vals.push(obj[key]);
                }
            }
            return vals;
        }
    }

    if(!console.inspect){
        console.inspect = function(){
             const util = require("util");
             var inspectedArguments = Object.values(arguments).map(x => util.inspect(x, false, null));
             console.log.apply(null, inspectedArguments);
        }
    }

    if((process.argv[1])&&(!process.argv[1].match(/_mocha$/))) {
       reconfigureConsoleLog()
    }

    function reconfigureConsoleLog(){
        Array("log","warn","info","error").forEach(cat=>{
            var key = "["+cat.toUpperCase()+"]"
            var originalFn = console[cat]
            console[cat] = function(){
                var o = [].slice.call(arguments)
                o.unshift(new Date().toISOString(), key)

                originalFn.apply(this, o)
            }
        })
    }

})()

