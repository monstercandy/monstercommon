
var dotqLib = require( "../deps.js")
var assert = require("chai").assert

describe('extending', function() {    
    "use strict"

    it('extending', function() {

        var re = extend({a:"b"}, {c:"d"})
        assert.deepEqual(re, {a:"b",c:"d"})

    })

    it('override extending', function() {

        var re = extend({a:"b"}, {a:"c"})
        assert.deepEqual(re, {a:"c"})

    })

    it('extending undefined', function() {

        var re = extend({a:"b"}, undefined, {c:"d"})
        assert.deepEqual(re, {a:"b",c:"d"})

    })

    it('calling console.inspect', function(){

             console.inspect(123, "xcv", ["foo","bar"], {"foo": "bar"});
    })

})
