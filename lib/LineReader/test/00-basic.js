// require("../../MonsterDependencies")

var assert = require("chai").assert

describe('simple', function() {    
    "use strict"

    var reader = require( "../reader.js")

    it('get first line', function(done) {

         reader("test/test-3lines.txt", function(err, first) {
         	 if(err) return done(err)

         	 assert.equal(first, "first")
             done()
         })

    })

    it('get with the rest', function(done) {

         reader("test/test-3lines.txt", function(err, first, rest) {
         	 if(err) return done(err)

         	 assert.equal(first, "first")
         	 assert.equal(rest, "second\nthird\n")
             done()
         })

    })

    it('get with async', function(done) {

         reader.Async("test/test-3lines.txt")
         .then(line => {
         	 assert.equal(line, "first")
             done()
         })
         .catch(done)

    })

    it('get with async and rest', function(done) {

         reader.Async("test/test-3lines.txt", true)
         .then(l => {
         	 assert.equal(l.first, "first")
         	 assert.equal(l.rest, "second\nthird\n")
             done()
         })
         .catch(done)

    })

    it('get empty file', function(done) {

         reader("test/test-empty.txt", function(err, first) {
         	 if(err) return done(err)

         	 assert.equal(first, null)
             done()
         })

    })
})

