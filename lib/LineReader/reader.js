var ofn = module.exports = function(filename, readyCallback) {

        const readline = require('readline');
        const fs = require('fs');

        var fd
        var closedAlready
        const stream = fs.createReadStream(filename)
        stream.on('open', function (aFd) {
            fd = aFd
            // console.log("opened with fd: ", fd)
        })
        stream.on('error', function (err) {
            if(closedAlready) return
            // console.log("here?")
            readyCallback(err)
        });

        const rl = readline.createInterface({
          input: stream
        });

        var lineRead = false
        var first = null
        var rest = null
        rl.once('close', () => {
            readyCallback(null, first, rest)
        })

        rl.on('line', (line) => {

             if(first === null) {
                first = line
                if(readyCallback.length <= 2) // rest is not wanted
                {
                    // console.log("closing stuff", fd)                    
                    closedAlready = true
                    fs.closeSync(fd)
                    return rl.close()
                }
             }
             else {
                if(rest === null) rest = ""

                rest += line + "\n"
             }

        });

}

ofn.Async = function(filename, wantsTheRest) {
    return new Promise(function(resolve, reject) {
        function cbFull(err, first, rest) {
            if(err) return reject(err)

            resolve({first: first, rest: rest})
        }
        function cbMin(err, first) {
            if(err) return reject(err)
            resolve(first)
        }
        ofn(filename, wantsTheRest? cbFull : cbMin)
    }) 
}
