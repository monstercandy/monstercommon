require( "../MonsterDependencies")(__dirname)

const TEST_LISTEN_PORT = 52123
const express = require('express');
const commanderLib = require( "lib/index.js")

var app = express();
var commander = commanderLib({secure_task_id: false, routerFn:express.Router})

app.all('/gc', function(req,res,next){
  console.log("doing garbage collection")
  gc(true)
  res.send(process.memoryUsage())
});
app.use('/tasks', commander.router);

var server = app.listen(TEST_LISTEN_PORT);

var counter = 0;
spawnNext()

function spawnNext() {
  counter++
  var id = "foo"// +counter
  console.log("spawning next:", id)
  return commander.spawn({removeImmediately:true,upload:true,task_id:id,chain:{executable: "perl", args: ["test/echo.pl"]}, triggerOnRead: true})
    .then(h=>{
	   return h.executionPromise
	})
	.catch(ex=>{
	   console.log("execution failed:", ex)
	})
	.then(()=>{
	   return spawnNext()
	})
}


