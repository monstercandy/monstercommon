var lib = module.exports = function(options){
	if(!options) options = {}

	var re = {}

	var stuffs
	var sumLength

    re.clear = function(){
		cachedString = null
    	stuffs = []
    	sumLength = 0
    }
	re.set = function(stringOrBuffer, onTruncate) {
		re.clear()
		return re.push(stringOrBuffer, onTruncate)
	}

	function itemRemoved(removedItem, onTruncate){
		sumLength -= removedItem.length
		if(options.onTruncate)
			options.onTruncate(removedItem)
		if(onTruncate)
			onTruncate(removedItem)
	}

	re.push = function(stringOrBuffer, onTruncate){
		cachedString = null
		var alength = stringOrBuffer.length
		if((options.maxLength)&&(sumLength+alength > options.maxLength)) {
			var oLength = stuffs.length
			for(var i = 0; i < oLength / 2; i++) {
				itemRemoved(stuffs.shift(), onTruncate)
			}

			while((stuffs.length)&&(sumLength+alength > options.maxLength)) {
				itemRemoved(stuffs.shift(), onTruncate)
			}

            // skip data?...
			if(sumLength+alength > options.maxLength)
				return

		}
		sumLength += alength
		stuffs.push(stringOrBuffer)
	}

	re.getSize = function(){
		return sumLength
	}

	re.asBuffers = function(){
		return stuffs
	}

	re.toString = re.asString = function(){
		if(cachedString) return cachedString
		var re = ""
		stuffs.forEach(stuff=>{
			re += stuff
		})
		return re
	}

	re.indexOf = function(){
		var str = re.toString()
		return String.prototype.indexOf.apply(str, arguments)
	}


	re.clear()

	return re

}
