
var lib = module.exports = function(moduleOptions){
  if(!moduleOptions) moduleOptions = {}

  if(!moduleOptions.read_timeout_ms) moduleOptions.read_timeout_ms = 10000
  if(!moduleOptions.try_to_execute_interval_ms) moduleOptions.try_to_execute_interval_ms = 5000
  if(!moduleOptions.max_tasks_simultaneously) moduleOptions.max_tasks_simultaneously = 1
  if(!moduleOptions.max_task_output_bytes) moduleOptions.max_task_output_bytes = 100*1024 // 100 kbytes
  if(!moduleOptions.lifesignal_during_execution_seconds) moduleOptions.lifesignal_during_execution_seconds = 30 // sending some data to the readers after 30seconds by default
  if(!moduleOptions.remove_task_output_after_seconds) moduleOptions.remove_task_output_after_seconds = 900
  if(!moduleOptions.remove_task_after_seconds) moduleOptions.remove_task_after_seconds = 1800
  if(!moduleOptions.download_idle_timeout) moduleOptions.download_idle_timeout = 60000
  if(typeof moduleOptions.secure_task_id == "undefined") moduleOptions.secure_task_id = true

  const bufferQueueLib = require("./bufferqueue.js")
  const child_process = require('child_process');
  const TokenLib = require("Token")

  var tasksAddedSoFar = 0
  var tasksExecutingCurrently = 0
  var tasksAllExecutingCurrently = 0
  var tasks = {}



  var routerStats
  var router
  if(moduleOptions.routerFn) {
      routerStats = moduleOptions.routerFn()
      routerStats.get("/", function(req,res,next){
         var re = []
         Object.keys(tasks).forEach(task_id=>{
            var task = tasks[task_id]
            var a = {}
            Array("task_id","commandLine","executing","finished","aborted","triggerOnRead","removeImmediately","maxTriggers","commandChainIndex").forEach(x=>{
               if(typeof task[x] == "undefined") return
               a[x] = task[x]
            })
            a.readers = task.readers.length
            a.requests = task.requests.length
            re.push(a)
         })
         // console.dir(re)
         req.sendResponse(re)
      })

      router = moduleOptions.routerFn()
      const corsResponseHeaders = {
            'Content-Type': 'text/html',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'content-type',
          }

      router.use(function(req,res,next){

         req.isInputSender = function(){
            return ((req.task) && (req.task.inputSender) && (req == req.task.inputSender.request))
         }
         req.isOutputReader = function(){
            return ((req.task) && (req.task.outputReader) && (req == req.task.outputReader.request))
         }

         res.do_sendCode = function(code){
            var headers = dummyCloneObject(corsResponseHeaders)
            if(req.task) {
                if(req.task.downloadAs) {
                    headers["Content-Type"] = "application/binary"
                    headers["Content-Disposition"] = "attachment; filename=\""+req.task.downloadAs+"\""
                }
                if(req.task.downloadMimeType)
                    headers["Content-Type"] = req.task.downloadMimeType
            }

            res.do_writeHead(code, headers)
         }
         res.do_sendStatusAndFinish = function(code, message){
            res.do_sendCode(code)
            res.do_write(message+"\n")
            res.do_end()
         }
         res.do_writeHead = function(code, headers){
            if(res.ended) return
            if(res.headers_sent) return
            res.headers_sent = true
            console.log("sending http response code", code, headers)
            res.writeHead(code, headers)
         }
         res.do_write = function(data){
            if(!data) return
            if(res.ended) return

            if(!res.headers_sent) res.do_sendCode(200)
            if(!data.asBuffers) return res.do_low_level_write(data)

            data.asBuffers().forEach(buffer=>{
               res.do_low_level_write(buffer)
            })
         }
         res.writesInProgress = 0
         res.do_low_level_write = function(buffer){
             res.writesInProgress++
             res.write(buffer, function(err){
                if(err)
                   console.error("do_low_level_write: error during write", err)

                res.writesInProgress--
                if(((!res.writesInProgress)&&(res.ended))||(err))
                   res.do_low_level_end()
             })
         }
         res.do_end = function(){
            if(res.ended) return
            console.log("ending response")
            res.ended = true
            if(!res.writesInProgress)
              res.do_low_level_end()
         }

         res.do_low_level_end = function(){
            console.log("ending response really")
            res.end()
            onRequestEnd(req,res)
         }

         next()
      })

      function onRequestEnd(req,res) {
        if(req.requestEnded) return
        if(!req.task) return

        console.log("onRequestEnd", req.task.task_id)

        req.requestEnded = true

        if(req.stdinBound) {
            console.log("request stdin was bound")
        }
        else
        if((req.task.download)||(req.task.upload)){
          // we dont abort the task if stdin was bound, since the child stdin is closed anyway and it will trigger shutting down the running application

          // we dont abort the task if the socket we just lost is just an output reader (and not in download mode); the task should go on!
          if((req.task.download)||(!req.isOutputReader))
             re.abortTask(req.task.task_id)
        }

        var index = req.task.requests.indexOf(req);
        if(index != -1)
            req.task.requests.splice( index, 1 );

        index = req.task.readers.indexOf(res);
        if(index != -1)
            req.task.readers.splice( index, 1 );

      }

      Array("", "/output", "/input").forEach(cat=>{
        var url = `/:task_id${cat}/abort`;
        router.get(url, findTask(true), abortTask)
        router.post(url, findTask(true), abortTask)
      })

      router.options(/.*/, function(req,res,next){
          console.log("answering options")
          res.do_writeHead(200, corsResponseHeaders)
          res.do_end()
      })
      router.get("/:task_id", findTask(), fetchTask)
      router.post("/:task_id", findTask(), fetchTask)

      function saveReqRes(key, err) {
        return function(req,res,next){
          if(!req.task.upload) throw new Error("NOT_AN_UPLOAD_TASK")
          if((key=="inputSender")&&(req.task[key])) throw new Error(err+"_ALREADY_BUSY")
          if((key=="outputReader")&&(req.task[key])) {
             // aborting the previous outputreader, as there should be only one.
             req.task[key].response.do_end()
          }
          req.task.doubleRequestMode = true
          req.task[key] = {request: req, response: res}
          next()
        }
      }

      router.get("/:task_id/output", findTask(), saveReqRes("outputReader", "OUTPUT"), fetchTask)
      router.post("/:task_id/input", findTask(), saveReqRes("inputSender", "INPUT"), fetchTask)

      router.get("/:task_id/trigger", findTask(), triggerTask)
      router.post("/:task_id/trigger", findTask(), triggerTask)

      function abortTask(req, res, next) { // this one should list the web storages which have currently authentication configured
          console.log("abort request received for", req.params.task_id, req.task.commandLine)

           if(req.task.nonAbortable) {
             res.do_sendStatusAndFinish(200, "This task cannot be aborted.")
             return
           }


          re.abortTask(req.task.task_id)

          res.do_sendStatusAndFinish(200, "ok")

      }
      function replaceRequestAndResponseForTask(req,res) {

          if(!req.task.doubleRequestMode) {

            req.task.readers.forEach(reader=>{
               reader.do_end()
            })

            req.task.requests=[]
            req.task.readers=[]
          }

          req.task.requests.push(req)
          req.task.readers.push(res)
      }
      function fetchTask(req, res, next) { // this one should list the web storages which have currently authentication configured
          console.log("task reader connected for", req.params.task_id, req.task.commandLine, req.task.doubleRequestMode, req.isInputSender(), req.isOutputReader())

          if(!req.isOutputReader())
             cleanInvocationTimeout(req.task)

          if(req.isOutputReader()) {
             // we send the headers immediately (as the frontend waits on them to initiate the uploading request)
             res.do_write("::: output reader attached\n")
          }

          if(!req.isInputSender()) {
            // console.log("!!!!!!!!!!! sending task output so far", req.isInputSender(), req.isOutputReader())
            res.do_write(req.task.output)
          }

          if(req.task.finished) {
              res.do_end()
              return
          }

          setupPendingSignal()

          if(req.task.download){
             // this is needed in advance so the child process can just start dumping the output to the socket
             res.do_sendCode(200)
          }

          replaceRequestAndResponseForTask(req, res)


          Array("close","error", "end").forEach(c=>{
              req.on(c, function(p){
                console.log("req event for", c, p, req.task.task_id, req.task.commandLine)

                if(c != "end") // this is purposefully here, dont remove
                  res.do_end()

              })
          })
          Array("close","error","end").forEach(c=>{
              req.socket.on(c, function(p){
                console.log("req socket event for", c, p, req.task.task_id, req.task.commandLine)

                if((c == "close")&&(req.task.download)&&(!req.task.aborted)&&(req.task.executing)) {
                  // This might be happen earlier than how the process exits, so we need to handle this situation as normal
                  console.log("This is a download task, where the leecher script just disconnected.")
                  req.task.dontSetAborted = true;
                }

                res.do_end()

              })

          })


          // this is an upload task and a request has just been attached to it. this should kick off spawning the child process
          if((req.task.upload)||(req.task.download)) {
            tryToExecuteTask()
          }


          function setupPendingSignal() {
            if(req.task.download) return
            if(req.task.aborted) return
            if(req.task.executing) return
            if(!moduleOptions.lifesignal_during_execution_seconds) return

            setTimeout(function(){
                 if(req.task.finished) return
                 if(req.task.executing) return
                 console.log("sending task execution is pending...")
                 res.do_write('::: task execution is pending :::\n')
                 setupPendingSignal()
              }, moduleOptions.lifesignal_during_execution_seconds * 1000)
          }

      }


      function triggerTask(req,res,next){
         if(!req.task.parentTask) {
              res.do_sendStatusAndFinish(200, "This task cannot be triggered.")
              return
         }

          res.do_writeHead(200, corsResponseHeaders)
          res.do_write(JSON.stringify({task_id: req.task.task_id}))
          res.do_end()

      }

      function findTask(abort){

         return function(req,res,next) {


             if(!tasks[req.params.task_id]) {
              res.do_sendStatusAndFinish(200, "Task not found.")
              return
             }

             req.task = tasks[req.params.task_id]

             if(!req.task.triggerOnRead)
               return next()

             console.log("this is a triggerOnRead task")

             if(req.task.childTask) {
               console.log("it has a chiltask already, redirecting to it")
               req.task = req.task.childTask
               return next()
             }

             if(abort) {
               res.do_sendStatusAndFinish(200, "Task is not running, nothing to abort.")
               return
             }


             // should redirect it
             if((req.task.maxTriggers != -1)&&(req.task.maxTriggers <= req.task.triggerCounter)) {
                console.log("cannot be triggered anymore")
                res.do_sendStatusAndFinish(200, "This task can be triggered no more.")
                return
             }

             console.log("creating a new instance of the task")

             req.task.triggerCounter++

             cleanInvocationTimeout(req.task)

             var childTask = dummyCloneObject(req.task)
             delete childTask.task_id
             delete childTask.triggerOnRead
             delete childTask.maxTriggers
             childTask.is_cloned_task = true
             childTask.parentTask = req.task;

             addTaskToQueue(childTask)
               .then(h=>{
                  // console.log("!adding new tsk to the queue", req.task)
                  req.task.triggerCallbacks.forEach(tc=>{
                    tc(h)
                  })


                  var realTask = tasks[h.id]
                  console.log("! parent task is:", req.task.task_id)
                  console.log("! child task is:", h.id)
                  req.task.triggeredChildIds.push(h.id)
                  req.task.childTask = realTask
                  realTask.parentTask = req.task
                  req.task = realTask

                  // redirecting it to the child...
                  req.params.task_id = realTask.task_id

                  return next()
               })

         }
      }

  }

  function cleanInvocationTimeout(task) {
     if(!task.invocationTimer) return

     console.log("cleaning up invocationTimer", task.task_id)
     clearTimeout(task.invocationTimer)
     task.invocationTimer = null
  }


  function setupInvocationTimeout(task) {

        if(!task.invocationTimeout) return

        console.log("configuring an invocation timer", task.task_id)

        task.invocationTimer = setTimeout(function(){

           console.log("invocation timer fired, aborting!", task.task_id)

           re.abortTask(task.task_id)
           re.setupTaskRemoval(task)

        }, task.invocationTimeout)
  }

  var re = {}
  re.router = router
  re.routerStats = routerStats

  re.EventEmitter = function(){
     return lib.EventEmitter(re)
  }

  re.getTaskResult = function(task_id) {
     var task = tasks[task_id]
     if(!task) return {}
     return {code: task.code, output: task.output}
  }

  re.changeTaskChain = function(task_id, newChain){
     var task = tasks[task_id]
     if(!task) throw new Error("Task not found")
     if(!task.triggerOnRead) throw new Error("This task is not triggerOnRead, cannot be changed")
     task.chain = newChain
     setCommandLine(task)
  }

  re.abortTask = function(task_id) {
     var task = tasks[task_id]

     console.log("abortTask", task_id)

     // the order is important here, dont touch
     if(!task) return
     if(task.aborted) return
     if(task.finished) return

     if(task.dontSetAborted) return;

     task.aborted = true
     task.doReject(new Error("Timeout or aborted: "+task_id))      


     // we send response header to the connected readers (if they have already received them, this wont be sent out to them agian)
     task.readers.forEach(res => {

        res.do_sendCode(500)

     })



     if(!task.executing) {

          // this is a special case, we have some readers, they are waiting for the command to be actually started
          // we end up waiting readers (they havent received any response so far)

             task.readers.forEach(res => {

                res.do_end()

             })


             return
     }


     if((task.child) && (task.child.kill)) {
          console.log("killing child")

          task.child.kill()
          if((task.child.pid)&&(task.spawnOptions)&&(task.spawnOptions.detached)) {
            try{
               console.error("trying to kill the complete process group", -task.child.pid )
               process.kill( -task.child.pid )
            }catch(ex){

            }
          }
     }

  }
  re.deleteTask = function(task_id){

     var task = tasks[task_id]
     if(!task) return

     console.log("deleting task", task_id)

     if((task.parentTask) && (task.parentTask.triggerCounter >= task.parentTask.maxTriggers)){

         console.log("removing parent task as well, no more triggers available", task.parentTask.task_id)
         re.deleteTask(task.parentTask.task_id)
     }

     re.abortTask(task_id)
     /*
     // freeing request/response references explicitly: TODO: is this neccessary?
     tasks[task_id].readers = []
     tasks[task_id].requests = []
     */
     delete tasks[task_id]
  }

  re.spawn = function(taskOptions, replacements){
      if(typeof taskOptions != "object") throw new Error("Options should be a hash")
      var task = dummyCloneObject(taskOptions)

      if((task.chain)&&(task.command)) throw new Error("You cannot have both chain and single command spec")
      if(!task.chain) {

        task.chain = [{
           executable: task.command || task.executable, args: task.args, spawnOptions: task.spawnOptions
        }]

        delete task.command
        delete task.args
        delete task.spawnOptions
      }

      if((task.task_id)&&(tasks[task.task_id])) throw new Error("We have already got a task with this id")

      if(replacements)
        task.chain = require("Replacer")(task.chain, replacements)

      return addTaskToQueue(task)
  }

  re.getTaskIds = function(){
      return Object.keys(tasks)
  }
  re.getUnfinishedTaskIds = function(){
      var re = []
      Object.keys(tasks).forEach(task_id=>{
          var task = tasks[task_id]
          if(task.finished) return
          if(task.aborted) return
          re.push(task_id)
      })
      return re
  }


  re.setupTaskRemoval = function(task){

        if(!task.removeImmediately) {
            if(moduleOptions.remove_task_output_after_seconds != moduleOptions.remove_task_after_seconds) {

              if(task.remove_output_timeout)
                clearTimeout(task.remove_output_timeout)

              task.remove_output_timeout = setTimeout(function(){
                 console.log("removing command output", task.task_id, task.commandLine)
                 task.output.set(task.executionResult)
              }, moduleOptions.remove_task_output_after_seconds * 1000)

            }

            if(moduleOptions.remove_task_after_seconds) {
              if(task.remove_task_timeout)
                clearTimeout(task.remove_task_timeout)

              remove_task_timeout = setTimeout(function(){
                 console.log("removing command", task.task_id, task.commandLine)
                 re.deleteTask(task.task_id)

              }, moduleOptions.remove_task_after_seconds * 1000)

            }
        }
        else
          re.deleteTask(task.task_id)

  }

  setInterval(tryToExecuteTask, moduleOptions.try_to_execute_interval_ms)

  return re

  function setCommandLine(task) {
      if(!Array.isArray(task.chain))
        task.chain = [task.chain]

      task.chain.forEach(chain => {

        if((!chain.callback)&&(!chain.executable)) throw new Error("No executable specified")
        if(!chain.args) chain.args = []
        if(!chain.spawnOptions) chain.spawnOptions = {}

        if((!task.upload)&&(!chain.stdin)) chain.stdin = "" // for closing it immediately else some apps might hang

        chain.commandLine = (chain.executable +" "+ chain.args.join(" ")).trim()

      })

      task.commandLine = task.chain[0].commandLine + (task.chain.length > 1 ? " (+"+(task.chain.length-1)+" others...)" : "")
  }

  function addTaskToQueue(task) {

      setCommandLine(task)

      tasksAddedSoFar++
      var p
      if(!task.task_id)
        p = moduleOptions.secure_task_id ? TokenLib.GetTokenAsync(16, "hex") : Promise.resolve(tasksAddedSoFar)
      else
        p = Promise.resolve(task.task_id)

      return p.then(id=>{
             task.task_id = id
             task.output = bufferQueueLib({maxLength: moduleOptions.max_task_output_bytes})

             task.executing = false
             task.finished = false
             task.aborted = false

             task.requests = []
             task.readers = []
             task.commandChainIndex = 0
             if(!task.triggerCallbacks)
               task.triggerCallbacks = []

             task.triggerCounter = 0
             if(!task.maxTriggers)
               task.maxTriggers = 1
             task.triggeredChildIds = []

             task.rejections = [];
             task.doReject = function(err){
                 task.rejections.push(err);
                 if(task.parentTask) {
                    task.parentTask.rejections.push(err);
                 }
                 if(task.executionPromiseReject)
                    return task.executionPromiseReject(err);
             }
             task.doResolve = function(re){
                 if(task.executionPromiseResolve)
                    return task.executionPromiseResolve(re);
             }

             if((task.triggerCallbacks.length <= 0) && (task.parentTask)) {//  && (false)
                 console.log("noone is interested in execution result of this child task, so not configuring any promises");
                 // task.executionPromise = task.parentTask.executionPromise;

             } else {
                 task.executionPromise = new Promise(function(resolve,reject){
                   task.executionPromiseResolve = resolve
                   task.executionPromiseReject = reject
                 })              
             }

             if(task.emitter) {
                task.onSendData = task.emitter.onSendData
                task.onTask = task.emitter.onTask
             }

             if((task.download)||(task.upload)) {
                if(!task.invocationTimeout) task.invocationTimeout = 30000
                if(!task.is_cloned_task)
                   task.triggerOnRead = true
                if(task.download) // upload tasks do need the control messages!
                  task.omitControlMessages = true

                if((task.download)&&(typeof task.omitAggregatedOutput == "undefined"))
                  task.omitAggregatedOutput = true
                if(typeof task.removeImmediately == "undefined")
                  task.removeImmediately = true
             }
             if(task.download)
                task.dontLog_stdout = true

             task.countExecution = true
             if((task.executeImmediately)||(task.emitterHead))
               task.countExecution = false

             setupInvocationTimeout(task)

             tasks[id] = task

             if(task.onTask)
                task.onTask(task.task_id)

             if(task.max_execution_time_ms)
                setTimeout(function(){
                   re.abortTask(task.task_id)
                }, task.max_execution_time_ms)


             tryToExecuteTask()

             return Promise.resolve({id: id, executionPromise: task.executionPromise})
        })

  }

  function tryToExecuteTask() {

       Object.keys(tasks).forEach(task_id =>{
          var task = tasks[task_id]

          if(task.executing) return
          if(task.finished) return
          if(task.aborted) return
          if(task.triggerOnRead) return
          if((task.emitter)&&(!task.emitter.spawned)) return

          // we need to execute the task if:
          // 1. it is a download task
          // 2. it is an upload task AND
          //    - it is not in doubleRequestMode
          //    or
          //    - it is the inputSender request
          if((task.download)&&(!task.readers.length)) return
          if((task.upload)&&(!task.doubleRequestMode)&&(!task.readers.length)) return
          if((task.upload)&&(task.doubleRequestMode)&&(!task.inputSender)) return

          if((task.countExecution)&&(tasksExecutingCurrently >= moduleOptions.max_tasks_simultaneously)) return

          console.log(`Executing task: ${task.task_id} ${task.commandLine}`)

          task.executing = true
          tasksAllExecutingCurrently++
          if(task.countExecution)
             tasksExecutingCurrently++

          spawnNextCommandOfTask(task)

       })

  }


  function processStdinLogic(task, otherOptions) {
      var re = otherOptions || {}
      /*
      if((task.download)||(task.upload)) {
          re.stdin = []
          re.stdin.push(task.upload ? task.requests[0] : "ignore")
          re.stdin.push(task.download ? task.readers[0] : "pipe")
          re.stdin.push(task.download ? task.readers[0] : "pipe")
      }
      */

      /*
      if(task.download) {
          re.stdin = ["ignore", task.readers[0].socket, task.readers[0].socket]
      }
      */

      return re
  }

  function doInputPiping(task) {

      if(!task.upload) return
      if(!task.child) return
      if(!task.child.stdin) return

      var inputRequest
      var inputResponse
      if(task.doubleRequestMode) {
        if(!task.inputSender) return
        inputRequest = task.inputSender.request
        inputResponse = task.inputSender.response
      }
      else
        inputRequest = task.requests[0]

      if(!inputRequest) return
      if(inputRequest.stdinBound) return

      console.log("piping request into stdin")
      inputRequest.stdinBound = true

      // error first, pipe after!
      task.child.stdin.on("error", function(e) {
          console.error("we lost a child stdin pipe:", e);
          if(task.inputSender) {
            task.inputSender.response.do_sendCode(500)
            task.inputSender.response.do_write("Process stdin closed unexpectedly")
            task.inputSender.response.do_end()
          }

          // we dont abort the task, as the process will exit now anyway and thus the response code would be lost
          //re.abortTask(task.task_id)
      });
      var weClosedIt = false
      inputRequest.on("error", function(e) {
          if(weClosedIt) return
          console.error("we lost an upload task:", e);
          re.abortTask(task.task_id)
      });

      inputRequest.pipe(task.child.stdin).on("finish", function(){

         if(!inputResponse) return // we dont tamper in non doublerequestmode

         console.log("inputsender has finished uploading, closing stdin of child")

         /*
         we send any kind of feedback to input sender requests only at this point to avoid browsers being confused
         */
         inputResponse.do_write("ok") // this will also send the headers
         inputResponse.do_end()

         weClosedIt = true

      })

  }

  function spawnNextCommandOfTask(task) {

      var commandToExecute = task.chain[task.commandChainIndex]

      if(task.commandChainIndex == 0)
          sendData("task execution started", lib.DATA_TYPE_CONTROL)

          task.commandChainIndex++


          var spawnOptions = processStdinLogic(task, commandToExecute.spawnOptions)

          console.log("executing command of task:", task.task_id, commandToExecute.commandLine)

          task.child =
             !commandToExecute.callback ?
             child_process.spawn(commandToExecute.executable, commandToExecute.args, spawnOptions) :
             commandToExecute.callback(task)

          doInputPiping(task)

          if(task.download) {
            /*
              var cmd = getCurrentCommand()
              cmd["stdoutClosed"] = true
              cmd["stderrClosed"] = true
              */

             task.child.stdout.pipe(task.readers[0])

             // it seems we cant pipe stderr to the same reader as it causes "write after end" exception
             // task.child.stderr.pipe(task.readers[0])

             if(task.readers[0].socket) {
                 var timeout = task.download_idle_timeout || moduleOptions.download_idle_timeout
                 console.log("configuring timeout for the download task", timeout)

                 task.readers[0].socket.setTimeout(timeout, ()=>{
                      console.log('socket of download task timeouted. Ending.');
                      task.child.stdout.destroy()
                      task.child.stderr.destroy()
                      re.abortTask(task.task_id)
                 });
             }

          }


          Array("stdout","stderr").forEach(c=>{
              if(!task.child[c]) return

              // for download tasks we dont intercept the data but just pass them through
              if((c== "stderr")||(!task.download)) {

                  task.child[c].on('data', (data) => {
                    if(!task["dontLog_"+c]) {
                        var str = `${c}: ${data}`
                        if(task.logToError)
                          console.error(str);
                        else
                          console.log(str);
                    }
                    if(task[c+"Reader"])
                       task[c+"Reader"](data)

                    if(task["filter_"+c])
                       data = task["filter_"+c](data)

                    if(task.dontRelayStderr) return;

                    if(!data) return

                    sendData(data, c == "stdout" ? lib.DATA_TYPE_STDOUT : lib.DATA_TYPE_STDERR)
                  });
              }

              task.child[c].on('close', () => {
                console.log(`${c}: closed`);
                var cmd = getCurrentCommand()
                cmd[c+"Closed"] = true
                currentCommandReady(cmd)
              });
          })

          // feed stdin with some static bytes
          if((typeof commandToExecute.stdin != "undefined")&&(task.child.stdin)){
            if(commandToExecute.stdin)
              task.child.stdin.write(commandToExecute.stdin)
            task.child.stdin.end()
          }

          task.child.on('error', onChildClose);
          task.child.on('exit', onChildClose);

          sendData("command execution started", lib.DATA_TYPE_CONTROL)

          function getCurrentCommand(){
             return task.chain[task.commandChainIndex-1]
          }

          function currentCommandReady(currentCommand) {
              if(!currentCommand.stdoutClosed) return
              if(!currentCommand.stderrClosed) return
              if(!currentCommand.childClosed) return

              var code = currentCommand.code

            var status = ""
            if((!status)&&(task.aborted)) status = "aborted"
            if(!status) status = code ? "error" : "successful"

            console.log(`${task.task_id} ${task.commandLine} child process exited with code ${code}`, task.chain.length, task.commandChainIndex, status);

            if((currentCommand)&&(currentCommand.commandCallback)&&(!currentCommand.commandCallback(task, currentCommand, status))) {
                console.log("commandCallback was unsuccessful, aborting the chain of commands")
                status = "unsuccessful"
            }


            if((status == "successful") && (task.chain.length > task.commandChainIndex)) {
              console.log("Executing next command in the chain of the task")
              spawnNextCommandOfTask(task)
              return
            }

            console.log("there are no more commands in this chain or we interrupt the process because of unsuccessful entry in the chain")

            if(task.lifeSignal)
              clearTimeout(task.lifeSignal)


            tasksAllExecutingCurrently--
            if(task.countExecution)
              tasksExecutingCurrently--
            task.executing = false
            task.finished = true

            if(task.parentTask) {
               if((task.parentTask.maxTriggers != -1)&&(task.parentTask.maxTriggers <= task.parentTask.triggerCounter)) {
                  // parent task should be resolved the same way as the child is
                  doResolveOrReject(task.parentTask, code)
               }

               console.log("deleting childTask attribute in command", task.parentTask.task_id)
               delete task.parentTask.childTask
            }


            task.executionResult = `mc task status: ${status}`

            sendData(task.executionResult, lib.DATA_TYPE_CONTROL, true)
            closeThemAll()

            task.code = code


            doResolveOrReject(task, code)

            re.setupTaskRemoval(task)

            tryToExecuteTask()

          }

          function doResolveOrReject(task, code) {
            var cre = re.getTaskResult(task.task_id)            

            if(code === 0)
              return task.doResolve(cre);

            var p = {code:cre.code};
            if(cre.output)
              p.output = cre.output.toString();

            cre.rejections = task.rejections;

            return task.doReject(p);
          }


          function onChildClose(code){
            console.log("onChildClose", task.task_id)
            var currentCommand = task.chain[task.commandChainIndex-1]
            currentCommand.code = code
            currentCommand.childClosed = true
            currentCommandReady(currentCommand)
          }


          function setupNewLifeSignal(){
            if(task.finished)
              return

            if(!moduleOptions.lifesignal_during_execution_seconds)
              return

            if(task.lifeSignal) {
              // console.log("clearing lifeSignal", task.task_id, task.commandLine)
              clearTimeout(task.lifeSignal)
            }

            task.lifeSignal = setTimeout(function(){
               if(task.finished) return

               console.log("sending life signal", task.task_id, task.commandLine)
               sendData("command still executing", lib.DATA_TYPE_CONTROL)

               task.lifeSignal = null

            }, (task.lifesignal_during_execution_seconds || moduleOptions.lifesignal_during_execution_seconds) * 1000)
          }

          function closeThemAll(){
             console.log("and closing all readers")
             task.readers.forEach(res => {
                res.do_end()
             })
          }

          function sendData(data, dataType, extraEnter){
             const truncateMessage = "command output truncated"

             if(task.onSendData)
               task.onSendData(data, dataType)

             if((dataType == lib.DATA_TYPE_CONTROL)&&(task.omitControlMessages))
                 return

             if(dataType == lib.DATA_TYPE_CONTROL)
                 data = (extraEnter ? "\n" : "") + "::: "+data+" :::\n"



             if(!task.omitAggregatedOutput) {

                 if(
                    ((dataType == lib.DATA_TYPE_STDOUT)&&(task.omitAggregatedStdout))
                    ||
                    ((dataType == lib.DATA_TYPE_STDERR)&&(task.omitAggregatedStderr))
                   )
                 {
                 }
                 else
                 {
                   var truncates = 0
                   task.output.push(data, function(removedItem){
                        truncates++
                   })

                   if(truncates > 0) {
                      sendData(truncateMessage, lib.DATA_TYPE_CONTROL)
                   }
                 }
             }

             if(!task.doubleRequestMode) {

                 task.readers.forEach(res => {

                    res.do_write(data)
                 })

             }
             else if(task.outputReader) {

                 // we have got a dedicated output reader
                 task.outputReader.response.do_write(data)
             }
             else {
               console.error("task is in double request mode, but has no readers so far")
             }

             setupNewLifeSignal()
          }

  }



  function dummyCloneObject(src){
    var re = {}
    for(q of Object.keys(src)){
       re[q] = src[q]
    }
    return re
  }

}

lib.EventEmitter = function(commanderOptions){
   "use strict"

   if(!commanderOptions) commanderOptions = {}

          const EventEmitter = require('events');

          class MyEmitter extends EventEmitter {}

          var stderrEmitter = new MyEmitter();
          var stdoutEmitter = new MyEmitter();
          var mainEmitter = new MyEmitter();
          mainEmitter.stdout = stdoutEmitter
          mainEmitter.stderr = stderrEmitter
          mainEmitter.tasks = []

          mainEmitter.requestsBeingRelayed = {}

          function abortRelayedTasks(){
             var c = getCommander()
             mainEmitter.tasks.forEach(task_id=>{
                c.abortTask(task_id)
             })
          }
          function abortRelayedRequests(){
             Object.keys(mainEmitter.requestsBeingRelayed).forEach(baseUrl=>{
                var abortUrl = baseUrl + "/abort"
                console.error("!!! sending abort to relayed request:", abortUrl)
                getRequest()(abortUrl)
             })
          }

          mainEmitter.kill = function(){
             console.log("we just received a stop request")

             mainEmitter.close()
          }

          mainEmitter.send = function(channel,data) {
             mainEmitter[channel].emit('data',data)
          }
          Array("stdout","stderr").forEach(c=>{
            mainEmitter["send_"+c] = function(data){
              mainEmitter.send(c, data)
            }
            mainEmitter["send_"+c+"_ln"] = function(data){
              mainEmitter.send(c, data+"\n")
            }
          })

          var commander
          function getCommander(){
             if(!commander)
               commander = commanderOptions.spawn ? commanderOptions : lib(commanderOptions)
             return commander
          }
          function getRequest(){
             const request = require('RequestCommon')
             return request
          }

          mainEmitter.onTask = function(task_id) {
              // this is a special mode, returns the main task id
              mainEmitter.tasks.push(task_id)
          }

          mainEmitter.onSendData = function(data, dataType) {
             if(dataType == lib.DATA_TYPE_CONTROL) return
             mainEmitter.stdout.emit('data', data)
          }

          mainEmitter.relayUrl = function(url) {
             const request = getRequest()
             return mainEmitter.relayRequest(request({method:"GET",uri: url}))
          }
          mainEmitter.relayStream = function(stream, options) {
             if(typeof options != "object") options = {};


             var mcStatusReceived = false;
             var failed = false;

              stream.on('error', function(err) {
                 failed = true;
                 doFail();
              })

              stream.on('end', function(data) {

                  if(options.onEnd)
                     options.onEnd(data);

                  if((!mcStatusReceived)&&(!failed))
                     doOk();

              })
              stream.on('data', function(data) {
                  // console.log('!!! received via relayRequest: ' + data.length + ' bytes of compressed data:', data.toString())

                  var str = data.toString()
                  mainEmitter.send_stdout(str.replace("::: mc task status: successful :::", ""))

                  var myRegexp = /::: mc task status: (.+) :::/;
                  var match = myRegexp.exec(str);
                  if(match){
                     mcStatusReceived  = true
                     if(match[1] == "successful") {
                       doOk()
                     }
                     else {
                       doFail(new Error(match[1]))
                     }
                  }

              })

              function doOk(){
                 console.log("Reading stream has finished successfully");
                 if(options.onOk)
                    options.onOk();

                 if(!options.dontCloseWhenFinished)
                   mainEmitter.close();
              }

              function doFail(err){
                 console.error("Error while reading stream", err);
                 if(options.onFail)
                   options.onFail(err);

                 if(!options.dontCloseWhenFinished)
                   mainEmitter.close(1);
              }
          }
          mainEmitter.relayRequest = function(req) {

            var url = ((req.uri)&&(req.uri.href)) ? req.uri.href : ""
            if(url)
              mainEmitter.requestsBeingRelayed[url] = true

            // console.log("!!!!!!", mainEmitter.requestsBeingRelayed)

            return new Promise(function(resolve,reject){
                var failed
                var mcStatusReceived
                var ok
                function doFail(err){
                    failed = true
                    reject(err)
                }
                function doOk(){
                    ok = true
                    resolve()
                }

                req.on('error', function(err) {
                    console.log("relayRequest error during the request", err)
                    doFail(err)
                })
                req.on('response', function(response) {
                  mainEmitter.relayStream(response, {
                      onEnd: function(){
                          if(url)
                             delete mainEmitter.requestsBeingRelayed[url]                        
                      },
                      onFail: doFail,
                      onOk: doOk,
                      dontCloseWhenFinished: true,
                  });

                })

            })

          }

          mainEmitter.spawn = function(taskOptions){

             const extend = require("node.extend")
             var taskOptions = extend(taskOptions || {}, {emitterHead: true, chain:{executable: "execution is an eventemitter mode", callback: function(task){
                return mainEmitter
             }}})

             return getCommander().spawn(taskOptions)
               .then(h=>{
                  mainEmitter.spawned = true
                  return Promise.resolve(h)
               })
          }

          mainEmitter.close = function(code){
            if(mainEmitter.closed) return
            mainEmitter.closed = true
            stdoutEmitter.emit("close")
            stderrEmitter.emit("close")
            mainEmitter.emit("exit", code || 0)

            abortRelayedTasks()
            abortRelayedRequests()
          }

          return mainEmitter

}

lib.DATA_TYPE_CONTROL = 1
lib.DATA_TYPE_STDOUT = 2
lib.DATA_TYPE_STDERR = 3
