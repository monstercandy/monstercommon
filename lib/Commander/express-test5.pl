#!/usr/bin/perl

use strict;
use warnings;
use IO::Socket::INET;
use constant BYTES => 100000000;

my $host = shift;
my $id = shift || "foo";

die "Usage: $0 host" if(!$host);


my $sock = IO::Socket::INET->new(PeerAddr => $host,
                                 PeerPort => 52123,
                                 Proto    => 'tcp') or die "cant: $!";

$sock->autoflush(1);

print $sock "GET /gc HTTP/1.1\r\nConnection: keep-alive\r\n\r\n";

print "And now reading the response\n";

while($sock->sysread(my $buf, 1024)) {
  print "read: ".(length $buf)." $buf\n";
}
