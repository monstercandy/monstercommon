require( "../MonsterDependencies")(__dirname)

const TEST_LISTEN_PORT = 18791
const express = require('express');
const commanderLib = require( "lib/index.js")

var app = express();
var commander = commanderLib({secure_task_id: false, lifesignal_during_execution_seconds: 1, routerFn:express.Router})

app.use('/commands', commander.router);

var server = app.listen(TEST_LISTEN_PORT);

const firstTwo = false

commander.spawn({command: "ping", args: ["-n",100,"127.0.0.1"], triggerOnRead: firstTwo})
commander.spawn({command: "ping", args: ["-n",100,"127.0.0.1"], triggerOnRead: firstTwo})

var mainStuff
var n = 1
commander.spawn({chain:[{executable: "ping", args: ["-n",2,"127.0.0.1"]}], triggerOnRead: true, maxTriggers: 3, triggerCallbacks: [function(h){

  console.log("new subtask execution triggered:", h)

  h.executionPromise.then((d)=>{
  	 n++
  	 var new_command = {executable: "ping", args: ["-n",2,"127.0.0."+n]}
  	 console.log("a command execution has just finished!", h.id, d, new_command)
     commander.changeTaskChain(mainStuff.id, new_command)
   })

}]})
  .then(h=>{
  	  mainStuff = h
      return h.executionPromise
  })
  .then(()=>{
  	  console.log("!!! main command execution has just finished")  	
  })
