#!/usr/bin/perl

my $lines = shift @ARGV || 10;
my $sleepbetween = shift @ARGV || 1;

local $| = 1;
binmode STDOUT;

while($lines > 0){
  print "line $lines\n" or die "Cant write: $!";
  sleep $sleepbetween;
  $lines--;
}
