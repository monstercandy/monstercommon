#!/usr/bin/perl

use constant DATA_LENGTH => 500*1024; # 100kb at once

local $| = 1;
binmode STDOUT;

my $x = ("x" x DATA_LENGTH)."\n";
my $i = 0;
while(1){
  $i++;
  print STDERR "$i\n" or die "Cant write: $!";
  print $x or die "Cant write: $!";
  sleep 1;
}
