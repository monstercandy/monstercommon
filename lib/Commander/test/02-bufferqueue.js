require("../../MonsterDependencies")(__dirname)

var assert = require("chai").assert

const bufferQueueLib = require( "../lib/bufferqueue.js")

describe('basic tests', function() {    


    it('string concat', function() {

         var bq = bufferQueueLib()
         bq.push("hello")
         bq.push(" ")
         bq.push("world")
         assert.equal(bq.asString(), "hello world")

    })

    it('string casting', function() {

         var bq = bufferQueueLib()
         bq.push("hello")
         bq.push(" ")
         bq.push("world")
         assert.equal(bq, "hello world")
    })


    it('max length constraint, first blob should be truncated', function() {

         var bq = bufferQueueLib({maxLength: 10})
         bq.push("123456")
         bq.push("789012")
         assert.equal(bq.asString(), "789012")

    })

    it('max length constraint, oversized blob should be rejected', function() {

         var bq = bufferQueueLib({maxLength: 10})
         bq.push("123456")
         bq.push("12345678901234")
         assert.equal(bq.asString(), "")
         assert.equal(bq.getSize(), 0)

    })

    it('max length constraint, callback should be invoked upon truncate', function() {

         var truncates = 0
         var bq = bufferQueueLib({maxLength: 10, onTruncate: function(data){ 
            assert.equal("123456", data); 
            truncates++ 
         }})
         bq.push("123456")
         bq.push("789012")
         assert.equal(bq.asString(), "789012")
         assert.equal(truncates, 1)

    })


    it('same with local parameter', function() {

         var truncates = 0
         var bq = bufferQueueLib({maxLength: 10})
         bq.push("123456", function(data){ 
            assert.fail()
         })
         bq.push("789012", function(data){ 
            assert.equal("123456", data); 
            truncates++ 
         })
         assert.equal(bq.asString(), "789012")
         assert.equal(truncates, 1)

    })

})

