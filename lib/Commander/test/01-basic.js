require("../../MonsterDependencies")(__dirname)

var assert = require("chai").assert

const TEST_LISTEN_PORT = 18791
const request = require('RequestCommon')
const express = require('express');
const commanderLib = require( "../lib/index.js")



describe('basic tests', function() {
    "use strict"

    var net = require('net');


        it('exception should support string conversion', function(done) {

             var commander = commanderLib()
             commander.spawn({command: "ping", args: ["-foobar"]})
               .then(h=>{

                  return h.executionPromise.then(()=>{
                     console.log("here")
                     done(new Error("Should not be here"));
                  })
               })
               .catch(ex=>{
                  assert.ok(ex.code);
                  assert.ok(ex.output);
                  assert.equal(typeof ex.output, "string");
                  assert.ok(ex.output.toString()); // toString should still be possible to call (robustness)
                  done();
               })

        })



    it('multiple subsequent connect by readers', function(done) {


        this.timeout(12000)

        var app = express();
        var commander = commanderLib({routerFn:express.Router})
        app.use('/tasks', commander.router);

        var server = app.listen(TEST_LISTEN_PORT);

         commander.spawn({chain:{executable: "perl", args: ["test/manylines.pl"]}, omitAggregatedOutput: true})
           .then(h=>{
               var url = getUrl(h)

               var clients = 0
               connectAsNewReader()

               function connectAsNewReader() {
                var clientId = ++clients
                console.log(clientId, "connecting...")
                var client = new net.Socket();
                client.connect(TEST_LISTEN_PORT, '127.0.0.1', function() {
                  client.write("GET /tasks/"+h.id+" HTTP/1.0\r\n\r\n");
                  console.log(clientId, '- request headers sent');

                });

                client.on('data', function(data) {
                  var str = data.toString()
                  console.log(clientId, 'inputsender response received:', str);

                  if(str.match(/line 5/)) {
                     console.log("we are at halfway!")
                     client.destroy()
                     return connectAsNewReader()
                  }

                  if(str.match(/::: mc task status: successful :::/)) {
                     assert.equal(clientId, 2) // it should be the second client
                     server.close()
                     done()
                   }


                });
               }

           })

    })


    it('stdout/stderr', function(done) {

         var stdout = ""
         var stderr = ""
         var commander = commanderLib()
         commander.spawn({
          stdoutReader: function(data){stdout+=data;},
          stderrReader: function(data){stderr+=data;},
          omitControlMessages: true,
          chain: [{executable: "perl",
          args: ["test/stdout-stderr.pl"] }]
         })
           .then(h=>{

              return h.executionPromise

           })
           .then(x=>{
             assert.equal(stdout, "stdout\n")
             assert.equal(stderr, "stderr\n")
             assert.equal(x.output.toString(), "stderr\nstdout\n")
             done()

           })
           .catch(done)

    })

    it('stdout/stderr test, wanting stdout only', function(done) {

         var commander = commanderLib()
         commander.spawn({
          omitAggregatedStderr: true,
          omitControlMessages: true,
          chain: [{executable: "perl",
          args: ["test/stdout-stderr.pl"] }]
         })
           .then(h=>{

              return h.executionPromise

           })
           .then(x=>{
             assert.equal(x.output.toString(), "stdout\n")
             done()

           })
           .catch(done)

    })


    it('timeout for execution', function(done) {

         var commander = commanderLib()
         commander.spawn({max_execution_time_ms: 1500, chain:{executable: "ping", args: ["-n",100,"127.0.0.1"]}})
           .then(h=>{
              return h.executionPromise
           })
           .then(result=>{
              console.log("succeeded", result)
              assert.fail(new Error("should have failed"))
           })
           .catch(ex=>{
              console.error("failed", ex)
              assert.ok(ex.message.indexOf("Timeout or aborted") == 0)
              done()
           })
           .catch(done)

    })



    it('executing a command that cannot be found', function(done) {

         var commander = commanderLib()
         commander.spawn({command: "something_that_cannot_be_found"})
           .then(h=>{
                  console.log("main chain", h)

              h.executionPromise.then(()=>{
                  console.log("executionpromise then")
                  done(new Error("Then should have not called"))
              })
              .catch((m)=>{
                  // this is expected
                  console.log("in catch of executionpromise:", m)
                  assert.equal(m.code.code, 'ENOENT')

                  done()
              })

           })
           .catch(ex=>{
              console.log("main chain exception")
              done(new Error("The main chain should have been successful"))
           })

    })



    it('command id should have high entrophy', function(done) {

         var commander = commanderLib()
         commander.spawn({command: "ping", args: ["-n",1,"127.0.0.1"]})
           .then(h=>{
              assert.equal((""+h.id).length, 32)

              h.executionPromise.then(()=>{
                 done()

              })
           })
           .catch(done)

    })


    Array({"command":"ping"},{"command":"[crap]",replacements:{"crap":"ping"}}).forEach(test=>{

        it('ping localhost: '+test.command, function(done) {

             var commander = commanderLib({secure_task_id: false})
             commander.spawn({command: test.command, args: ["-n",1,"127.0.0.1"]}, test.replacements)
               .then(h=>{
                  assert.equal(h.id, 1)

                  return h.executionPromise.then(()=>{
                     done()

                  })
               })
               .catch(done)

        })


    })


    it('binary output', function(done) {

         var commander = commanderLib({secure_task_id: false})
         commander.spawn({omitControlMessages: true, chain: [{executable: "perl", args: ["test/binary.pl"] }]})
           .then(h=>{
              assert.equal(h.id, 1)

              return h.executionPromise

           })
           .then(x=>{
             var buffers = x.output.asBuffers()
             assert.equal(buffers.length, 1)
             var i = 0
             for (const b of buffers[0]) {
                assert.equal(b, i++)
             }
             done()

           })
           .catch(done)

    })


    it('ping 127.0.0.1 and 127.0.0.2', function(done) {

         var commander = commanderLib({secure_task_id: false})
         commander.spawn({chain: [{executable: "ping", args: ["-n",1,"127.0.0.1"] }, {executable: "ping", args: ["-n",1,"127.0.0.2"] } ]})
           .then(h=>{
              assert.equal(h.id, 1)

              return h.executionPromise

           })
           .then(x=>{
            console.log("!foo!", x.output.toString())
             assert.ok(x.output.indexOf('::: command execution started :::') > -1)
             assert.ok(x.output.indexOf('Ping statistics for 127.0.0.1:') > -1)
             assert.ok(x.output.indexOf('Ping statistics for 127.0.0.2:') > -1)
             assert.ok(x.output.indexOf('::: mc task status: successful :::') > -1)

             done()

           })
           .catch(done)

    })

    it('ping localhost, invalid arguments, should reject', function(done) {

         var commander = commanderLib()
         commander.spawn({command: "ping", args: ["-sde","crap"]})
           .then(h=>{

              h.executionPromise.then(()=>{
                 done(new Error("should have failed"))

              }).catch((cmd)=>{
                 assert.ok(cmd.code != 0)
                 done()
              })
           })

    })


    it('ping chain, second command invalid', function(done) {

         var commander = commanderLib({secure_task_id: false})
         commander.spawn({chain: [{executable: "ping", args: ["-n",1,"127.0.0.1"] }, {executable: "ping", args: ["-sde","crap"]}, {executable: "ping", args: ["-n",1,"127.0.0.3"] } ]})
           .then(h=>{
              assert.equal(h.id, 1)

              return h.executionPromise

           })
           .then(()=>{
              throw new Error("should not be here")
           })
           .catch(x=>{
             assert.property(x, "code")
             assert.ok(x.code != 0)
             assert.ok(x.output.indexOf('::: command execution started :::') > -1)
             assert.ok(x.output.indexOf('Ping statistics for 127.0.0.1:') > -1)
             assert.ok(x.output.indexOf('Ping statistics for 127.0.0.2:') <= -1)
             assert.ok(x.output.indexOf('Ping statistics for 127.0.0.3:') <= -1)
             assert.ok(x.output.indexOf('::: mc task status: error :::') > -1)

             done()

           })

    })


    it("command output should be available via http", function(done){

        var app = express();
        var commander = commanderLib({routerFn:express.Router})

        app.use('/tasks', commander.router);

        var server = app.listen(TEST_LISTEN_PORT);


         commander.spawn({command: "ping", args: ["-n",1,"127.0.0.1"]})
           .then(h=>{
               var url = getUrl(h)

               request({method:'GET',uri: url}, (error, response, body) => {

                   assert.ok(body.indexOf('::: command execution started :::') > -1)
                   assert.ok(body.indexOf('Ping statistics for 127.0.0.1:') > -1)
                   assert.ok(body.indexOf('::: mc task status: successful :::') > -1)

                   server.close()

                   done()

                });

           })

    })

    it("command output should be available via http, for second fetch as well", function(done){

        var app = express();
        var commander = commanderLib({routerFn:express.Router})

        app.use('/tasks', commander.router);

        var server = app.listen(TEST_LISTEN_PORT);

         commander.spawn({command: "ping", args: ["-n",1,"127.0.0.1"]})
           .then(h=>{
               var url = getUrl(h)

               request({method:'GET',uri: url}, (error, response, body) => {

                   assert.ok(body.indexOf('::: command execution started :::') > -1)
                   assert.ok(body.indexOf('Ping statistics for 127.0.0.1:') > -1)
                   assert.ok(body.indexOf('::: mc task status: successful :::') > -1)

                   request({method:'GET',uri: getUrl(h)}, (error, response, body) => {

                       assert.ok(body.indexOf('::: command execution started :::') > -1)
                       assert.ok(body.indexOf('Ping statistics for 127.0.0.1:') > -1)
                       assert.ok(body.indexOf('::: mc task status: successful :::') > -1)

                       server.close()

                       done()

                    });

                });

           })

    })



    it('command queing', function(done) {

         this.timeout(5000)

         var firstReady = false
         var secondReady = false
         var commander = commanderLib()
         commander.spawn({command: "ping", args: ["-n",3,"127.0.0.1"]})
           .then(h=>{
              return h.executionPromise
           })
           .then(()=>{
              assert.ok(!secondReady)
              firstReady = true

           })
           .catch(done)

         commander.spawn({command: "ping", args: ["-n",1,"127.0.0.2"]})
           .then(h=>{
              return h.executionPromise
           })
           .then(()=>{
              assert.ok(firstReady)
              secondReady = true

              done()
           })
           .catch(done)

    })


    it('command queing with http response checking', function(done) {

         this.timeout(5000)

        var app = express();
        var firstReady = false
        var secondReady = false
        var commander = commanderLib({routerFn:express.Router,lifesignal_during_execution_seconds:1})
        app.use('/tasks', commander.router);
        var server = app.listen(TEST_LISTEN_PORT);


         commander.spawn({command: "ping", args: ["-n",3,"127.0.0.1"]})
           .then(h=>{
              return h.executionPromise
           })
           .then(()=>{
              assert.ok(!secondReady)
              firstReady = true

           })
           .catch(done)

         commander.spawn({command: "ping", args: ["-n",1,"127.0.0.1"]})
           .then(h=>{
               var url = getUrl(h)

               request({method:'GET',uri: url}, (error, response, body) => {
                   if(error) return done(error)

                    if(!secondReady) return done(new Error("second task execution has not yet finished"))

                    if(body.indexOf('::: task execution is pending :::') < 0) return done(new Error("pending text not found"))

                    server.close()
                    done()

               })

              return h.executionPromise
           })
           .then(()=>{
              assert.ok(firstReady)
              secondReady = true

           })
           .catch(done)



    })




    it('command queing with invocationTimeout: second command should not be started due to timeout', function(done) {

         this.timeout(5000)

         var firstReady = false
         var secondReady = false
         var commander = commanderLib()
         commander.spawn({command: "ping", args: ["-n",3,"127.0.0.1"]})
           .then(h=>{
              return h.executionPromise
           })
           .then(()=>{
              firstReady = true

           })
           .catch(done)

         commander.spawn({invocationTimeout: 2000, command: "ping", args: ["-n",1,"127.0.0.1"]})
           .then(h=>{
              return h.executionPromise
           })
           .then(()=>{
              assert.fail("should have failed")

           })
           .catch(ex=>{
              secondReady = true
              assert.ok(ex.message.indexOf("Timeout or aborted") == 0)
              done()
           })
           .catch(done)

    })



    it('command queing with invocationTimeout: second command should still start (timeout is higher)', function(done) {

         this.timeout(5000)

         var firstReady = false
         var secondReady = false
         var commander = commanderLib()
         commander.spawn({command: "ping", args: ["-n",2,"127.0.0.1"]})
           .then(h=>{
              return h.executionPromise
           })
           .then(()=>{
              assert.ok(!secondReady)
              firstReady = true

           })
           .catch(done)

         commander.spawn({invocationTimeout: 3000, command: "ping", args: ["-n",1,"127.0.0.1"]})
           .then(h=>{
              return h.executionPromise
           })
           .then(()=>{
              assert.ok(firstReady)
              secondReady = true

              done()
           })
           .catch(done)

    })



    it('triggerOnRead', function(done) {

         this.timeout(5000)

         var app = express();
         var commander = commanderLib({routerFn:express.Router,lifesignal_during_execution_seconds:1})
         app.use('/tasks', commander.router);
         var server = app.listen(TEST_LISTEN_PORT);

         var responseReceived = false;
         var mainFinishWasCalled = false
         var requestSent = false
         commander.spawn({command: "ping", args: ["-n",1,"127.0.0.1"], triggerOnRead: true})
           .then(h=>{

              var url = getUrl(h)

              setTimeout(function(){

                  console.log("sending request")
                  requestSent = true
                  request({method:'GET',uri: url}, (error, response, body) => {

                        if(error) return done(error)

                        console.log("!foo!", body)
                        assert.ok(body.indexOf('Ping statistics for 127.0.0.1:') > -1)
                        assert.ok(body.indexOf('::: mc task status: successful :::') > -1)

                        responseReceived = true;

                        doneLogic();

                  })


              }, 2000)

              return h.executionPromise
           })
           .then((xx)=>{
              mainFinishWasCalled = true;
              doneLogic();

           })
           .catch(done)

           function doneLogic(){
              if(!mainFinishWasCalled) return;
              if(!responseReceived) return;
              server.close();              
              done();
           }

    })


    it('triggerOnRead trigger only', function(done) {

         this.timeout(5000)

         var app = express();
         var commander = commanderLib({secure_task_id: false,routerFn:express.Router,lifesignal_during_execution_seconds:1})
         app.use('/tasks', commander.router);
         var server = app.listen(TEST_LISTEN_PORT);

         var responseReceived = false;
         var mainFinishWasCalled = false;
         commander.spawn({command: "ping", args: ["-n",1,"127.0.0.1"], triggerOnRead: true})
           .then(h=>{

              assert.equal(h.id, 1)

              var url = getUrl(h)+"/trigger"

              request({method:'GET',uri: url}, (error, response, body) => {

                    if(error) return done(error)

                    assert.equal(body, '{"task_id":2}')

                    server.close();

                    responseReceived = true;

                    doneLogic();

              })



              return h.executionPromise
           })
           .then((xx)=>{

              mainFinishWasCalled = true;
              doneLogic();

           })
           .catch(done)


           function doneLogic(){
              if(!mainFinishWasCalled) return;
              if(!responseReceived) return;
              server.close();              
              done();
           }

    })





    it('triggerOnRead shall delegate exceptions to main', function(done) {

         this.timeout(5000)

         var app = express();
         var commander = commanderLib({routerFn:express.Router,lifesignal_during_execution_seconds:1})
         app.use('/tasks', commander.router);
         var server = app.listen(TEST_LISTEN_PORT);

         var responseReceived = false;
         var mainException = false
         var requestSent = false
         commander.spawn({command: "perl", args: ["test/fail.pl"], triggerOnRead: true})
           .then(h=>{

              var url = getUrl(h)

              setTimeout(function(){

                  console.log("sending request")
                  requestSent = true
                  request({method:'GET',uri: url}, (error, response, body) => {

                        if(error) return done(error)

                        console.log("!foo!", body)
                        assert.ok(body.indexOf('To fail is human') > -1)
                        assert.ok(body.indexOf('::: mc task status: error :::') > -1)

                        responseReceived = true;

                        doneLogic();

                  })


              }, 2000)

              return h.executionPromise
           })
           .then((xx)=>{
              console.log("simple then was called");
              throw new Error("Should have failed!");
           })
           .catch(err=>{
              console.log("err", err);
              if(typeof err.output == "undefined") {
                 // unexpected exceptipon
                 console.log("UNEXPECTED EXCEPTION")
                 throw err;
              }

              mainException = true;
              doneLogic();

           })
           .catch(done)

           function doneLogic(){
              if(!mainException) return;
              if(!responseReceived) return;
              server.close();              
              done();
           }

    })


    it('command aborting', function(done) {

         this.timeout(5000)

        var app = express();
        var commander = commanderLib({routerFn:express.Router})
        app.use('/tasks', commander.router);
        var server = app.listen(TEST_LISTEN_PORT);

         commander.spawn({command: "ping", args: ["-n",100,"127.0.0.1"]})
           .then(h=>{
              var url = getUrl(h)
              var url_abort = getUrl(h)+"/abort"

               request({method:'GET',uri: url}, (error, response, body) => {

                   assert.ok(body.indexOf('::: command execution started :::') > -1)
                   assert.ok(body.indexOf('Reply from 127.0.0.1') > -1)
                   assert.ok(body.indexOf('::: mc task status: aborted :::') > -1)

                   server.close()
                   done()
                });


              setTimeout(function(){

               request({method:'GET',uri: url_abort}, (error, response, body) => {

                   assert.equal(body, "ok\n")

                });

              },2000)

              return h.executionPromise
           })
           .then(()=>{
              assert.fail("should not be here")

           })
           .catch(ex=>{
              assert.ok(ex.output.indexOf("::: mc task status: aborted :::") > -1)
              console.log("execution failed", ex)

           })



    })


    it('slow scripts should show some life signal', function(done) {

         this.timeout(5000)

         var commander = commanderLib({lifesignal_during_execution_seconds:1})
         commander.spawn({command: "perl", args: ["test/slow.pl",2]})
           .then(h=>{

              return h.executionPromise.then((cmd)=>{

                 assert.ok(cmd.output.indexOf('::: command execution started :::') > -1)
                 assert.ok(cmd.output.indexOf('::: command still executing :::') > -1)
                 assert.ok(cmd.output.indexOf('hello!') > -1)
                 assert.ok(cmd.output.indexOf('::: mc task status: successful :::') > -1)

                 done()

              })
           })
           .catch(done)

    })






    it('multiple triggers, changing the command chain', function(done) {

         this.timeout(5000)

         var app = express();
         var commander = commanderLib({secure_task_id: false,routerFn:express.Router,lifesignal_during_execution_seconds:1})
         app.use('/tasks', commander.router);
         var server = app.listen(TEST_LISTEN_PORT);
         var triggerUrl

         var mainStuff
         var n = 1
         commander.spawn({chain:[{executable: "ping", args: ["-n",2,"127.0.0.1"]}], triggerOnRead: true, maxTriggers: 3, triggerCallbacks: [function(h){

            console.log("! new subtask execution triggered:", h)

            h.executionPromise.then((d)=>{


               console.log("! subtask execution has finished:", d, n)

               assert.ok(d.output.indexOf('Ping statistics for 127.0.0.'+n+':') > -1)

               n++
               var new_command = {executable: "ping", args: ["-n",2,"127.0.0."+n]}
               // console.log("a command execution has just finished!", h.id, d, new_command)
               commander.changeTaskChain(mainStuff.id, new_command)

              console.log("! subtask execution callback has finished, doing trigger")

               doTrigger()
             })
             .catch(done)

         }]})
         .then(h=>{

            console.log("main promise then called", h)

            mainStuff = h

            triggerUrl = getUrl(h)+"/trigger"

            doTrigger()

            return h.executionPromise
         })
         .then(()=>{
            console.log("!!! main command execution has just finished")

            assert.equal(n, 4)

         })
         .catch(done)
//console.log("body", body)

         var reqReturns = 0
         function doTrigger() {
            request({method:'GET',uri: triggerUrl}, (error, response, body) => {

                if(error) return done(error)

                reqReturns++

                if(reqReturns >= 4) {
                   assert.equal(body, "This task can be triggered no more.\n")

                   server.close()
                   return done()
                }

                assert.equal(body, '{"task_id":'+(n+1)+'}')

            })

         }

    })


    it('aborting a chain of commands', function(done) {
         function cc(task, cmd, status) {
            assert.equal(cmd.executable, "ping")
            return false // aborting
         }

         var commander = commanderLib({secure_task_id: false})
         commander.spawn({chain: [{executable: "ping", args: ["-n",1,"127.0.0.1"], commandCallback: cc }, {executable: "PING", args: ["-n",1,"127.0.0.2"] } ]})
           .then(h=>{
              assert.equal(h.id, 1)

              return h.executionPromise

           })
           .then(x=>{
             console.log("!foo!", x.output.toString())
             assert.ok(x.output.indexOf('::: command execution started :::') > -1)
             assert.ok(x.output.indexOf('Ping statistics for 127.0.0.1:') > -1)
             assert.ok(x.output.indexOf('Ping statistics for 127.0.0.2:') <= -1)
             assert.ok(x.output.indexOf('::: mc task status: unsuccessful :::') > -1)

             done()

           })
           .catch(done)

    })

    it('not aborting a chain of commands in a callback', function(done) {
         function cc(task, cmd, status) {
            assert.equal(cmd.executable, "ping")
            return true // proceeding
         }

         var commander = commanderLib({secure_task_id: false})
         commander.spawn({chain: [{executable: "ping", args: ["-n",1,"127.0.0.1"], commandCallback: cc }, {executable: "PING", args: ["-n",1,"127.0.0.2"] } ]})
           .then(h=>{
              assert.equal(h.id, 1)

              return h.executionPromise

           })
           .then(x=>{
             assert.ok(x.output.indexOf('::: command execution started :::') > -1)
             assert.ok(x.output.indexOf('Ping statistics for 127.0.0.1:') > -1)
             assert.ok(x.output.indexOf('Ping statistics for 127.0.0.2:') > -1)
             assert.ok(x.output.indexOf('::: mc task status: successful :::') > -1)

             done()

           })
           .catch(done)

    })



    it('with omitControlMessages control messages should not be present in the output', function(done) {

         var commander = commanderLib({})
         commander.spawn({omitControlMessages: true, chain: [{executable: "ping", args: ["-n",1,"127.0.0.1"] }]})
           .then(h=>{

              return h.executionPromise

           })
           .then(x=>{
             assert.ok(x.output.indexOf('::: command execution started :::') <= -1)
             assert.ok(x.output.indexOf('Ping statistics for 127.0.0.1:') > -1)
             assert.ok(x.output.indexOf('::: mc task status: successful :::') <= -1)

             done()

           })
           .catch(done)

    })


    it('with omitAggregatedOutput output should not be saved into task.output', function(done) {

         var commander = commanderLib({})
         commander.spawn({omitAggregatedOutput: true, chain: [{executable: "ping", args: ["-n",1,"127.0.0.1"] }]})
           .then(h=>{

              return h.executionPromise

           })
           .then(x=>{
             assert.ok(x.output.indexOf('::: command execution started :::') <= -1)
             assert.ok(x.output.indexOf('Ping statistics for 127.0.0.1:') <= -1)
             assert.ok(x.output.indexOf('::: mc task status: successful :::') <= -1)

             done()

           })
           .catch(done)

    })


    it("EventEmitter mode", function(done){

         var emitter = commanderLib.EventEmitter()
         emitter.spawn()
         .then(h=>{

            setTimeout(function(){
              emitter.stdout.emit("data", "hello world\n")
              emitter.close()
            }, 1000)

            return h.executionPromise
         })
         .then(b=>{
            assert.ok(b.output.indexOf("hello world\n") > -1)
            done()
         })

    })




    it("EventEmitter mode relaying additional commands", function(done){

         var commander = commanderLib({secure_task_id: false})

         var emitter = commanderLib.EventEmitter()
         emitter.spawn()
         .then(h=>{

           var executionChain = []
           commander.spawn({emitter: emitter, chain: [{executable: "ping", args: ["-n",1,"127.0.0.1"] } ]})
             .then(h=>{
               executionChain.push(h.executionPromise)
               return commander.spawn({emitter: emitter, chain: [{executable: "ping", args: ["-n",1,"127.0.0.2"] } ]})
             })
             .then(h=>{
               executionChain.push(h.executionPromise)

               return Promise.all(executionChain)
             })
             .then(()=>{
                emitter.close()
             })
             .catch(ex=>{
                console.error("errur during command", ex)
                emitter.send_stderr("error during commands\n"+ex.message+"\n")
                emitter.close(1)
             })

            return h.executionPromise
         })
         .then(x=>{
            assert.ok(x.output.indexOf('::: command execution started :::') > -1)
            assert.ok(x.output.indexOf('Ping statistics for 127.0.0.1:') > -1)
            assert.ok(x.output.indexOf('Ping statistics for 127.0.0.2:') > -1)
            assert.ok(x.output.indexOf('::: mc task status: successful :::') > -1)

            assert.ok(x.output.indexOf('error during commands') <= -1)

            done()
         })
         .catch(errObject =>{
             done(new Error("error during stuff"))
         })

    })




    it('request body should be bound to command input', function(done) {

        var app = express();
        var commander = commanderLib({routerFn:express.Router})

        app.use('/tasks', commander.router);

        var server = app.listen(TEST_LISTEN_PORT);


         commander.spawn({upload: true, command: "perl", args: ["test/sha1.pl"]})
           .then(h=>{
               var url = getUrl(h)

               request({method:'POST',uri: url, body: "hello\n"}, (error, response, body) => {

                   assert.ok(body.indexOf('::: command execution started :::') > -1)
                   assert.ok(body.indexOf('da39a3ee5e6b4b0d3255bfef95601890afd80709') > -1)
                   assert.ok(body.indexOf('::: mc task status: successful :::') > -1)

                   server.close()

                   done()

                });

           })

    })


    it('stdin data could be configured without requests as well', function(done) {

        var commander = commanderLib()


         commander.spawn({chain:{executable: "perl", args: ["test/sha1.pl"], stdin: "hello\n"}})
           .then(h=>{
              return h.executionPromise
           })
           .then(result=>{
               assert.ok(result.output.indexOf('da39a3ee5e6b4b0d3255bfef95601890afd80709') > -1)
               done()

           })
           .catch(done)

    })




    it("relaying external command task outputs via an emitter", function(done){

        this.timeout(10000)

        var app = express();
        var commander = commanderLib({routerFn:express.Router})

        app.use('/tasks', commander.router);

        var emitterExecutionPromiseHasFinished = false
        var emitterRequestHasFinished = false

        var server = app.listen(TEST_LISTEN_PORT);
        var c = 0

        const commands_to_be_executed = 3

        var currentCommandCounter = 0
        var commandIds = []

        var ps = []
        for(var c = 1; c <= commands_to_be_executed; c++) {
           ps.push(
            commander.spawn({triggerOnRead: true, chain:{executable: "ping", args: ["-n",2,"127.0.0."+c]}})
             .then(h=>{
                commandIds.push(h.id)
             })
           )
        }


         var emitter = commander.EventEmitter()

         Promise.all(ps)
          .then(()=>{
             return emitter.spawn()
          })
          .then(h=>{

            var url = getUrl(h)
            request({method:'GET',uri: url}, (error, response, body) => {

               console.log("!!! main emitter finished:", body)

               assert.ok(body.indexOf('::: command execution started :::') > -1)
               for(var i = 1; i <= commands_to_be_executed; i++)
                   assert.ok(body.indexOf('Ping statistics for 127.0.0.'+i+':') > -1)

               var count = (body.match(/::: mc task status: successful :::/g) || []).length;
               assert.equal(count, 1) // relayRequest should omit the status lines

               server.close()

               emitterRequestHasFinished = true

               callDoneIfReady()

            });


            startFetchingNextCommand()


            return h.executionPromise
         })
         .then(x=>{
            for(var i = 1; i <= commands_to_be_executed; i++)
                 assert.ok(x.output.indexOf('Ping statistics for 127.0.0.'+i+':') > -1)

            emitterExecutionPromiseHasFinished = true
         })
         .catch(done)

         function startFetchingNextCommand() {
            if(currentCommandCounter >= commandIds.length) {
               // chain completed, closing
               emitter.close()
            }
            var h = {id: commandIds[currentCommandCounter++] }
            if(!h.id) return // close the loop
            var url = getUrl(h)
            console.log("!!! fetching triggerOnRead task:", url)
            // var r = request({method:'GET',uri: url})
            emitter.relayUrl(url)
              .then(()=>{
                 console.log("!!! relayRequest finished", h, url)
                 startFetchingNextCommand()
              })
         }


         function callDoneIfReady(){
            if(!emitterExecutionPromiseHasFinished) return
            if(!emitterRequestHasFinished) return
            done()
         }


    })




    it('upload task in full duplex mode with two parallel requests (input sender and output reader)', function(done) {

        this.timeout(5000)

        var app = express();
        var commander = commanderLib({routerFn:express.Router})

        app.use('/tasks', commander.router);

        var server = app.listen(TEST_LISTEN_PORT);
        const data = "hello\n"
        var outputReaderResponseReceived = false
        var inputSenderResponseReceived = false
        var outputDataSentCompletely = false

         commander.spawn({lifesignal_during_execution_seconds: 1, upload: true, command: "perl", args: ["test/sha1.pl"]})
           .then(h=>{
               var url = getUrl(h)

               request({method:'GET',uri: url+"/output"}, (error, response, body) => {

                   console.log("!!!!!!!!!!!!!!!!!!!!!!! output reader request has finished", error, body)

                   assert.ok(body.indexOf('::: command execution started :::') > -1)
                   assert.ok(body.indexOf('::: command still executing :::') > -1)
                   assert.ok(body.indexOf('da39a3ee5e6b4b0d3255bfef95601890afd80709') > -1)
                   assert.ok(body.indexOf('::: mc task status: successful :::') > -1)

                   outputReaderResponseReceived = true

                   finishLogic()

                });


                var client = new net.Socket();
                client.connect(TEST_LISTEN_PORT, '127.0.0.1', function() {
                  client.write("POST /tasks/"+h.id+"/input HTTP/1.0\r\nContent-length: "+(data.length)+"\r\n\r\n");
                  console.log('- request headers sent');

                  setTimeout(function(){
                     client.write(data)
                     outputDataSentCompletely = true
                     console.log("- request body sent")
                  }, 2000)
                });

                client.on('data', function(data) {
                  var str = data.toString()
                  console.log('inputsender response received:', str);
                  if(!outputDataSentCompletely)
                      assert(false, "we shouldnt have received any data at this point")

                  if(str.indexOf("ok") > -1) {
                    console.log("got ok in the response!!!")
                    inputSenderResponseReceived = true
                    client.destroy(); // kill client after server's response
                  }

                  finishLogic()
                });


               function finishLogic(){
                console.log("finishLogic", outputReaderResponseReceived, inputSenderResponseReceived, outputDataSentCompletely)
                  if(!outputReaderResponseReceived) return
                  if(!inputSenderResponseReceived) return
                  if(!outputDataSentCompletely) return

                  server.close()
                  done()
               }

           })

    })



    it('upload task in full duplex mode should timeout if only output is called', function(done) {

        this.timeout(5000)

        var app = express();
        var commander = commanderLib({routerFn:express.Router})

        app.use('/tasks', commander.router);

        var server = app.listen(TEST_LISTEN_PORT);

         commander.spawn({invocationTimeout: 2000, upload: true, command: "perl", args: ["test/sha1.pl"]})
           .then(h=>{
               var url = getUrl(h)

               request({method:'GET',uri: url+"/output"}, (error, response, body) => {

                   console.log("invocation timeout test", error, body)

                   assert.equal(body, "::: output reader attached\n")

                   server.close()
                   done()

                });


           })

    })




    it('task in full duplex mode with 2 parallel and 1 upcoming requests (outputreader should take over)', function(done) {

        const data = "1234567890"
        this.timeout(12000)

        var app = express();
        var commander = commanderLib({routerFn:express.Router})
        app.use('/tasks', commander.router);

        var server = app.listen(TEST_LISTEN_PORT);

        var outputDataSentCompletely = false
        var inputSenderResponseReceived = false
        var outputReaderResponseReceived = false
        var writerKickedOffAlready = false

         commander.spawn({chain:{executable: "perl", args: ["test/manylines.pl"]}, upload: true, omitAggregatedOutput: true})
           .then(h=>{
               var url = getUrl(h)

               var clients = 0
               connectAsNewReader()


               function connectAsWriter() {
                  var client = new net.Socket();
                  client.connect(TEST_LISTEN_PORT, '127.0.0.1', function() {
                    client.write("POST /tasks/"+h.id+"/input HTTP/1.0\r\nContent-length: "+(data.length)+"\r\n\r\n");
                    console.log('- writer request headers sent');

                    setTimeout(function(){
                       client.write(data)
                       outputDataSentCompletely = true
                       console.log("- request body sent")
                    }, 2000)
                  });

                  client.on('data', function(data) {
                    var str = data.toString()
                    console.log('inputsender response received:', str);
                    if(!outputDataSentCompletely)
                        assert(false, "we shouldnt have received any data at this point")

                    if(str.indexOf("ok") > -1) {
                      console.log("got ok in the response!!!")
                      inputSenderResponseReceived = true
                      client.destroy(); // kill client after server's response
                    }

                    finishLogic()
                  });
               }


               function connectAsNewReader() {
                var clientId = ++clients
                console.log(clientId, "connecting...")
                var client = new net.Socket();
                client.connect(TEST_LISTEN_PORT, '127.0.0.1', function() {
                  client.write("GET /tasks/"+h.id+"/output HTTP/1.0\r\n\r\n");
                  console.log(clientId, '- request headers sent');

                });

                client.on('data', function(data) {
                  var str = data.toString()
                  console.log(clientId, 'inputsender response received:', str);

                  if(!writerKickedOffAlready) {
                     writerKickedOffAlready = true
                     connectAsWriter()
                  }

                  if(str.match(/line 5/)) {
                     console.log("we are at halfway!")
                     client.destroy()
                     return connectAsNewReader()
                  }

                  if(str.match(/::: mc task status: successful :::/)) {
                     assert.equal(clientId, 2) // it should be the second client
                     outputReaderResponseReceived = true
                     finishLogic()
                   }


                });
               }

               function finishLogic(){
                  if(!outputReaderResponseReceived) return
                  if(!inputSenderResponseReceived) return
                  if(!outputDataSentCompletely) return

                   server.close()
                   done()
               }

           })
    })





    it("download task should be aborted when the socket idle timeout is reached", function(done){

        this.timeout(10000)

        var app = express();
        var commander = commanderLib({routerFn:express.Router})

        app.use('/tasks', commander.router);

        var server = app.listen(TEST_LISTEN_PORT);
        var child;


         commander.spawn({download:true, download_idle_timeout: 3000, chain:{executable: "perl",  args: ["test/forever.pl"]}})
           .then(h=>{
                var requestHeaders = "GET /tasks/"+h.id+" HTTP/1.0\r\n\r\n";
                console.log('connecting to 127.0.0.1:'+TEST_LISTEN_PORT+' and sending request:');
                console.log(requestHeaders);
                var spawn = require('child_process').spawn;
                child = spawn("perl", ["test/send-http-request-and-dont-read.pl",requestHeaders]);
                child.stdout.on('data', (data) => {
                  console.log(`stdout: ${data}`);
                });

                child.stderr.on('data', (data) => {
                  console.error(`stderr: ${data}`);
                });

                child.on('close', (code) => {
                  console.log(`child process exited with code ${code}`);
                });

                return h.executionPromise
           })
           .then(v=>{
              return done(new Error("should have failed"))
           })
           .catch(err=>{
              console.log("!!!! in catch!", err)
              assert.ok(err.message.indexOf("Timeout or aborted") == 0)
              server.close()
              child.kill();
              done()
           })
           .catch(done)
    })

  function getUrl(h) {
     return 'http://localhost:'+TEST_LISTEN_PORT+'/tasks/'+h.id
  }

})

