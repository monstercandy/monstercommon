#!/usr/bin/perl
use strict;
use warnings;


local $| = 1;
binmode STDOUT;

my $wantedOutput = 210*1024;
my $read = 0;
my $highest100 = 0;
while(read(STDIN, my $buf, 1024)){
  $read += length($buf);
  my $a = int($read / 100000);
  if($a > $highest100) {
  	print "...Read so far: $read\n";
  }
}

print "Read $read bytes from STDIN, now sleeping 5 seconds, then sending $wantedOutput bytes.\n";
sleep(5);

my $SLEEP_AFTER_BYTES = 8000;
my $bytes_in_sleep_unit = 0;
my $outputSofar = 0;
my $lines = 0;
while(1){
  my $line= ("x"x1024)." $lines\n";
  $outputSofar += length($line);
  print $line or die "Cant write: $!";  
  last if($outputSofar > $wantedOutput);

  my $abytes_in_sleep_unit = int($outputSofar / $SLEEP_AFTER_BYTES);
  if($abytes_in_sleep_unit > $bytes_in_sleep_unit) {
    $bytes_in_sleep_unit = $abytes_in_sleep_unit;
    sleep(1);
  }

  $lines++;
}

print "Okay, thats all folks\n";
