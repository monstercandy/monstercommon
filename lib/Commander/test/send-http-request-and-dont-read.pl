#!/usr/bin/perl

use warnings;
use strict;
use IO::Socket::INET;

my $d = join(" ",@ARGV);
die "Usage: $0 header line\n" if(!$d);

my   $sock = IO::Socket::INET->new(PeerAddr => '127.0.0.1',
                                 PeerPort => '18791',
                                 Proto    => 'tcp') or die "Cant connect! $!";
print "sending: $d\n";
print $sock "$d\r\n\r\n";

sleep(1000);
