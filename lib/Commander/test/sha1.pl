#!/usr/bin/perl
use strict;
use warnings;
use Digest::SHA1  qw(sha1 sha1_hex sha1_base64);

local $| = 1;

my $in = <STDIN>;
printf STDERR "Just read %d bytes, %s\n", length($in), unpack("H*", $in);

print sha1_hex(<STDIN>);
