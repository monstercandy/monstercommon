#!/usr/bin/perl
use strict;
use warnings;

binmode STDOUT;
map { print chr($_) } (0..255);
