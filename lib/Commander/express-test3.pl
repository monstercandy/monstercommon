#!/usr/bin/perl

use strict;
use warnings;
use IO::Socket::INET;
use constant BYTES => 10000;

my $host = shift;

die "Usage: $0 host" if(!$host);


my $sock = IO::Socket::INET->new(PeerAddr => $host,
                                 PeerPort => 52123,
                                 Proto    => 'tcp') or die "cant: $!";

$sock->autoflush(1);

print $sock "POST /tasks/foo HTTP/1.1\r\nContent-Length: ".BYTES."\r\nConnection: keep-alive\r\n\r\n";
my $i = 0;
while($i < BYTES) {
  print STDERR "round: $i\n";
  print $sock "x";
  $i++;
  sleep(1);
}
