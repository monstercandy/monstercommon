
var ore = function(app, tests, additionalOptions) {
   if((!additionalOptions)||(typeof additionalOptions !== "object")) additionalOptions = {"timeout": additionalOptions}

   var timeout = additionalOptions.timeout || 5000
  if(!timeout) timeout = 5000
  
  process.env.NODE_ENV= "production"

  describe('MonsterExpress', function() {
        this.timeout(timeout);


        var serverInfo
		before(function(done){
	  
	       console.log("cleaning up")
	       app.Cleanup()
	         .then(app.Start)
		     .then(function(aServerInfo){
			      serverInfo = aServerInfo
				  done()
		     })

		});

        const MApi = require("MonsterClient")
        var mapiparams = {"host":"127.0.0.1", "port": app.config.get("listen_port"), "scheme": "http"}
        if(app.config.tokenapi)
        	mapiparams.auth_token = "test_token"
        else
        	mapiparams.api_secret= app.config.getApiSecret()
        var mapi = MApi(mapiparams, app.config)

        if(app.config.tokenapi) {
            var mapiparams_internal = simpleCloneObject(mapiparams)
            delete(mapiparams_internal["auth_token"])
            mapiparams_internal["api_secret"] = app.config.getApiSecret()
        	mapi.internal = MApi(mapiparams_internal, app.config)
            mapi.internal.setApiPrefix(app.config.appRestPrefix)
        }



        var assert = ore.assert

	  	assert.internal_error = function(err, result, httpResponse, done) {
		   return assert.app_error(err, result, httpResponse, "INTERNAL_ERROR", done)
		}
		assert.validation_error = function (err, result, httpResponse, done) {
		   assert.app_error(err, result, httpResponse, "VALIDATION_ERROR")
		   assert.isNotNull(err.errorParameters)
		   return done()
		}

		assert.app_error = function(err, result, httpResponse, expected_error, done) {
		 	assert.isNotNull(err)
		    assert.equal(httpResponse.statusCode, 500)
		    assert.equal(err.message, expected_error)
		    if(done)
		    	done()  
		}

    	describe('ping', function() {

		    it('pong', function() {

	             return assert.isFulfilled(
		                mapi.pingAsync()
		                .then((res)=>{
							mapi.setApiPrefix(app.config.appRestPrefix)
		                	
			             	assert.equal(res.httpResponse.statusCode, 200)
			             	assert.equal(res.result, "pong", "ping-pong")

	                    })) //.catch(ex=>{console.log("XXXXXXX",ex)})

		    });

		})


  	    tests(mapi, assert)


		after(function(){
			serverInfo.server.close()
		})



	
  })

  
}

ore.MockedEventClientLib = function(server){
   var re = {}
   re.LogAsync = function(d){
      // console.log("Mocked LogAsync", d)
      return Promise.resolve()
   }
   return re
}
ore.moduleOptions = {
	EventClientLib: ore.MockedEventClientLib,
	NoMorgan: true,
}

var chai = require("chai");
var chaiAsPromised = require("chai-as-promised"); 
chai.use(chaiAsPromised);
ore.assert = chai.assert

module.exports = ore
