
const dotq = require( "../../MonsterDotq")

var tokenLib = require( "../lib/index.js")
var assert = require("chai").assert

describe('pseudo', function() {    
    "use strict"

    it('token', function(done) {

          tokenLib.GetPseudoToken("password", "salt", 1466194067, 16, (err, res)=>{
              if(err) done(err)
              assert.equal(res, "ue8f1R5gLYDqBPZOVhRJnQ==") 
              done()
          })

    })

    it('when buffer requested, it should not be encoded', function(done) {

          tokenLib.GetToken(8, function(err,d){
              if(err) done(err)
              assert.ok(typeof d == "object") 
              done()

          }, "buffer")

    })


    it('pbkdf2 with default options', function(done) {

          tokenLib.GetPasswordHash("password", {dotq: dotq})
            .then(d=>{
              // console.log(d)
              Array("salt","salt_length","pbkdf2_iterations","pbkdf2_output_length","pbkdf2_hash_algo","password_hash")
                .forEach(q=>{
                   assert.property(d, q)                  
                })
              done()

            })
            .catch(done)

    })


})
