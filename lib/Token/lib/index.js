var re = module.exports = {}

  const crypto = require("crypto")

re.GetToken = function(bytesCount, callback, encoding) {
   crypto.randomBytes(bytesCount || 8, (err, rnd)=>{
   	  if(err) return callback(err)
   	  return callback(null, encoding == "buffer" ? rnd : rnd.toString(encoding || 'base64'))
   })
}

re.GetTokenAsync = function(bytesCount, encoding) {
  return new Promise(function(resolve, reject){
  	 re.GetToken(bytesCount, function(err, token){
        if(err) return reject(err)
        resolve(token)
     }, encoding)
  })
  
} 

re.GetPseudoToken = function(password, salt, timestamp, bytesCount, callback, encoding) {
  if(!encoding) encoding = "base64"
  if(!timestamp) timestamp = new Date().unixtime()
  if(!bytesCount) bytesCount = 16

  crypto.pbkdf2(password+timestamp, salt+timestamp, 10, bytesCount, 'sha256', (err, key) => {
    if (err) return callback(err);
    return callback(null, key.toString(encoding));  // 'c5e478d...1469e50'
  });
}

re.GetPseudoTokenAsync = function(password, salt, timestamp, bytesCount, encoding) {

  return new Promise(function(resolve, reject){
     re.GetPseudoToken(password, salt, timestamp, bytesCount, function(err, token){
        if(err) return reject(err)
        resolve(token)
     }, encoding)
  })
}

re.GetPasswordHashSalt = function(plainPassword, salt, configHash){
      const pbkdf2 = (configHash.dotq || require("MonsterDotq")).single(crypto.pbkdf2)

      if(!configHash) configHash = {}

      var re = {
        pbkdf2_iterations: configHash.pbkdf2_iterations || 1000,
        pbkdf2_output_length: configHash.pbkdf2_output_length || 16,
        pbkdf2_hash_algo: configHash.pbkdf2_hash_algo || "sha256",
      }

      return pbkdf2(plainPassword, salt, re.pbkdf2_iterations, re.pbkdf2_output_length, re.pbkdf2_hash_algo)         
        .then(key=>{
          re.password_hash = key.toString("hex")
          // console.log("key with appended salt is", re)
          return Promise.resolve(re)
        })
  }


re.GetPasswordHash = function(plainPassword, configHash){
    var salt 

      if(!configHash) configHash = {}

     var p = configHash.salt ? 
        Promise.resolve(configHash.salt) : 
        re.GetTokenAsync(configHash.salt_length || 8, "buffer")

     return p.then(asalt=>{
          salt = asalt

          return re.GetPasswordHashSalt(plainPassword, salt, configHash)
        })
        .then(re=>{
           re.salt_length = salt.length
           re.salt = salt.toString("hex")
           return Promise.resolve(re)
        })
  }

