var MError = require("MonsterError")

const util = require("util")

var lib = module.exports = function(serviceOptions, config) {
  var host, port, username, password, scheme, timeout, maxSockets

  if(!config) config = {}

  if(typeof(serviceOptions) !== "object")
      throw new MError("INVALID_PARAMETER")

  host = serviceOptions["ip"] || serviceOptions["host"]
  username = serviceOptions["username"]
  password = serviceOptions["password"]
  scheme = serviceOptions["scheme"] || "http"
  port = serviceOptions["port"]
  timeout = serviceOptions["timeout"] ||  config["timeout"] ||  (config.get ? config.get("timeout") : null)

/*
  maxSockets = Infinity
  if((config.get)&&(config.get("maxSockets")))
    maxSockets =config.get("maxSockets")
*/

  if((!username)||(!password))
      throw new MError("CREDENTIALS_MISSING")


  function getFullUrl(uri){
      var x =`${scheme}://${host}`
      if(port)
          x += `:${port}`
      x += uri
      return x
  }

  var auth_token


  var re = {}

  re.getApiType = lib.getApiType

  re.SetAuthToken = function(token){
    auth_token = token
  }

  re.requestLib = require("RequestCommon")

  re.doRequestStream = function(method, uri, payload, readyCallback) {
      var payloadStr = payload
      if((payload)&&(!util.isString(payload)))
        payloadStr = JSON.stringify(payload)

      var fullUrl = getFullUrl(uri)
      var params = {
          method: method,
          url: fullUrl,
          // we dont do this anymore, as https globalAgent CA would be discarded then by request lib
          //pool: {maxSockets: maxSockets},
          body: payloadStr,
          headers: {}
      }
      if(timeout) { // msecs is not a number issue
          params.timeout = timeout;
      }

      if(auth_token)
          params.headers.MonsterToken= auth_token

      if(re.extraHeaders) {
         Object.keys(re.extraHeaders).forEach((k)=>{
            params.headers[k] = re.extraHeaders[k]
         })        
      }


      var req = re.requestLib(params)
      .on('error', function(err) {
          console.error("Error while network request", {method: method, host: host, port: port, uri: uri, username: username}, err);
          return readyCallback(new MError("NETWORK_ERROR", null, err))
      })

      return req

  }

  var loginPromise
  function loginAsync(){
      if(!loginPromise) {
          loginPromise = new Promise(function(resolve,reject){

                // note: we need to manually insert api prefix here, since it is normally added in MonsterClient

                // we use this harcoded uri here, since it must always be like that, it would never be possible that it would be remapped
                return requestInternal("POST", "/api/pub/account/authentication", {"username":username,"password":password},function(err,res){
                    loginPromise = null
                    if(err) return reject(err) // auto login failed

                    auth_token = res.token
                    if(!auth_token)
                      return reject(new MError("INTERNAL_ERROR")) // auto login failed

                    resolve()
                }, false)

          })
      }
      return loginPromise
  }
  re.login = function(loginReadyCallback){
     return loginAsync()
       .then(loginReadyCallback)
       .catch(loginReadyCallback)
  }

  function requestInternal(method, uri, payload, readyCallback, try_to_relogin) {
      var str = ''
      var req = re.doRequestStream(method, uri, payload, readyCallback)
      req.on('response', function(response) {
          response.on('data', function(chunk) {
                str += chunk;
          })
          response.on('end', function () {

              // console.log("ProxyTapi str result:", str)
              var j
              try {

                 j = JSON.parse(str)

                 if((j.result)&&(j.result.error)) {
                    // to be a little bit more robust
                    if(j.result.error.message.match(/^(INVALID_TOKEN_OR_SESSION_EXPIRED|SESSION_EXPIRED|AUTH_FAILED)$/))
                    {
                        if(try_to_relogin) {
                           // Our auth token has probably expired. Lets try to relogin


                           return re.login((err)=>{
                              if(err) return readyCallback(err)
                              return requestInternal(method, uri, payload, readyCallback, false)
                           })
                        }

                        return readyCallback(new MError("INTERNAL_ERROR"), j["result"], response);
                    }

                    console.error("API call failed", method, uri, j.result.error);
                    return readyCallback(new MError(j.result.error.message, j.result.error.params), j["result"], response);
                 }

                 // everything is fine
                 return readyCallback(null, j["result"], response)

              }catch(ex){
                 console.log("exception was thrown", req.method, req.path, ex)
                 return readyCallback(new MError("INTERNAL_ERROR", null, ex))
              }

          })
      })

      return req

  }

  re.doRequest = function(method, uri, payload, readyCallback) {

      if(!auth_token) {

         return re.login((err)=>{
            if(err) return readyCallback(err)
            return requestInternal(method, uri, payload, readyCallback, false)
         })
      }

      return requestInternal(method, uri, payload, readyCallback, true)
  }


  return re
}

lib.getApiType = function(){
    return "public"
}
