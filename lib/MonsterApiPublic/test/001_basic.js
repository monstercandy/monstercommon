require( "../../MonsterDependencies")(__dirname)

var apilib = require( "../lib/index.js")

var apiparams = {
    scheme: "https",
    host: "127.0.0.1",
    username: "user",
    password: "pass",
}
var loginExpectation = {
     method: "POST", 
     url: "https://127.0.0.1/api/pub/account/authentication", 
     body: {username:apiparams.username,password:apiparams.password}, 
     returnValue: {result:{token:"12345"}}
}
var pingExpectation = {
    "method": "GET",
    "url": "https://127.0.0.1/ping",
    "returnValue": {"result":"pong"},
    "header": true
}

var assert = require("chai").assert

describe('login', function() {
    it('direct login', function(done) {

         var api = new apilib(apiparams)
         api.requestLib = mockedRequestLib(
             loginExpectation
         )

         api.login(function(err){
            done(err)  
         })

    })

    it('auto login (no token yet)', function(done) {

         var api = new apilib(apiparams)
         api.requestLib = mockedRequestLib(
             loginExpectation,
             ()=>{
                api.requestLib = mockedRequestLib(pingExpectation)
             }
         )

         api.doRequest("GET", "/ping", "", function(err, res){
            assert.equal(res, "pong")
            done(err)  
         })

    })

    it('auto login (expired token)', function(done) {

         var api = new apilib(apiparams)
         api.SetAuthToken("nope")

         api.requestLib = mockedRequestLib(
             pingExpectation,
             ()=>{
                api.requestLib = mockedRequestLib(
                    loginExpectation,
                    ()=>{
                        api.requestLib = mockedRequestLib(
                             pingExpectation
                        )

                    }
                )
             }
         )

         api.doRequest("GET", "/ping", "", function(err, res){
            assert.equal(res, "pong")
            done(err)  
         })

    })

})


function mockedRequestLib(expect, mockReadyCallback) {

  return function(params) {
            assert.equal(params.method, expect.method)
            assert.equal(params.url, expect.url)
            if(expect.body)
              assert.equal(params.body, JSON.stringify(expect.body))

            if((expect.header)&&((!params.headers)||(!params.headers["MonsterToken"])))
                assert.fail("TOKEN_NOT_SENT")

            var re = {}
            re.on = function(event, requestCallback) {

                if(event == "response"){
                    var response = {}
                    response.on = function(event, responseCallback){
                        if(event == "data")
                            responseCallback(JSON.stringify(expect.returnValue))
                        if(event == "end") {
                            if(mockReadyCallback)
                                mockReadyCallback()

                            responseCallback(null)
                        }
                        return response
                    }
                    requestCallback(response)
                }
                return re
            }
            return re
  }
}
