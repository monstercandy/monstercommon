var lib = module.exports = function(filename, readyCallback, customParser) {

        const MError = require("MonsterError")

        const readline = require('readline');
        const fs = require('fs');

        const stream = fs.createReadStream(filename)
        stream.on('error', function (err) {
            readyCallback(err)
        });

        const rl = readline.createInterface({
          input: stream
        });

        var lineRead = false
        rl.once('close', () => {
            if(lineRead) return
            readyCallback(null, {})
        })

        rl.once('line', (line) => {

             lineRead = true

             try {
                 if(customParser) return readyCallback(null, customParser(line, originalParser))

                 readyCallback(null, originalParser(line))

             } catch(ex){
                 readyCallback(ex)

             }

        });

        function originalParser(line){

             if(line.length < 3)
                return readyCallback(new MError("INVALID_FIRST_LINE_IN_DOMAIN_FILE"))
             if(line.charAt(0) != "#")
                return readyCallback(new MError("INVALID_FIRST_CHAR_IN_DOMAIN_FILE"))

             var recs = JSON.parse(line.substr(1))

             return recs
        }

}

lib.ReadAsync = function(filename, customParser)  {

        return new Promise(function(resolve,reject){

                lib(filename, function(err, result){
                   if(err) return reject(err)

                   return resolve(result)
                }, customParser)

         })

}

lib.ReadFileLineByLineAsync = function(filename, callback){

     return new Promise(function(resolve,reject){

            const readline = require('readline');
            const fs = require('fs');
            const stream = fs.createReadStream(filename);
            stream.on('error', function (err) {
                return reject(err)
            });


            const rl = readline.createInterface({
              input: stream
            });

            rl.on('line', callback);
            rl.once('close', () => {
                return resolve();
            })
     })

}
