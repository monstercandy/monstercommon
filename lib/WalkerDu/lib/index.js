(function(){

	const commanderLib = require("../../Commander");
    const dotq = require( "../../MonsterDotq");
    const fs = dotq.fs();
    const path = require("path");
    var commander = commanderLib();

	const supportedUnits = {
		"mb": "-m",
		"kb": "-k",
	}
	const supportedUnitsDivide = {
		"mb": 1024*1024,
		"kb": 1024,
	}
	
	var walkerMainFun = module.exports = function(options){
	    for(let q of ["basedir"]) {
	    	console.log(q)
		   if(!options[q]) throw new Error(q+" parameter is mandatory");
	    }
	    if(!options.unit) options.unit = "mb";
	    var unitOption = supportedUnits[options.unit];
	    if(!unitOption) throw new Error("Unsupported unit");

        var mainResult = {};
        var mainDirFilesInBytes = 0;

        return Promise.resolve()
          .then(()=>{
          	  if(options.targets)
          	  	  return options.targets;
          	  
          	  return fs.readdirAsync(options.basedir, {})
          	    .then((names)=>{
          	    	 var re = [];
          	    	 return dotq.linearMap({array:names, catch: true, action: name=>{
          	    	 	var n = path.join(options.basedir, name);
          	    	 	return fs.lstatAsync(n)
          	    	 	  .then(dirent=>{
          	    	 	  	 if(dirent.isDirectory())
             	    	 		re.push(name);
             	    	 	 else
             	    	 	 	mainDirFilesInBytes += dirent.size;
          	    	 	  })
          	    	 }})
          	    	 .then(()=>{
          	    	 	 var x = Math.ceil(mainDirFilesInBytes / supportedUnitsDivide[options.unit]);
          	    	 	 if(x > 0)
          	    	 	 	mainResult[''] = x;

          	    	 	 return re;
          	    	 })
          	    })
          })
          .then((targets)=>{
          	  return dotq.linearMap({array: targets, catch: true, action: target=>{
          	  	   var duCmdArgs = [unitOption, "--one-file-system"];
          	  	   if(!options.emitter) {
          	  	      duCmdArgs.push("-s"); // just the summary line
          	  	   }
          	  	   // and the target finally
          	  	   duCmdArgs.push(target);
          	  	   var spawnConfig = {
          	  	   	   dontLog_stdout: true,
			           omitAggregatedStderr: true,
			           omitControlMessages: true,
                       emitter: options.emitter, 
                       chain: [
                          {
                          	spawnOptions: {
                          		cwd: options.basedir,
                          	},
                          	executable: options.duCmd || "du", 
                          	args: duCmdArgs
                          }
                       ],
          	  	   };
                   return commander.spawn(spawnConfig)
	               .then(h=>{
	               	  return h.executionPromise;
	               })
	               .then((x)=>{
                      var str = walkerMainFun.getLastLine(x.output.toString());
					  var mainNumberRegexp = /^([0-9]+)\s+/; 
                      var matches = mainNumberRegexp.exec(str);
                      if((matches)&&(matches[1]))
                      {
                      	var number = parseInt(matches[1], 10);
                      	if((number)||(!options.dontReportEmptyDirs))
                      	{
                      	   mainResult[target] = number;                      		
                      	}
                      }
	               })
          	  }})
          	  .then(()=>{
          	  	 return mainResult;
          	  })
          })
	}

    walkerMainFun.getLastLine = function(s){
        var lastLineRegexp = /(.*$)/g; 
		var matches = lastLineRegexp.exec(s.trim());
		if(!matches) {
			return s;
		}
		return matches[1];
	}

	return walkerMainFun;


})()
