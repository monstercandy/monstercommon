
require( "../../MonsterDependencies")(__dirname)

var walkerLib = require( "../lib/index.js")
var assert = require("chai").assert

describe('basic', function() {    
    "use strict"

    it('last line', function() {
       var l = walkerLib.getLastLine(`
100     ./fonts/conf.d
104     ./fonts
644     ./licenses
468     ./mplayer
63888   .
`);
       assert.equal(l, "63888   .");
    });

    it('simple walking', function() {

          return walkerLib({
            basedir: ".",
            unit: "kb",
            duCmd: "C:\\tools\\coreutils\\du.exe"
          })
          .then(results=>{
              assert.deepEqual(results, {
                "": 2,
                "node_modules": 620,
                "lib": 4,
                "test": 4,
              })
          })

    })

})
