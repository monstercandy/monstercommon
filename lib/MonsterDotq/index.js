var lib = module.exports = function(re, limit) {
    "use strict"

    for(let q of Object.keys(re)) {

        if(q.match(/Async$/)) continue;
        if(q.match(/Sync$/)) continue;
        if(!isFunction(re[q])) continue;
        if((limit)&&(-1 == limit.indexOf(q))) continue;

        re[q+"Async"] = lib.single(re[q])

    }


    function isFunction(functionToCheck) {
     var getType = {};
     return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
    }
}

lib.sleep = function(ms){
  return new Promise((resolve,reject)=>{
      return setTimeout(resolve, ms);
  })
}

lib.single = function(fn){
    return function() {
        var args = arguments

        return new Promise(function(resolve,reject){

              // console.log("inside here",args, Object.keys(args), Object.keys(args).length)

              var mainArguments = Array.prototype.slice.call(args);
              mainArguments.push(function(err,res){
                    if(err) return reject(err)

                    resolve(res)
                });

             // console.log("applying",q,"params",mainArguments)

            fn.apply(null, mainArguments)
        })

    }

}

lib.linearMap = function(params) {
   if(typeof params != "object")   throw new Error("invalid params")
   if(typeof params.action != "function")   throw new Error("invalid action")
   if(!Array.isArray(params.array))   throw new Error("invalid array")

   var mainResult = []
   var arr = params.array.slice(0)
   var i = 0
   return new Promise(function(resolve,reject){
      next()

      function next(){
         var a = arr.shift()
         if(typeof a == "undefined") return resolve(mainResult) // end of chain
         var re
         try {
            re = params.action(a, i)
            if((!re)||(!re.then))
              re = Promise.resolve(re)
         }catch(ex){
            re = Promise.reject(ex)
         }

         return re.catch(err=>{
              if(params.catch) {
                 if(typeof params.catch == "function")
                   return params.catch(err, a, i)

                 console.error("Error during execution", err)
                 return Promise.resolve()
              }
              throw err
           })
           .then((re)=>{
              if(params.collectResult)
                mainResult.push(re)

              i++
              return next()
           })
           .catch(reject)
      }
   })
}

var fsAsync
lib.fs = function() {
    if(fsAsync)return fsAsync
    fsAsync =require("fs")
    lib(fsAsync)

    fsAsync.ensureRemovedAsync = function(filename){
       return enoentHandler(fsAsync.unlinkAsync(filename));          
    }
    fsAsync.ensureRmdiredAsync = function(dirname){
       return enoentHandler(fsAsync.rmdirAsync(dirname));          
    }

    return fsAsync

    function enoentHandler(p){
      return p.catch(ex=>{
        if(ex.code != "ENOENT") throw ex // this is for robustness: we don't treat non existing webserver config files to be an error during cleanup      
      })
    }
}
