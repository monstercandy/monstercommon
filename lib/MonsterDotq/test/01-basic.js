
var dotqLib = require( "../index.js")
var assert = require("chai").assert

describe('pseudo', function() {
    "use strict"

    it('one arg', function() {

    	var re = {
    		"fn": function(p1, cb){
    			 cb(null, p1+1)
    		}
    	}

        dotqLib(re)

        return re.fnAsync(3)
          .then((d)=>{
          	  assert.equal(d, 4)
          })

    })


    it('two args', function() {

    	var re = {
    		"add": function(p1, p2, cb){
    			 cb(null, p1+p2)
    		}
    	}

        dotqLib(re)

        return re.addAsync(3,4)
          .then((d)=>{
          	  assert.equal(d, 7)
          })

    })


    it('exception', function(done) {

    	var re = {
    		"ex": function(p1, cb){
    			 cb(new Error("rather"))
    		}
    	}

        dotqLib(re)

        re.exAsync(3)
          .then((d)=>{
          	  done(new Error("should have been an exception"))
          })
          .catch(ex=>{
          	  done();
          })

    })

    it('filter', function(done) {

    	var re = {
    		"f1": function(p1, cb){
    			 cb(p1)
    		},
    		"f2": function(p1, cb){
    			 cb(p1)
    		}
    	}

        dotqLib(re,["f2"])

        assert.notProperty(re, "f1Async")
        assert.property(re, "f2Async")
        done()
    })
})

describe('linerMap', function() {
   it("basic, returning static value", function(){
      var input = [1,2,3]
      var indexOuter = 0
      return dotqLib.linearMap({array:input, collectResult: true, action: function(x, indexInner){
         assert.equal(indexOuter, indexInner)
         assert.equal(x, input[indexInner])
         indexOuter++
         return x*2
      }})
      .then((re)=>{
         assert.deepEqual(re, [2,4,6])
      })
   })

   it("exception should stop execution by default", function(){
      var input = [1,2,3]
      return dotqLib.linearMap({array:input, collectResult: true, action: function(x, indexInner){
         assert.equal(0, indexInner)
         throw new Error("this one should be delegated")
      }})
      .then((re)=>{
        throw new Error("should not be here")
      })
      .catch(err=>{
         assert.equal(err.message, "this one should be delegated")
      })
   })

   it("exception should be swalled with catch mode", function(){
      var input = [1,2,3]
      var indexOuter = 0
      return dotqLib.linearMap({array:input, catch: true, collectResult: true, action: function(x, indexInner){
         assert.equal(indexOuter, indexInner)
         assert.equal(x, input[indexInner])
         indexOuter++
         throw new Error("just for sure")
      }})
      .then((re)=>{
         assert.deepEqual(re, [undefined, undefined, undefined])
      })
   })

   it("should work well with zero in the list", function(){
      var input = [1,0,3]
      var re = 0;
      return dotqLib.linearMap({array:input, action: function(x, indexInner){
         re+= x;
      }})
      .then(()=>{
         assert.equal(re, 4)
      })
   })


    it('sleep should work', function() {

        return dotqLib.sleep(500)

    })
})

