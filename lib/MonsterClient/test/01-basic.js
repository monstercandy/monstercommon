require( "../../MonsterDependencies")(__dirname)

var MonsterClientLib = require( "../lib/index.js")
var assert = require("chai").assert

describe('pseudo', function() {

    it('calling get, success', function(done) {
        const uriToFetch = "/foo"
        const dataToReturn = "xxx"
        var pool = {
           "GetRawServers": function(){
              return [{"name":"server1"},{"name":"server2"}]
           }
        }

        var clientLib = function(server){
           assert.deepEqual(server, {"name":"server1"})

           return {
              getAsync: function(uri) {
                 assert.equal(uri, uriToFetch)
                 return Promise.resolve(dataToReturn)
              }
           }
        }

        MonsterClientLib.getMapiPool(pool, {clientLib: clientLib})
          .getAsync(uriToFetch)
          .then(d=>{
             assert.equal(d, dataToReturn)

             done()

          })
          .catch(done)
    })

    it('calling get, fail', function(done) {
        const uriToFetch = "/foo"
        const errorMessage = "Sorry"

        var pool = {
           "GetRawServers": function(){
              return [{"name":"server1"},{"name":"server2"}]
           }
        }

        var clientLib = function(server){
           assert.deepEqual(server, {"name":"server1"})

           return {
              getAsync: function(uri) {
                 assert.equal(uri, uriToFetch)
                 return Promise.reject(new Error(errorMessage))
              }
           }
        }

        MonsterClientLib.getMapiPool(pool, {clientLib: clientLib})
          .getAsync(uriToFetch)
          .then(x=>{
             done(new Error("should have failed"));
          })
          .catch(x=>{
             assert.equal(x.message, errorMessage)
             done()
          })
    })

    it('calling post, success', function(done) {

        const uriToFetch = "/foopost"
        const dataToSend = {"a":"b"}
        const dataToReturn = "xxx"
        var pool = {
           "GetRawServers": function(){
              return [{"name":"server1"},{"name":"server2"}]
           }
        }

        var serversTouched = {}
        var clientLib = function(server){
           if(!serversTouched[server.name]) serversTouched[server.name] = 0
           serversTouched[server.name]++

           return {
              postAsync: function(uri, payload) {
                 assert.deepEqual(payload, dataToSend)
                 assert.equal(uri, uriToFetch)
                 return Promise.resolve(dataToReturn)
              }
           }
        }

        MonsterClientLib.getMapiPool(pool, {clientLib: clientLib})
          .postAsync(uriToFetch, dataToSend)
          .then(d=>{
             assert.deepEqual(serversTouched, {"server1":1, "server2": 1})
             assert.equal(d, dataToReturn)

             done()
          })
          .catch(done)

    })

    it('calling post, success, firstServerOnly', function(done) {

        const uriToFetch = "/foopost"
        const dataToSend = {"a":"b"}
        const dataToReturn = "xxx"
        var pool = {
           "GetRawServers": function(){
              return [{"name":"server1"},{"name":"server2"}]
           }
        }

        var serversTouched = {}
        var clientLib = function(server){
           if(!serversTouched[server.name]) serversTouched[server.name] = 0
           serversTouched[server.name]++

           return {
              postAsync: function(uri, payload) {
                 assert.deepEqual(payload, dataToSend)
                 assert.equal(uri, uriToFetch)
                 return Promise.resolve(dataToReturn)
              }
           }
        }

        MonsterClientLib.getMapiPool(pool, {clientLib: clientLib})
          .postAsync(uriToFetch, dataToSend, {firstServerOnly: true})
          .then(d=>{
             assert.deepEqual(serversTouched, {"server1":1})
             assert.equal(d, dataToReturn)

             done()
          })
          .catch(done)

    })

    it('calling post, one of the servers fail', function(done) {

        const uriToFetch = "/foopost"
        const dataToSend = {"a":"b"}
        const dataToReturn = "xxx"
        var pool = {
           "GetRawServers": function(){
              return [{"name":"server1"},{"name":"server2"}]
           }
        }

        var serversTouched = {}
        var clientLib = function(server){
           if(!serversTouched[server.name]) serversTouched[server.name] = 0
           serversTouched[server.name]++

           return {
              postAsync: function(uri, payload) {
                 assert.deepEqual(payload, dataToSend)
                 assert.equal(uri, uriToFetch)

                 if(server == "server2")
                    return Promise.reject(new Error("Sorry"))

                 return Promise.resolve(dataToReturn)
              }
           }
        }

        MonsterClientLib.getMapiPool(pool, {clientLib: clientLib})
          .postAsync(uriToFetch, dataToSend)
          .then(d=>{
             assert.deepEqual(serversTouched, {"server1":1, "server2": 1})
             assert.equal(d, dataToReturn)

             done()
          })
          .catch(done)

    })

    it('calling post, one of the servers fail with throwIfAnyFailed', function(done) {

        const uriToFetch = "/foopost"
        const dataToSend = {"a":"b"}
        const dataToReturn = "xxx"
        const errorMessage = "Sorry"
        var pool = {
           "GetRawServers": function(){
              return [{"name":"server1"},{"name":"server2"}]
           }
        }

        var serversTouched = {}
        var clientLib = function(server){
           if(!serversTouched[server.name]) serversTouched[server.name] = 0
           serversTouched[server.name]++

           return {
              postAsync: function(uri, payload) {
                 assert.deepEqual(payload, dataToSend)
                 assert.equal(uri, uriToFetch)

                 if(server.name == "server2")
                    return Promise.reject(new Error(errorMessage))

                 return Promise.resolve(dataToReturn)
              }
           }
        }

        MonsterClientLib.getMapiPool(pool, {clientLib: clientLib, throwIfAnyFailed: true})
          .postAsync(uriToFetch, dataToSend)
          .catch(x=>{
             assert.deepEqual(serversTouched, {"server1":1, "server2": 1})
             assert.equal(x.message, errorMessage)
             done()
          })

    })

    it('calling post, all servers fail', function(done) {

        const uriToFetch = "/foopost"
        const dataToSend = {"a":"b"}
        const dataToReturn = "xxx"
        const errorMessage = "Sorry"
        var pool = {
           "GetRawServers": function(){
              return [{"name":"server1"},{"name":"server2"}]
           }
        }

        var serversTouched = {}
        var clientLib = function(server){
           if(!serversTouched[server.name]) serversTouched[server.name] = 0
           serversTouched[server.name]++

           return {
              postAsync: function(uri, payload) {
                 assert.deepEqual(payload, dataToSend)
                 assert.equal(uri, uriToFetch)

                 return Promise.reject(new Error(errorMessage))
              }
           }
        }

        MonsterClientLib.getMapiPool(pool, {clientLib: clientLib})
          .postAsync(uriToFetch, dataToSend)
          .catch(x=>{
             assert.deepEqual(serversTouched, {"server1":1, "server2": 1})
             assert.equal(x.message, errorMessage)
             done()
          })

    })


        var pool = {
           internal: {
             "GetRawServers": function(){
                return [{api_secret: "sdf"}]
             }
           },
           relay: {
             "GetRawServers": function(){
                return [{auth_token: "sdf"}]
             }
           },

           public: {
             "GetRawServers": function(){
                return [{username: "sdf"}]
             }
           },
        }

        Object.keys(pool).forEach(type=>{
            it('getApiType should return the correct api type: '+type, function(done) {

                    var m = MonsterClientLib.getMapiPool(pool[type])
                    assert.equal(m.getApiType(), type)
                done()

            })
        })

});


describe('caching', function() {

    it('getting new mapi objects on the same pool should be the same cached version', function() {

        var pool = {
           "GetRawServers": function(){
              return [{"name":"server1"},{"name":"server2"}]
           }
        }

        var r1 = MonsterClientLib.getMapiPoolCached(pool)
        var r2 = MonsterClientLib.getMapiPoolCached(pool)

        assert.equal(r1.mapiPoolId, r2.mapiPoolId);
    });


    it('getting new mapi objects on different pools should return different object', function() {

        var pools = [];
        Array(1,2).forEach(i=>{
          pools.push({
             GetVolumeName: function(){
                return "pool"+i;
             },
             GetRawServers: function(){
                return [{"name":"server"+i}]
             }
          })
        });

        var r1 = MonsterClientLib.getMapiPoolCached(pools[0])
        var r2 = MonsterClientLib.getMapiPoolCached(pools[1])

        assert.notEqual(r1.mapiPoolId, r2.mapiPoolId);
    });



    it('getting new mapi objects on same pool but with differnt options should return different objects', function() {

        var pool = {
           "GetRawServers": function(){
              return [{"name":"server1"}]
           }
        }

        var r1 = MonsterClientLib.getMapiPoolCached(pool);
        var r2 = MonsterClientLib.getMapiPoolCached(pool, {"a":1});
        var r3 = MonsterClientLib.getMapiPoolCached(pool, {"b":2});

        assert.notEqual(r1, r2);
        assert.notEqual(r1, r3);
        assert.notEqual(r2, r3);
    });

});

