(function(){
const MError = require("MonsterError")

var ore = function(serverOptions){
  var xargs = arguments
  function getApiModule() {
    var api = ore.getApiModuleLib(serverOptions)
    if(!api)
        throw new Error("INVALID_SERVER_ARGUMENTS")

    return api.apply(this, xargs)
  }

  var apiPrefix = ""
  var re = getApiModule()

  re.extraHeaders = {}
  re.SetupFieldsByRequest = function(req, forwardedForSecret) {
    re.SetUserAgent(req.userAgent);
    re.SetXForwardedFor(req.clientIp);
    if(forwardedForSecret) {
      re.extraHeaders["MonsterForwardedFor"] = req.clientIp;
      re.extraHeaders["MonsterForwardedForSecret"] = forwardedForSecret;
      re.extraHeaders["MonsterServer"] = req.clientServer;
    }
    // header fields are lower cased: https://nodejs.org/api/http.html#http_message_headers
    if(typeof req.headers['monsterserverindex'] !== "undefined")
       re.extraHeaders["MonsterServerIndex"] = req.headers['monsterserverindex']

    if((req.TokenUser)&&(req.TokenUser.account)&&(req.TokenUser.account.u_id))
       re.extraHeaders["MonsterAccountId"] = req.TokenUser.account.u_id;

    re.MonsterTime = req.GetMonsterTime();
    re.extraHeaders["MonsterTime"] = re.MonsterTime;
  }
  re.SetUserAgent = function(userAgent) {
      re.extraHeaders["User-Agent"] = userAgent
  }
  re.SetXForwardedFor = function(ip) {
      re.extraHeaders["X-Forwarded-For"] = ip
  }
  re.setApiPrefix = function(prefix) {
      apiPrefix = prefix
  }
  re.getApiPrefix = function() {
      return apiPrefix;
  }
  re.requestStream = function(method, uri, payload, readyCallback) {
     uri = apiPrefix + uri
     return re.doRequestStream(method, uri, payload, readyCallback)
  }
  re.request = function(method, uri, payload, readyCallback) {
     uri = apiPrefix + uri
     return re.doRequest(method, uri, payload, readyCallback)
  }
  re.requestAsync = function(method, uri, payload) {
     return new Promise(function(res,rej){
         re.request(method, uri, payload, function(err, result, httpResponse){

             if(err) return rej(err)

             var re = {"result": result, "httpResponse": httpResponse}

             res(re)

         })
     })
  }

  re.ping = function(callBack) {
      return re.get("/ping", callBack)
  }


  setupMethods()

  if(serverOptions.api_prefix)
    re.setApiPrefix(serverOptions.api_prefix)

  function setupMethods(){
     "use strict"

      for(let func of ["ping","login"]) {
          if(!re[func]) continue;

          re[func+"Async"] = function() {
              return new Promise(function(res,rej){
                 re[func]((err, result, httpResponse)=>{
                     if(err) return rej(err)
                     var re = {result: result, httpResponse: httpResponse}
                     res(re)
                 })
              })
          }
      }

      for(let method of ["GET"]) {
        let mlc = method.toLowerCase()
        re[mlc] = function(uri, callback) {
            return re.request(method, uri, "", callback)
        }
        re[mlc+"Async"] = function(uri) {
            return re.requestAsync(method, uri, "")
        }
      }

      for(let method of ["HEAD","SEARCH"]) {
        let mlc = method.toLowerCase()
        re[mlc] = function(uri, callback) {
            return re.request(method, uri, "", callback)
        }
        re[mlc+"Async"] = function(uri) {
            return re.requestAsync(method, uri, "")
        }
      }


      for(let method of ["POST","PUT","DELETE"]) {
        let mlc = method.toLowerCase()
        re[mlc] = function(uri,payload, callback) {
            return re.request(method, uri, payload, callback)
        }
        re[mlc+"Async"] = function(uri, payload) {
            return re.requestAsync(method, uri, payload)
        }
      }

  }

  return re
}



ore.getMapi = function(options, callback) {
        var server = options.server.FirstServer()
        if(!server) return callback(new MError("SERVICE_NOT_CONFIGURED"))

        var mapi = ore(server)
        return callback(null, mapi)
  }


var mapiPoolCache = {};
ore.getMapiPoolCached = function(serversRouter, poolOptions)
{
     var oPoolOptions = JSON.stringify(poolOptions);
     var serversRouterName = serversRouter.GetVolumeName ? serversRouter.GetVolumeName() : serversRouter;
     if((mapiPoolCache[serversRouterName])&&(mapiPoolCache[serversRouterName][oPoolOptions]))
       return mapiPoolCache[serversRouterName][oPoolOptions];
     if(!mapiPoolCache[serversRouterName])
       mapiPoolCache[serversRouterName] = {}

     var re = mapiPoolCache[serversRouterName][oPoolOptions] = ore.getMapiPool(serversRouter, poolOptions);
     return re;
}

var mapiPoolId = 0;
ore.getMapiPool = function(serversRouter, poolOptions)
{

     if(!serversRouter) throw new MError("SERVICE_NOT_CONFIGURED")
     if(!poolOptions) poolOptions = {}
     if(!(poolOptions.serverSelectionAlgo||"").match(/^(first)$/))
       poolOptions.serverSelectionAlgo = "first"

     const getMethods = Array("GET","HEAD","SEARCH")

     var gSetupFieldsByRequest
     var gSetXForwardedFor
     var gForwardedForSecret
     var re = {mapiPoolId: (serversRouter.GetVolumeName? serversRouter.GetVolumeName() : "") + (mapiPoolId++) };
     re.SetupFieldsByRequest = function(req, forwardedForSecret) {
        gSetupFieldsByRequest = req;
        gForwardedForSecret = forwardedForSecret;
     }
     re.SetXForwardedFor = function(fwrd) {
        gSetXForwardedFor = fwrd
        re.AddHeader()
     }

     re.GetRawServers = function() {
        var servers = serversRouter.GetRawServers()
        if(!servers.length) throw new MError("SERVICE_NOT_CONFIGURED_SERVERS")
        return servers
     }


     re.request = function(requestOptions, callback) {
        re.requestAsync(requestOptions)
          .then((d)=>{
             callback(null, d.result, d.poolErrors)
          })
          .catch(x=>{
             callback(x)
          })
     }

     re.getApiPrefix = function(){
        var servers = re.GetRawServers();
        return servers[0].api_prefix;
     }

     re.requestAsync = function(requestOptions) {
        var suc = []
        var rej = []
        if(!requestOptions.extraHeaders) requestOptions.extraHeaders = re.extraHeaders || {}

        return Promise.resolve()
          .then(()=>{
              var servers = re.GetRawServers()
              if(!requestOptions.method) throw new MError("SERVICE_NOT_CONFIGURED_METHOD")
              if(!requestOptions.uri) throw new MError("SERVICE_NOT_CONFIGURED_URI")

              var ps = []
              var serversToUse = getServersToUse(requestOptions.method, servers)

              var mlc = requestOptions.method.toLowerCase()

              var serverIndex = 0
              serversToUse.some(server=>{

                 if(!requestOptions.extraHeaders[server.host])
                   requestOptions.extraHeaders[server.host] = {}

                 // we do this at the beginning, so it can be overridden later via SetupFieldsByRequest()
                 if(typeof requestOptions.extraHeaders[server.host]["MonsterServerIndex"] === "undefined")
                    requestOptions.extraHeaders[server.host]["MonsterServerIndex"] = serverIndex
                 serverIndex++

                 // TODO: review why forceRelayer feature was added in MonsterRelay (it is currently not in use, it seems)
                 var aServer = server
                 if(poolOptions.forceRelayer) aServer = ore.cloneServersAsRelayer(server)
                 if(poolOptions.forcePipe) aServer = ore.cloneServersAsPipe(server)

                 // we attach a simple mapiClient to the server, otherwise we'd have a new MonsterApiPublic instance every time meaning new login events every time a request is being made
                 var c;
                 if(!poolOptions.dontCache) {
                    if((!aServer.mapiClient)||(!aServer.mapiClient.get))
                      aServer.mapiClient = (poolOptions.clientLib || ore)(aServer, requestOptions.config || poolOptions.config);

                    c = aServer.mapiClient;
                 }
                 else
                    c = (poolOptions.clientLib || ore)(aServer, requestOptions.config || poolOptions.config);

                 c.extraHeaders = requestOptions.extraHeaders[aServer.host]

                 if(requestOptions.SetupFieldsByRequest)
                   c.SetupFieldsByRequest(requestOptions.SetupFieldsByRequest, requestOptions.forwardedForSecret)
                 else
                 if(gSetupFieldsByRequest)
                   c.SetupFieldsByRequest(gSetupFieldsByRequest, gForwardedForSecret)

                 if(requestOptions.SetXForwardedFor)
                   c.SetXForwardedFor(requestOptions.SetXForwardedFor)
                 else
                 if(gSetXForwardedFor)
                   c.SetXForwardedFor(gSetXForwardedFor)

                 if(requestOptions.SetUserAgent)
                   c.SetUserAgent(requestOptions.SetUserAgent)

        				 if(getMethods.indexOf(requestOptions.method) > -1)
        				   delete requestOptions.payload

                 ps.push(
                    c[mlc+"Async"](requestOptions.uri, requestOptions.payload)
                     .then(d=>{
                        suc.push(d)
                     })
                     .catch(x=>{
                        // this leaks the password.
                        // x.server = server
                        x.server = {}
                        Array("host","port","scheme","api_prefix","username").forEach(q=>{
                           x.server[q] = server[q]
                        })
                        rej.push(x)
                     })
                 )

                 if(requestOptions.firstServerOnly)
                   return true
              })

              return Promise.all(ps)
          })
          .then(()=>{
              if(((poolOptions.throwIfAnyFailed)&&(rej.length))||(!suc.length))
                throw rej[0]

              var re = suc[0]
              re.poolErrors = rej || []
              return Promise.resolve(re)
          })


     }


      getMethods.forEach(method=> {
        var mlc = method.toLowerCase()

        re[mlc] = function(uri, options, callback) {
            return re.request(prepareOptions(options, method, uri), callback)
        }
        re[mlc+"Async"] = function(uri, options) {
            return re.requestAsync(prepareOptions(options, method, uri))
        }
      })

      Array("POST","PUT","DELETE").forEach(method=>{
        var mlc = method.toLowerCase()
        re[mlc] = function(uri, payload, options, callback) {
            return re.request(prepareOptions(options, method, uri, payload), callback)
        }
        re[mlc+"Async"] = function(uri, payload, options) {
            return re.requestAsync(prepareOptions(options, method, uri, payload))
        }
      })

     re.getApiType = function(){
         var servers = re.GetRawServers()
         var lib = ore.getApiModuleLib(servers[0])
         if(!lib) throw new MError("INVALID_SERVER_DATA")
         return lib.getApiType()
     }

     return re

     function prepareOptions(options, method, uri, payload) {
            if(!options) options = {}
            options.uri = uri
            options.method = method
            options.payload = payload
            return options
     }

     function getServersToUse(method, servers) {
        if(getMethods.indexOf(method) <= -1)
           return servers // post methods involve all servers

        // proceeding with get

        if(poolOptions.serverSelectionAlgo == "first") {
           for(var q of servers)
             return [q]
        }


        throw new MError("INVALID_ALGO")
     }


  }

ore.cloneServersAsRelayer = function(serverOptions)
{
   var newServer = simpleCloneObject(serverOptions)
   delete newServer.api_secret
   delete newServer.username
   delete newServer.password
   newServer.auth_token = "-"
   return newServer
}

ore.cloneServersAsPipe = function(serverOptions)
{
   var newServer = simpleCloneObject(serverOptions)
   delete newServer.api_secret
   delete newServer.username
   delete newServer.password
   delete newServer.auth_token
   newServer.pipe = true
   return newServer
}

ore.getApiModuleLib = function(serverOptions) {
    if(!serverOptions) return

    if(serverOptions.pipe)
      return require("MonsterApiPipe")

    if(serverOptions.api_secret)
      return require("MonsterApiInternal")

    if(serverOptions.username)
      return require("MonsterApiPublic")

    if(serverOptions.auth_token)
       return require("MonsterApiRelay")
}

module.exports = ore

})()
