set -e

INSTANCE_BASENAME=$(echo $(basename $0) | cut -d- -f 1)
[ -z "$INSTANCE_NAME" ] && INSTANCE_NAME="$INSTANCE_BASENAME"

if [ -z "$INSTANCE_NAME" ]; then
  echo "couldnt determine the name of the caller"
  exit 1
fi

if [ ! -z "$INSTALL_DEPS" ]; then
  cd $(dirname $0)/..
  npm install --production
fi


function mc_start {
   cd $(dirname $0)/..
   export NODE_ENV=production
   exec node "$INSTANCE_BASENAME.js"
}  
