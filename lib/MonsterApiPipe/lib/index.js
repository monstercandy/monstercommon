
var MError = require("MonsterError")

const util = require("util")

var lib = module.exports = function(serviceOptions, config) {
  var host, port, scheme, timeout, maxSockets

  if(!config) config = {}

  if(typeof(serviceOptions) !== "object")
      throw new MError("INVALID_PARAMETER")

  host = serviceOptions["ip"] || serviceOptions["host"]
  scheme = serviceOptions["scheme"] || "http"
  port = serviceOptions["port"]
  timeout = serviceOptions["timeout"] || config["timeout"] || (config.get ? config.get("timeout") : null)

/*
  maxSockets = Infinity
  if((config.get)&&(config.get("maxSockets")))
    maxSockets =config.get("maxSockets")
*/

  function getFullUrl(uri){
      var x =`${scheme}://${host}`
      if(port)
          x += `:${port}`
      x += uri
      return x
  }


  var re = {}

  re.getApiType = lib.getApiType


  re.requestLib = require("RequestCommon")


  re.doRequest = function(method, uri) {

      var fullUrl = getFullUrl(uri)

      var params = {
          method: method,
          url: fullUrl,
          // we dont do this anymore, as https globalAgent CA would be discarded then by request lib
          // pool: {maxSockets: maxSockets},
          headers: {},
      }
      if(timeout) { // msecs issue
          params.timeout = timeout;
      }

      var extraHeaders = re.extraHeaders||serviceOption.extraHeaders;

      if(extraHeaders) {
         Object.keys(extraHeaders).forEach((k)=>{
            params.headers[k] = extraHeaders[k]
         })
      }

      var req = re.requestLib(params)

      return req
  }


  return re
}

lib.getApiType = function() {
    return "pipe"
}
