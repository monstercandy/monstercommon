const util = require('util');
const path = require('path')
const crypto = require('crypto');

const fs = require("fs")

var nconf = require('nconf');

var args = process.argv.slice(2);
var configFilename = getConfigfilename()


if(!fs.existsSync(configFilename)) {
  console.log("config file ("+configFilename+") cannot be found")
  process.reallyExit(-1);
}

var config =
   nconf.argv()
   .env()
   .file({ file:  configFilename});

if(!config.originalDefaults) {
  var aggregatedDefaults = {}

  config.originalDefaults = config.defaults
  config.defaults = function(additionalDefaults) {

    if(!additionalDefaults)
      return aggregatedDefaults;

    for(q of Object.keys(additionalDefaults)){
//       if(!aggregatedDefaults[q])
          aggregatedDefaults[q] = additionalDefaults[q]
    }

    config.originalDefaults(aggregatedDefaults)
  }
}

config.defaults({
      "request_validity_sec" : 60,
      "listen_port": 8080,
      "max_request_body_size": 10 * 1024,
      "max_request_body_size_import_export": 10*1024*1024
  })

config.args = args

var vol_path = config.get("volume_directory")
if(!vol_path) {
  console.log("volume_directory is not specified in the configuration file")
  process.reallyExit(-1);
}
var tapifn = process.env.VOLUME_FILE
if(!tapifn) {
  tapifn = path.join( vol_path, getProcessName() + ".json" );
  if(!path.isAbsolute(vol_path))
    tapifn = filePathInCurrentDir( tapifn );
}

console.log("Trying to access volume config at:", tapifn)

var tapiconf;
try{
   tapiconf = JSON.parse(fs.readFileSync(tapifn))

}
catch(ex){
	console.log("Volume config not found: " + ex)
	tapiconf = {};
}

config.getConfigfilename = function(){
  return configFilename;
}

config.getVolumePath = function(){
   return vol_path;
}

config.saveVolume= function(newVolume) {
    const cleanVolume = require("./cleaner.js")
    fs.writeFileSync(tapifn, JSON.stringify(cleanVolume(newVolume), null, 4), {"mode":0600})
    var uid = config.get("setuid")
    var gid = config.get("setgid")
    if((uid)||(gid))
      fs.chownSync(tapifn, uid||0, gid||0)

    config.set("volume", newVolume)

    util.log("New volume config saved as "+tapifn);

}

config.getApiSecret = function () {
    if(!config.get("volume")) return
    return config.get("volume")["api_secret"]

}

config.getHmac = function (secret) {
  if(!secret) {
    secret = config.getApiSecret();
    if(!secret) return;
  }
  return config.getHmacForApiSecret(secret);
}

config.getHmacForApiSecret = function (secret) {
  return crypto.createHmac('sha256', secret);
}

config.filePathInCurrentDir = filePathInCurrentDir

config.writePid = function(){
    fs.writeFile(tapifn+".pid", process.pid, function(err){})

}


if(!tapiconf["api_secret"]) {

    console.log("Generating new API secret");
    var buffer = require('crypto').randomBytes(16)

    tapiconf["api_secret"] = buffer.toString('hex');
    config.saveVolume(tapiconf, (err)=>{
          if (err) throw err;

    })


} else {
   config.set("volume", tapiconf);
}

if(config.get("setgroups")) {
   process.setgroups(config.get("setgroups"))
}

if(config.get("setgid")) {
   process.setgid(config.get("setgid"))
   if(process.getgid() != config.get("setgid"))
      throw new Error("setgid failed!")
}

if(config.get("setuid")) {
   process.setuid(config.get("setuid"))
   if(process.getuid() != config.get("setuid"))
      throw new Error("setuid failed!")
}

module.exports = config;


function getProcessName() {
  var procname = path.basename(process.argv[1], ".js");
  if(procname == "mocha") procname = "_mocha";
  return procname;
}



function filePathInCurrentDir (fn) {
  return path.join(process.cwd(), fn)
}

function getConfigfilename(){
  var configFilename = process.env.MC_CONFIG_FILE || process.env.CONFIG_FILE
  var procName = getProcessName()

  if((process.env.NODE_ENV == "production") && (!configFilename)) {
    configFilename = '/etc/monster/'+procName+'.json'
    if(!fs.existsSync(configFilename))
       configFilename = filePathInCurrentDir("../etc/"+procName+'.json')
  }

  if((!configFilename)||((procName == "_mocha")&&(!process.env.CONFIG_FILE)))
     configFilename = filePathInCurrentDir(procName+'-config-dev.json')

  return configFilename
}

