module.exports = function(ad) {
  var d = JSON.parse(JSON.stringify(ad))

  cleanObject(d)

  return d

  function cleanObject(node) {
  	  Object.keys(node).forEach(x=>{
  	  	  if(x == "mapiClient") {
  	  	  	  delete node[x]
  	  	  }
  	  	  else if(Array.isArray(node[x]))
  	  	     cleanArray(node[x])
  	  })
  }

  function cleanArray(arr){
  	 arr.forEach(x=>{
  	 	if(typeof x === "object")
  	 		cleanObject(x)
  	 })
  }
}
