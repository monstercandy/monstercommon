require( "../../MonsterDependencies")(__dirname)


describe('basic', function() {
    it('multiple defaults call', function() {

            var config = require('MonsterConfig');

            var t = {"call2": "default2", "from_config_file": "value_config", "call1":"default1", }

            for (var q of ["call1","call2"]) {
                var x = {}
                x[q] = t[q]
                config.defaults(x)
            }

            for (var q of Object.keys(t)){
                var v = config.get(q)
                if(v != t[q])
                   throw new Error("Error: "+q+" should be "+t[q]+", but it is "+v)
                // console.log("XXXXXXXXXXXXX", q)
            }

    })

    it('should be able to override defaults', function() {

            var config = require('MonsterConfig');

            var t = {"max_request_body": 1 }

            config.defaults(t)

            var v = config.get("max_request_body")
            if(v != t.max_request_body)
               throw new Error("Error: max_request_body should be 1")

            var x = {"a":1};
            config.defaults(x)

            x = {"a":2};
            config.defaults(x)

            v = config.get("a")
            if(v != x.a)
               throw new Error("Error: a should be 2")

    })


})

describe('should drop MapiClient nodes', function() {
    it('cleaning', function() {

            var vol = {
"api_secret":"x1",
"mailer-servers":[{"host":"127.0.0.1","port":10005,"scheme":"http","api_secret":"x2"}],
"info-webhosting-servers":[{"host":"127.0.0.1","port":"10006","scheme":"http","api_prefix":"/api/s/s1","username":"u2","password":"p2","mapiClient":{"extraHeaders":{"MonsterServerIndex":0}}}],
"info-accountapi-servers":[{"host":"127.0.0.1","port":"10006","scheme":"http","api_prefix":"/api","username":"u3","password":"p3"}],
"eventapi-servers":[{"host":"127.0.0.1","port":10009,"scheme":"http","api_secret":"x3"}],
"fileman-servers":[{"host":"127.0.0.1","port":10014,"scheme":"http","api_secret":"x"}],
"relayer-servers":[{"host":"127.0.0.1","port":"10006","scheme":"http","api_prefix":"/api/s/s1","username":"u1","password":"p1"}]}


            var cleaner = require("../lib/cleaner.js")

            var cleaned = cleaner(vol)

            // console.dir(cleaned)

            const assert = require("chai").assert

            assert.deepEqual(cleaned, { api_secret: 'x1',
  'mailer-servers':
   [ { host: '127.0.0.1',
       port: 10005,
       scheme: 'http',
       api_secret: 'x2' } ],
  'info-webhosting-servers':
   [ { host: '127.0.0.1',
       port: '10006',
       scheme: 'http',
       api_prefix: '/api/s/s1',
       username: 'u2',
       password: 'p2' } ],
  'info-accountapi-servers':
   [ { host: '127.0.0.1',
       port: '10006',
       scheme: 'http',
       api_prefix: '/api',
       username: 'u3',
       password: 'p3' } ],
  'eventapi-servers':
   [ { host: '127.0.0.1',
       port: 10009,
       scheme: 'http',
       api_secret: 'x3' } ],
  'fileman-servers':
   [ { host: '127.0.0.1',
       port: 10014,
       scheme: 'http',
       api_secret: 'x' } ],
  'relayer-servers':
   [ { host: '127.0.0.1',
       port: '10006',
       scheme: 'http',
       api_prefix: '/api/s/s1',
       username: 'u1',
       password: 'p1' } ] })


    })


})
