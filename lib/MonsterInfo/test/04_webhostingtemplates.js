require( "../../MonsterDependencies")(__dirname)

var assert = require("chai").assert

describe('webhosting template info', function() {    


    it('should call the correct uri in public mode', function(done) {

        const template_name = "WEB10000"
    	const final_result = {"some":"result"}

	    var info = require( "../lib/index.js").webhostingtemplates({getAccountMapiObject:{GetAccountMapi: function(){
	    	// this is the mocked Mapi Pool object here

	    	return {
	    		getAsync: function(uri) {
	    			assert.equal(uri, "/info/account/webhostingtemplate/"+template_name)
	    			return Promise.resolve({result:final_result})
	    		}
	    	}

	    }}})

	    info.GetInfo(template_name)
	      .then(d=>{
	      	  assert.deepEqual(d, final_result)
	      	  done()
	      })
	      .catch(done)

    })

    it('should call the correct uri in internal mode', function(done) {

        const template_name = "WEB10000"
    	const final_result = {"some":"result"}

	    var info = require( "../lib/index.js").webhostingtemplates({
            config: {
            	get: function(key){
            		if(key == "info_accountapi_mode")
              		  return "internal"
            	}
            },
	    	getAccountMapiObject:{GetAccountMapi: function(){
	    	// this is the mocked Mapi Pool object here

	    	return {
	    		getAsync: function(uri) {
	    			assert.equal(uri, "/accountapi/webhostingtemplate/"+template_name)
	    			return Promise.resolve({result:final_result})
	    		}
	    	}

	    }}})

	    info.GetInfo(template_name)
	      .then(d=>{
	      	  assert.deepEqual(d, final_result)
	      	  done()
	      })
	      .catch(done)

    })

})
