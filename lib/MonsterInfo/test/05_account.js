require( "../../MonsterDependencies")(__dirname)

var assert = require("chai").assert

describe('account info', function() {    


    it('should call the correct uri in public mode', function(done) {

        const user_id = "12345"
    	const final_result = {"some":"result"}

	    var info = require( "../lib/index.js").account({getAccountMapiObject:{GetAccountMapi: function(){
	    	// this is the mocked Mapi Pool object here

	    	return {
	    		getAsync: function(uri) {
	    			assert.equal(uri, "/info/account/account/"+user_id)
	    			return Promise.resolve({result:final_result})
	    		}
	    	}

	    }}})

	    info.GetInfo(user_id)
	      .then(d=>{
	      	  assert.deepEqual(d, final_result)
	      	  done()
	      })
	      .catch(done)

    })

    it('should call the correct uri in internal mode as well', function(done) {

        const user_id = "12345"
    	const final_result = {"some":"result"}

	    var info = require( "../lib/index.js").account({
            config: {
            	get: function(key){
            		if(key == "info_accountapi_mode")
              		  return "internal"
            	}
            },
	    	getAccountMapiObject:{GetAccountMapi: function(){
	    	// this is the mocked Mapi Pool object here

	    	return {
	    		getAsync: function(uri) {
	    			assert.equal(uri, "/accountapi/account/"+user_id)
	    			return Promise.resolve({result:final_result})
	    		}
	    	}

	    }}})

	    info.GetInfo(user_id)
	      .then(d=>{
	      	  assert.deepEqual(d, final_result)
	      	  done()
	      })
	      .catch(done)

    })
})
