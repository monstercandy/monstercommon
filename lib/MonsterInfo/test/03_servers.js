require( "../../MonsterDependencies")(__dirname)

var assert = require("chai").assert

describe('servers info', function() {    


    it('should call the correct uri in public mode', function(done) {

    	const final_result = {"some":"result"}

	    var info = require( "../lib/index.js").servers({getAccountMapiObject:{GetAccountMapi: function(){
	    	// this is the mocked Mapi Pool object here

	    	return {
	    		getAsync: function(uri) {
	    			assert.equal(uri, "/info/account/servers")
	    			return Promise.resolve({result:final_result})
	    		}
	    	}

	    }}})

	    info.GetInfo()
	      .then(d=>{
	      	  assert.deepEqual(d, final_result)
	      	  done()
	      })
	      .catch(done)

    })

    it('should call the correct uri in internal mode', function(done) {

    	const final_result = {"some":"result"}

	    var info = require( "../lib/index.js").servers({
            config: {
            	get: function(key){
            		if(key == "info_accountapi_mode")
              		  return "internal"
            	}
            },
	    	getAccountMapiObject:{GetAccountMapi: function(){
	    	// this is the mocked Mapi Pool object here

	    	return {
	    		getAsync: function(uri) {
	    			assert.equal(uri, "/accountapi/servers")
	    			return Promise.resolve({result:final_result})
	    		}
	    	}

	    }}})

	    info.GetInfo()
	      .then(d=>{
	      	  assert.deepEqual(d, final_result)
	      	  done()
	      })
	      .catch(done)

    })
})
