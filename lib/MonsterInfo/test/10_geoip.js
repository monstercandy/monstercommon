require( "../../MonsterDependencies")(__dirname)

var assert = require("chai").assert

describe('geoip info', function() {    

	var infoLib = require( "../lib/index.js")


    it('should call the correct uri with a simple ip', function(done) {

    	const final_result = {"country":"HU"}
    	const ip = "123.123.123.123"

	    var info = infoLib.geoip({getGeoipObject:{GetGeoip: function(){
	    	// this is the mocked Mapi Pool object here

	    	return {
	    		postAsync: function(uri, data) {
	    			assert.equal(uri, "/geoip/lookup")
	    			assert.deepEqual(data, {ip:ip})
	    			return Promise.resolve({result:final_result})
	    		}
	    	}

	    }}})

	    info.GetInfo(ip)
	      .then(d=>{
	      	  assert.deepEqual(d, final_result)
	      	  done()
	      })
	      .catch(done)

    })


    it('local ip should be accepted and dash should be returned', function(done) {

    	const final_result = {"country":"-"}
    	const ip = "::ffff:127.0.0.1"

	    var info = infoLib.geoip({getGeoipObject:{GetGeoip: function(){
	    	// this is the mocked Mapi Pool object here

	    	return {
	    		postAsync: function(uri, data) {
	    			assert.equal(uri, "/geoip/lookup")
	    			assert.deepEqual(data, {ip:ip})
	    			return Promise.resolve({result:final_result})
	    		}
	    	}

	    }}})

	    info.GetInfo(ip)
	      .then(d=>{
	      	  assert.deepEqual(d, final_result)
	      	  done()
	      })
	      .catch(done)

    })

})
