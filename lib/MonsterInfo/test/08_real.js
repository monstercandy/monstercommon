require( "../../MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
process.env.CONFIG_FILE = "./test/volume.conf"
process.env.VOLUME_FILE = "./test/volume.json"
var app = require( "volume.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

  describe("real test", function(){


        it('MonsterInfo should detect info-accountapi-servers to be internal', function(done) {

                var info = require( "../lib/index.js").servers({config:app.config, accountServerRouter: app.ServersInfoAccountapi})
                assert.ok(info.IsInternal())
                done()

        })


 })

})

