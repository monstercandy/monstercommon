require( "../../MonsterDependencies")(__dirname)

var assert = require("chai").assert

describe('webhosting info', function() {


    it('should call the correct uri with a simple webhosting id', function(done) {

    	const final_result = {"some":"result"}
    	const webhosting_id = "12345"

	    var info = require( "../lib/index.js").webhosting({getWebhostingObject:{GetWebhosting: function(){
	    	// this is the mocked Mapi Pool object here

	    	return {
	    		getApiPrefix: function(){
	    			return "/api";
	    		},
	    		getAsync: function(uri) {
	    			assert.equal(uri, "/s/[this]/info/webhosting/"+webhosting_id)
	    			return Promise.resolve({result:final_result})
	    		}
	    	}

	    }}})

	    info.GetInfo(webhosting_id)
	      .then(d=>{
	      	  assert.deepEqual(d, final_result)
	      	  done()
	      })
	      .catch(done)

    })


    it('the server specification should be missing when the client already has an api prefix configured', function(done) {

    	const final_result = {"some":"result"}
    	const webhosting_id = "12345"

	    var info = require( "../lib/index.js").webhosting({getWebhostingObject:{GetWebhosting: function(){
	    	// this is the mocked Mapi Pool object here

	    	return {
	    		getApiPrefix: function(){
	    			return "/s/s1";
	    		},
	    		getAsync: function(uri) {
	    			assert.equal(uri, "/info/webhosting/"+webhosting_id)
	    			return Promise.resolve({result:final_result})
	    		}
	    	}

	    }}})

	    info.GetInfo(webhosting_id)
	      .then(d=>{
	      	  assert.deepEqual(d, final_result)
	      	  done()
	      })
	      .catch(done)

    })


    it('should call the correct uri with a webhosting server+id object', function(done) {

    	const final_result = {"some":"result"}
    	const webhosting = {storage_id:"12345", server: "foobar"}

	    var info = require( "../lib/index.js").webhosting({getWebhostingObject:{GetWebhosting: function(){
	    	// this is the mocked Mapi Pool object here

	    	return {
	    		getApiPrefix: function(){
	    			return "/api";
	    		},
	    		getAsync: function(uri) {
	    			assert.equal(uri, `/s/${webhosting.server}/info/webhosting/${webhosting.storage_id}`)
	    			return Promise.resolve({result:final_result})
	    		}
	    	}

	    }}})

	    info.GetInfo(webhosting)
	      .then(d=>{
	      	  assert.deepEqual(d, final_result)
	      	  done()
	      })
	      .catch(done)

    })

})
