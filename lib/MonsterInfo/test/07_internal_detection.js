require( "../../MonsterDependencies")(__dirname)

var assert = require("chai").assert

describe('internal detection', function() {



    it('should detect internal pools', function(done) {


	    var router = {
	    	GetVolumeName: function(){return "internal"},
	    	GetRawServers: function(){
	    	 return [{"api_secret":"1234"}]
	    }}

	    var info = require( "../lib/index.js").servers({accountServerRouter: router})
	    assert.ok(info.IsInternal())
	    done()
    })

    it('should detect non internal pools', function(done) {


	    var router = {
	    	GetVolumeName: function(){return "public"},
	    	GetRawServers: function(){
	    	 return [{"username":"1234","password":"sdf"}]
	    }}

	    var info = require( "../lib/index.js").servers({accountServerRouter: router})
	    assert.notOk(info.IsInternal())
	    done()
    })
})
