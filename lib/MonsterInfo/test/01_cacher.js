require( "../../MonsterDependencies")(__dirname)

var assert = require("chai").assert


describe('cacher library, file backend', function() {    


    const os = require("os")
    const path = require("path")

    const input_data = {"a":"b"}
    const validated_data = {"foo": "bar"}
    const final_lookup_result = {"looked":"up"}

    Array(true,false).forEach(fetchShouldHaveCalled=>{
    	  // second time FetchInfo should not be called

		    it('GetInfo: '+fetchShouldHaveCalled, function() {

		        var fetchWasCalled = false
			    var info = require( "../lib/lib-cacher.js")({
			    	Validate: function(in_data){
			    	   assert.deepEqual(in_data, input_data)
		               return Promise.resolve(validated_data)
			    	},
			    	FetchInfo: function(d){
			    		fetchWasCalled = true
			    		assert.deepEqual(d, validated_data)
			    		return Promise.resolve(final_lookup_result)
			    	},
			    	GetCacheFile: function(d){
			    		assert.deepEqual(d, validated_data)    		
			    		return path.join(os.tmpdir(), "monsterinfo-mocha.txt")
			    	},
			    })

                var p= fetchShouldHaveCalled ? info.Delete(input_data) : Promise.resolve()
                return p.then(()=>{
			      	 return info.GetInfo(input_data)
			      })
			      .then(r=>{
			      	  assert.deepEqual(r, final_lookup_result)

			      	  assert.equal(fetchWasCalled, fetchShouldHaveCalled)

			      	  if(!fetchShouldHaveCalled) info.Delete(input_data) // just some cleanup

			      })

		    })

    })

		    it('multiple simultanous calls should call the backend logic only once', function() {

                var max_simult = 5
                var ready = 0
		        var fetchWasCalled = 0
			    var info = require( "../lib/lib-cacher.js")({
			    	Validate: function(in_data){
			    	   assert.deepEqual(in_data, input_data)
		               return Promise.resolve(validated_data)
			    	},
			    	FetchInfo: function(d){
			    		fetchWasCalled++
			    		assert.deepEqual(d, validated_data)
			    		return new Promise(function(resolve,reject){
			    			setTimeout(function(){ resolve(final_lookup_result); }, 1000)
			    	    })
			    	},
			    	GetCacheFile: function(d){
			    		assert.deepEqual(d, validated_data)    		
			    		return path.join(os.tmpdir(), "monsterinfo-mocha.txt")
			    	},
			    })

                return info.Delete(input_data)
                  .catch(ex=>{})
                  .then(()=>{
                  	  var ps = []
                  	  for(var i= 0; i < max_simult; i++)
			      	     ps.push(info.GetInfo(input_data)
			      	     	.then(r=>{
			      	           assert.deepEqual(r, final_lookup_result)			      	     		
			      	           ready++
			      	     	}))

			      	 return Promise.all(ps)
                  })
			      .then(r=>{
			      	  assert.equal(ready, max_simult)

			      	  assert.equal(fetchWasCalled, 1)

			      })

		    })


})

describe('rejected lookups should not be cached', function() {   


    const os = require("os")
    const path = require("path")

    const validated_data = {"foo": "bar"}
    const input_data = {"a":"b"}
    const final_lookup_result = {"looked":"up"}

    const rejection = "not this time"


		    it('GetInfo rejection', function() {

		        var fetchWasCalled = 0
			    var info = require( "../lib/lib-cacher.js")({
			    	Validate: function(in_data){
			    	   assert.deepEqual(in_data, input_data)
		               return Promise.resolve(validated_data)
			    	},
			    	FetchInfo: function(d){
			    		fetchWasCalled++
			    		assert.deepEqual(d, validated_data)
			    		return Promise.reject(new Error(rejection))
			    	},
			    	GetCacheFile: function(d){
			    		assert.deepEqual(d, validated_data)    		
			    		return path.join(os.tmpdir(), "monsterinfo-rejected.txt")
			    	},
			    })

                info.Delete(input_data)
                  .then(()=>{
			      	 return info.GetInfo(input_data)
			      })
                  .catch(ex=>{

                  	 assert.equal(ex.message, rejection)
                  	 assert.equal(fetchWasCalled, 1)

                  	 return info.GetInfo(input_data)


                  })
                  .catch(ex=>{

                  	 assert.equal(ex.message, rejection)
                  	 assert.equal(fetchWasCalled, 2) // should have been called agian!

			      })

		    })


})


describe('cacher library, memory backend', function() {    


    const path = require("path")

    const validated_data = {"foo": "bar"}
    const input_data = {"a":"b"}
    const final_lookup_result = {"looked":"up"}

    	  // second time FetchInfo should not be called

	    it('GetInfo first Fetches, second it does not, then after timeout it does again', function() {

	    	this.timeout(5000)

	        var fetchWasCalled = 0
		    var info = require( "../lib/lib-cacher.js")({
		    	cleanupIntervalMs: 500,
		    	cacheInfoSeconds: 2,
		    	Validate: function(in_data){
		    	   assert.deepEqual(in_data, input_data)
	               return Promise.resolve(validated_data)
		    	},
		    	FetchInfo: function(d){
		    		fetchWasCalled++
		    		assert.deepEqual(d, validated_data)
		    		return Promise.resolve(final_lookup_result)
		    	},
		    	GetCacheFile: function(d){
		    		assert.deepEqual(d, validated_data)    		
		    		return "memory/monsterinfo/mocha.txt" // note: we return multiple slashes here
		    	},
		    })

	        return info.GetInfo(input_data)
		      .then(r=>{
		      	  assert.deepEqual(r, final_lookup_result)

		      	  assert.equal(fetchWasCalled, 1)

		      	  return info.GetInfo(input_data)
		      })
		      .then(r=>{
		      	  assert.deepEqual(r, final_lookup_result)
		      	  assert.equal(fetchWasCalled, 1) // no more calls

                  return new Promise(function(resolve,reject){
                  	 setTimeout(resolve, 3000)
                  })
		      })
		      .then(()=>{
		      	  return info.GetInfo(input_data)		      	
		      })
		      .then(r=>{
		      	  assert.deepEqual(r, final_lookup_result)

		      	  assert.equal(fetchWasCalled, 2) // and should have called again

		      })		      

	    })

})
