module.exports = function(moduleOptions) {
    moduleOptions = moduleOptions || {}
    moduleOptions.subsystem = "mocha"

    const path = require('path')
    var fs = require("MonsterDotq").fs();

    var config = require('MonsterConfig');
    var me = require('MonsterExpress');
    const MError = me.Error

    config.appRestPrefix = "/git"
    config.tokenapi = true

    var app = me.Express(config, moduleOptions);
    app.enable("trust proxy")  // this application receives x-forwarded-for headers from other internal systems (typically relayer)

    var router = app.PromiseRouter(config.appRestPrefix)

    app.ServersInfoAccountapi = router.ServersMiddleware("info-accountapi", 0)

    return app

}
