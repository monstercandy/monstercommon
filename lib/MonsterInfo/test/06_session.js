require( "../../MonsterDependencies")(__dirname)

var assert = require("chai").assert

describe('session/token info', function() {    


    it('should call the correct uri in public mode', function(done) {

        const token = "12345"
    	const final_result = {"some":"result"}

	    var info = require( "../lib/index.js").session({getAccountMapiObject:{GetAccountMapi: function(){
	    	// this is the mocked Mapi Pool object here

	    	return {
	    		postAsync: function(uri, data) {
	    			assert.equal(uri, "/info/account/token")
	    			assert.deepEqual(data, {token: token})
	    			return Promise.resolve({result:final_result})
	    		}
	    	}

	    }}})

	    info.GetInfo(token)
	      .then(d=>{
	      	  assert.deepEqual(d, final_result)
	      	  done()
	      })
	      .catch(done)

    })

    it('should call the correct uri in internal mode', function(done) {

        const token = "12345"
    	const final_result = {"some":"result"}

	    var info = require( "../lib/index.js").session({
            config: {
            	get: function(key){
            		if(key == "info_accountapi_mode")
              		  return "internal"
            	}
            },
	    	getAccountMapiObject:{GetAccountMapi: function(){
	    	// this is the mocked Mapi Pool object here

	    	return {
	    		postAsync: function(uri, data) {
	    			assert.equal(uri, "/accountapi/authentication/token")
	    			assert.deepEqual(data, {token: token})
	    			return Promise.resolve({result:final_result})
	    		}
	    	}

	    }}})

	    info.GetInfo(token)
	      .then(d=>{
	      	  assert.deepEqual(d, final_result)
	      	  done()
	      })
	      .catch(done)

    })


   Array("x", "y", "foobar").forEach(c=>{
    it('GetCacheFile should be correct: '+c, function(done) {

        const token = "12345"
    	const final_result = {"some":"result"}

	    var info = require( "../lib/index.js").session({})
	    assert.equal("memory/"+c, info.GetCacheFile(c))
	    done()
    })

   })


    it('Delete should be available and working', function(done) {

	    var info = require( "../lib/index.js").session({})

	    info.Delete("something")
	      .then(()=>{
	      	  done()
	      })
	      .catch(done)

    })




    it('server name should be sent along if it is present', function(done) {

        const token = "12345"
    	const final_result = {"some":"result"}
    	var server_name = "superserver"

	    var info = require( "../lib/index.js").session({
            config: {
            	get: function(key){
            		if(key == "server_name")
              		  return server_name
            	}
            },
	    	getAccountMapiObject:{GetAccountMapi: function(){
	    	// this is the mocked Mapi Pool object here

	    	return {
	    		postAsync: function(uri, data) {
	    			assert.deepEqual(data, {token: token, server: server_name})
	    			return Promise.resolve({result:final_result})
	    		}
	    	}

	    }}})

	    info.GetInfo(token)
	      .then(d=>{
	      	  assert.deepEqual(d, final_result)
	      	  done()
	      })
	      .catch(done)

    })


})
