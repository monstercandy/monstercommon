require( "../../MonsterDependencies")(__dirname)

var assert = require("chai").assert

describe('email info', function() {    


    it('should call the correct uri in public mode', function(done) {

        const user_id = "12345"
    	const final_result = {"some":"result"}

	    var info = require( "../lib/index.js").email({getAccountMapiObject:{GetAccountMapi: function(){
	    	// this is the mocked Mapi Pool object here

	    	return {
	    		postAsync: function(uri, data) {
	    			// console.log("FOIOO", uri, data)
	    			assert.deepEqual(data, {emailCategory: "TECH"})
	    			assert.equal(uri, "/info/account/account/"+user_id)
	    			return Promise.resolve({result:final_result})
	    		}
	    	}

	    }}})

	    info.GetInfo({user_id:user_id, emailCategory: "TECH"})
	      .then(d=>{
	      	  assert.deepEqual(d, final_result)
	      	  done()
	      })
	      .catch(done)

    })

})
