var lib = module.exports = function(moduleOptions) {

    if(!moduleOptions) moduleOptions = {}


    var mapiPool // we keep a global instance of mapipool here, else it would attempt to login every freaking time some info lookup is required

  const path = require("path")

    if((moduleOptions.config)&&(moduleOptions.config.defaults)) {
        moduleOptions.config.defaults({geoip_info_directory:"memory"})
    }

  var hostingDir
  if(moduleOptions.config)
      hostingDir = moduleOptions.config.get("geoip_info_directory")

    var vali = require("MonsterValidators").ValidateJs()

    const cacher = require("./lib-cacher.js")

    var re

    var cacherInput = {
        Validate: Validate,
        FetchInfo: function(data) {
           var geoIpLib = re.GetGeoip();

           var url = "/geoip/lookup";
           var apiPrefix;
           if(geoIpLib.getApiPrefix)
             apiPrefix = geoIpLib.getApiPrefix();
           if(apiPrefix == "/api") {
               var s = data.server || "[this]";
               url = `/s/${s}${url}`
           }

           if((!data.ip)||(data.ip.match(/127.0.0.1/)))
              return Promise.resolve({"country":"-"});
           return geoIpLib.postAsync(url, data)
        },
        GetCacheFile: function(data) {
           if(!hostingDir) return

           return path.join(hostingDir, data.ip+".json")
        }

    }

   if(moduleOptions.config) {
      cacherInput.cacheInfoSeconds = moduleOptions.config.get("cache_geoip_info_seconds")
      cacherInput.cleanupIntervalMs = moduleOptions.config.get("cleanup_geoip_info_ms")
   }

    re= cacher(cacherInput)

    re.Validate = Validate

    re.GetGeoip = function(){
        if(moduleOptions.getGeoipObject) {
          if(!moduleOptions.getGeoipObject.GetGeoip)
            throw new Error("GetGeoip function not found")
          return moduleOptions.getGeoipObject.GetGeoip()
        }

        if(!mapiPool) {
           const Mapi = require("MonsterClient")
           mapiPool = Mapi.getMapiPoolCached(moduleOptions.geoipServerRouter)
        }

        return mapiPool
  }


    return re


    function Validate(ip) {
         return vali.async({ip:ip}, {ip: {isString: true}})
    }

}
