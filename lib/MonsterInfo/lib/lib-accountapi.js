(function(){

var vali = require("MonsterValidators").ValidateJs()

var lib = module.exports = {}
const path = require("path");

(function(name){
 lib[name] = function(moduleOptions) {
    var cacherInput = {
              Validate: function(in_data){
                  const validators = {
                           "user_id": {presence: true, isString: {lazy:true}},
                        }
                  return vali.async({user_id: in_data}, validators)
              },
              FetchInfo: function(data) {
                 return cacherInput.GetAccountMapi().getAsync(cacherInput.urlPrefix() + "/account/"+data.user_id)
              },
              GetCacheFileInner: function(cacheDir, data) {
                 return path.join(cacheDir, data.user_id+".json")

              }
          }
      return common(name, cacherInput, moduleOptions)
   }
})("account");


(function(name){
 lib[name] = function(moduleOptions) {
    var cacherInput = {
              Validate: function(in_data){
                  const validators = {
                           "user_id": {presence: true, isString: {lazy:true,strictName:true}},
                           "emailCategory": {presence: true, isString: {lazy:true,strictName:true}},
                        }
                  return vali.async(in_data, validators)
              },
              FetchInfo: function(data) {
                 return cacherInput.GetAccountMapi().postAsync(cacherInput.urlPrefix() + "/account/"+data.user_id, {emailCategory: data.emailCategory})
              },
              GetCacheFileInner: function(cacheDir, data) {
                 return path.join(cacheDir, data.user_id+data.emailCategory+".json")

              }
          }
      return common(name, cacherInput, moduleOptions)
   }
})("email");

(function(name){
lib[name] = function(moduleOptions) {
   var cacherInput = {
              Validate: function(in_data){
                  return Promise.resolve(in_data)
              },
              FetchInfo: function(data) {

                 return cacherInput.GetAccountMapi().getAsync(
                    data ?
                    cacherInput.urlPrefix()+"/server/"+data :
                    cacherInput.urlPrefix()+"/servers"
                 )
              },
              GetCacheFileInner: function(cacheDir, data) {
                 return path.join(cacheDir, "servers.json")
              }
          }
     return  common(name, cacherInput, moduleOptions)
   }
})("servers");

(function(name) {
lib[name] = function(moduleOptions) {
   var cacherInput = {
              Validate: function(in_data){
                  return Promise.resolve(in_data)
              },
              FetchInfo: function(data) {

                 return cacherInput.GetAccountMapi().getAsync(data ? cacherInput.urlPrefix()+"/webhostingtemplate/"+data : cacherInput.urlPrefix()+"/webhostingtemplates")
              },
              GetCacheFileInner: function(cacheDir, data) {
                 if(data) data = data.replace(/\W/g, "")
                 return path.join(cacheDir, (data ? data : "") + ".json")
              }
          }
    return  common(name, cacherInput, moduleOptions)
  }
})("webhostingtemplates");

(function(name) {
lib[name] = function(moduleOptions) {
   var cacherInput = {
              Validate: function(in_data){
                  if(!in_data) return Promise.reject(new MError("TOKEN_MISSING"))

                  return Promise.resolve(in_data)
              },
              FetchInfo: function(data) {
                 var uri = cacherInput.isInternal() ? "/authentication/token" : "/token"

                 var payload = {token:data}
                 if(cacherInput.moduleOptions.config) {
                   Array({c:"server_name",p:"server"}, {c:"server_auth_code",p:"auth_code"}).forEach(x=>{
                     var v = cacherInput.moduleOptions.config.get(x.c)
                     if(v)
                       payload[x.p] = v
                   })
                 }

                 return cacherInput.GetAccountMapi().postAsync(cacherInput.urlPrefix()+uri,payload)
              },
              GetCacheFile: function(data) { // note we use GetCacheFile here and not GetCacheFileInner
                 return "memory/"+data // we force to use the memcache
              }
     }
    return common(name, cacherInput, moduleOptions)
  }
})("session");

function common(name, cacherInput, moduleOptions) {

          if(!moduleOptions) moduleOptions = {}

        if((moduleOptions.config)&&(moduleOptions.config.defaults)) {
            var r = {}
            r[name+"_info_directory"]="memory"
            moduleOptions.config.defaults(r)
        }

         var mapiPool
        var cacheDir
        if(moduleOptions.config)
            cacheDir = moduleOptions.config.get(name+"_info_directory")


          const cacher = require("./lib-cacher.js")


        cacherInput.isInternal = function(){
            if((moduleOptions.config) && (moduleOptions.config.get("info_accountapi_mode") == "internal"))
               return true
            var pool = cacherInput.GetAccountMapi()
            return ((pool.getApiType) && (pool.getApiType() == "internal"))
        }
        cacherInput.urlPrefix = function(){
          return cacherInput.isInternal() ? "/accountapi" : "/info/account"
        }

         if(moduleOptions.config) {
            cacherInput.cacheInfoSeconds = moduleOptions.config.get("cache_"+name+"_info_seconds")
            cacherInput.cleanupIntervalMs = moduleOptions.config.get("cleanup_"+name+"_info_ms")

         }


         cacherInput.GetAccountMapi = function(){

            if (!mapiPool) {
                if(moduleOptions.getAccountMapiObject) {
                  if(!moduleOptions.getAccountMapiObject.GetAccountMapi)
                    throw new Error("GetAccountMapi function not found")
                  mapiPool = moduleOptions.getAccountMapiObject.GetAccountMapi()
                } else {
                  const Mapi = require("MonsterClient")
                  mapiPool = Mapi.getMapiPoolCached(moduleOptions.accountServerRouter)
                }
            }

            return mapiPool
        }

         cacherInput.moduleOptions = moduleOptions

          if(!cacherInput.GetCacheFile) {

              cacherInput.GetCacheFile = function(data) {
                 if(!cacheDir) return
                 return cacherInput.GetCacheFileInner(cacheDir, data)
              }

          }

          re= cacher(cacherInput)

          re.Validate = cacherInput.Validate
          re.IsInternal = cacherInput.isInternal
          re.GetCacheFile = cacherInput.GetCacheFile

          return re

  }






})()



