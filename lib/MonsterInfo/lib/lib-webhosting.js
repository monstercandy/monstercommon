var lib = module.exports = function(moduleOptions) {

   /*
   we expect the following options in moduleOptions
   getWebhostingObject
   webhostingServerRouter with public server, account with INFO_WEBHOSTING entitlement
   config w cache_webhosting_info_seconds key
   */

    if(!moduleOptions) moduleOptions = {}


    var mapiPool // we keep a global instance of mapipool here, else it would attempt to login every freaking time some info lookup is required

	const path = require("path")

    if((moduleOptions.config)&&(moduleOptions.config.defaults)) {
        moduleOptions.config.defaults({hosting_info_directory:"memory"})
    }

	var hostingDir
  if(moduleOptions.config)
      hostingDir = moduleOptions.config.get("hosting_info_directory")

    var vali = require("MonsterValidators").ValidateJs()

    const validators = {
             "storage_id": {presence: true, isInteger:{greaterThen: 0}},
          }

    const cacher = require("./lib-cacher.js")

    var re

    var cacherInput = {
        Validate: Validate,
        FetchInfo: function(data) {
           var whLib = re.GetWebhosting();
           var apiPrefix;
           if(whLib.getApiPrefix)
             apiPrefix = whLib.getApiPrefix();

           var url = `/info/webhosting/${data.storage_id}`
           if(apiPrefix == "/api") {
               if(!data.server) data.server = "[this]";
               url = `/s/${data.server}${url}`
           }
           return whLib.getAsync(url)
        },
        GetCacheFile: function(data) {
           if(!hostingDir) return

           return path.join(hostingDir, data.storage_id+".json")
        }

    }

   if(moduleOptions.config) {
      cacherInput.cacheInfoSeconds = moduleOptions.config.get("cache_webhosting_info_seconds")
      cacherInput.cleanupIntervalMs = moduleOptions.config.get("cleanup_webhosting_info_ms")
   }

    re= cacher(cacherInput)

    re.Validate = Validate

    re.GetWebhosting = function(){
        if(moduleOptions.getWebhostingObject) {
          if(!moduleOptions.getWebhostingObject.GetWebhosting)
            throw new Error("GetWebhosting function not found")
          return moduleOptions.getWebhostingObject.GetWebhosting()
        }

        if(!mapiPool) {
           const Mapi = require("MonsterClient")
           mapiPool = Mapi.getMapiPoolCached(moduleOptions.webhostingServerRouter)
        }

        return mapiPool
  }


    return re


    function Validate(webhostingId_or_WithServer) {
         var d = webhostingId_or_WithServer
         if(typeof webhostingId_or_WithServer != "object")
             d = {"storage_id": webhostingId_or_WithServer}
         return vali.async(d, {storage_id: validators.storage_id, server: {isString: true}})
    }

}
