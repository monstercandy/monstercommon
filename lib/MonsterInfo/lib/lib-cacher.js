var lib = module.exports = function(moduleOptions){

    if(!moduleOptions) moduleOptions = {}

	const fs = require("MonsterDotq").fs()

	if(!moduleOptions.Validate) throw new Error("Validate callback must be provided")
	if(!moduleOptions.FetchInfo) throw new Error("FetchInfo callback must be provided")

    var fetchCache = {}

    var memcache

    var re = {}

    var memoryTest = new RegExp("^([^/]+)(?:/|\\\\)(.+)$")


    function PrepareCache(in_data, force_reload) {

            return Promise.resolve()
              .then(()=>{

                    var fullFn

                    if((!force_reload)&&(moduleOptions.GetCacheFile))
                      fullFn = moduleOptions.GetCacheFile(in_data)

                    if(!fullFn)
                      throw "caching is disabled"


                    var m = memoryTest.exec(fullFn)
                    if((m)&&(m[1] == "memory")) {

                       memoryKey = m[2]

                       if(!memcache) {
                           memcache = require("SimpleMemcache")({
                            cleanupOlderSec: moduleOptions.cacheInfoSeconds,
                            cleanupIntervalMs: moduleOptions.cleanupIntervalMs,
                            dontUpdateWithGet: true
                           })
                       }

                       return Promise.resolve({"type":"memory",key: memoryKey, memoryKey: memoryKey})

                    }

                    return Promise.resolve({"type":"file", key: fullFn})
              })

    }


    re.GetInfoWithoutCaching = function(in_data) {
       return re.GetInfo(in_data, true)
    }

    re.GetInfo = function(in_data, force_reload) {

      var cache
      var validatedData

      return moduleOptions.Validate(in_data)
        .then(aValidatedData=>{
           validatedData = aValidatedData

           return PrepareCache(validatedData, force_reload)

        })
        .then(aCache=>{
          cache = aCache


          if(cache.type == "memory") {
                   var v = memcache.get(cache.memoryKey)
                   if(!v) throw "not in memory cache"

                   return Promise.resolve( v )
          }


          return fs.statAsync(cache.key)
                      .then(stats=>{

                         if((moduleOptions.cacheInfoSeconds)&&(stats.mtime)) {
                            var cache_info_seconds = moduleOptions.cacheInfoSeconds
                            if(cache_info_seconds) {
                              var now = require("MonsterMoment").nowUnixtime()
                              var fmtime = stats.mtime.unixtime();

                              if(now - fmtime > cache_info_seconds)
                                 throw new "cached version is out of date"
                            }
                         }

                         return Promise.resolve()

                      })
                      .then(()=>{
                          // everything is good with the cached version

                         return fs.readFileAsync(cache.key)
                           .then(content => {
                              return Promise.resolve(JSON.parse(content))
                           })


                      })

        })
   	    .catch(ex=>{
                if(ex.message == "VALIDATION_ERROR") throw ex

  	  	 // we failed to get the cached version for any reasons, lets try to get it

                if(!cache) cache = {}

                // we have to attempt to fetch the details
                var p = fetchCache[cache.key]
                if((!cache.key)||(!p))
                  p = fetchCache[cache.key] = moduleOptions.FetchInfo(validatedData)
                  .catch(ex=>{
                    delete fetchCache[cache.key]
                    throw ex
                  })
                  .then(data=>{
                    delete fetchCache[cache.key]
                  	if((data)&&(data.result)) data = data.result // if we received it from as an http response

                      if(cache) {
                        if(cache.type == "memory") {
                           memcache.set(cache.memoryKey, data)

                        }

                        if(cache.type == "file") {
                            return fs.writeFileAsync(cache.key, JSON.stringify(data))
                              .then(()=>{
                                 return fs.chmodAsync(cache.key, 0600)
                              })
                              .then(()=>{
                                 return Promise.resolve(data)
                              })

                        }
                      }

                      return Promise.resolve(data)

                  })

              return p

    	  })



    }

    // this gonna delete the local cached version of the config object
    re.Delete = function(in_data) {

      	if(!moduleOptions.GetCacheFile) return Promise.resolve()

        return moduleOptions.Validate(in_data)
          .then(validatedData=>{

              return PrepareCache(validatedData)

          })
          .then(cache=>{

              // console.log("delete", cache)

              if(cache.type == "memory") {
                  // console.log("removing from cache")
                  memcache.del(cache.memoryKey)
                  return Promise.resolve()
              }

              if(cache.type == "file") {
                  // console.log("unlinking")
                  return fs.unlinkAsync(cache.key)
              }

          })
          .catch((ex)=>{
            console.log("delete cache entry failed", ex)
          })


    }




    return re


}
