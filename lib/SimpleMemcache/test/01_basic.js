require( "../../MonsterDependencies")(__dirname)

require("MonsterMoment")

describe('basic', function() {
    it('set', function(done) {

         var store = require("../lib/index.js")({cleanupIntervalMs: 500, cleanupOlderSec: 1})
         store.set("key", "value")
         var v = store.get("key")
         if(v != "value")
         	return done(new Error("value differs"))

         setTimeout(()=>{

            v = store.get("key")
            if(v != null)
            	return done(new Error("value was not cleaned up"))

            done()
         }, 1500)

    })


    it('set (dont update time ref with get)', function(done) {

         var store = require("../lib/index.js")({cleanupIntervalMs: 500, cleanupOlderSec: 10, dontUpdateWithGet: true})
         store.set("key", "value")
         var v = store.getRaw("key")
         if(v.data != "value")            
            return done(new Error("value differs"))
         var oldTs = v.ts

         setTimeout(()=>{

             v = store.getRaw("key")
             if(v.data != "value")            
                return done(new Error("value differs"))
             var newTs = v.ts

             if(oldTs != newTs)            
                return done(new Error("timestamps differ"))

             done()
         }, 1500)

    })

    
})
