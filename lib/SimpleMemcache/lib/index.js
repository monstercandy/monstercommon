module.exports = function(config){

    // this one is needed for nowUnixtime global function
    const moment = require("MonsterMoment")

    if(!config) config = {}
    if(typeof config.dontReturnOutOfDateData == "undefined")
        config.dontReturnOutOfDateData = true

    var db = {}

    var re = {}
    re.set = function(key, value) {
        db[key] = {ts: moment.nowUnixtime(), data: value}
    }
    re.getRaw = function(key) {
        var x = db[key]
        if(x) {
           if(!config.dontUpdateWithGet)
              x.ts = moment.nowUnixtime()
           return x
        }

    }
    re.get = function(key) {
        var x = re.getRaw(key)

        if(x) {
           if(config.dontReturnOutOfDateData)
           {
              var now = moment.nowUnixtime()
              var delOlderThen = getDelOlderThen()
              if(now - x.ts >= delOlderThen) {
                re.del(key)
                return // its too old.
              }
           }
           return x.data
        }
    }
    re.del=function(key) {
        delete db[key]
    }

    setInterval(()=>{
        var delOlderThen = getDelOlderThen()
        var n = moment.nowUnixtime()
        for(var q of Object.keys(db)) {
            var x = db[q]
            if(n - x.ts >= delOlderThen) {
              re.del(q)
            }
        }

    }, config.cleanupIntervalMs || 60000)

    return re

    function getDelOlderThen(){
        var delOlderThen = config.cleanupOlderSec || 15*60 // 15 minutes by default
        return delOlderThen
    }
}
