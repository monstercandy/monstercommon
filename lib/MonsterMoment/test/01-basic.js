
var moment = require( "../lib/moment.js")
var assert = require("chai").assert

describe('timezone_offset', function() {
    "use strict"


    it('iso8601 support', function() {

      var oTs = "2016-09-18T11:17:44+02:00"
      var m = moment.convertToTimezone(oTs, "Europe/Budapest")

      var c1 = m.toUtcDBISOString()
      var c2 = m.toISOString()
      // console.log("hre", c1, c2)
      assert.equal(c1, c2);

    })

    it('hungary from timezone name', function() {
      var offset = moment.getTimezoneOffsetForTimezone("Europe/Budapest")

      assert.ok(offset == -120 || offset == -60);

    })

    it('london from timezone name', function() {
      var offset = moment.getTimezoneOffsetForTimezone("Europe/London")

      assert.ok(offset == -60 || offset == 0);

    })

    it('utc timestamps corrected for Budapest', function() {

    	var rows = [{"a":"bla1", "ts": "2016-09-18 09:17:44"},{"a":"bla2", "ts": "2016-09-18 10:00:00"}]

      moment.ApplyTimezoneOffsetOnArrayOfHashes(rows, ["ts"], -120)

      assert.deepEqual(rows, [{"a":"bla1", "ts": "2016-09-18 11:17:44"},{"a":"bla2", "ts": "2016-09-18 12:00:00"}]);

    })

    it('utc timestamps corrected for London', function() {

      var rows = [{"a":"bla1", "ts": "2016-09-18T11:17:44+02:00"},{"a":"bla2", "ts": "2016-09-18T12:00:00+02:00"}]

      moment.ApplyTimezoneOnArrayOfHashes(rows, ["ts"], "Europe/London")

      assert.deepEqual(rows, [{"a":"bla1", "ts": "2016-09-18 10:17:44"},{"a":"bla2", "ts": "2016-09-18 11:00:00"}]);

    })


    it('Budapest to Budapest should not change (well at least in winter)', function() {

      var oTs = "2016-09-18T11:17:44+02:00"
      var cTs = moment.convertToTimezone(oTs, "Europe/Budapest").toISOStringWithTimeZone()
      assert.equal(cTs, oTs);

    })

    it('Budapest to Budapest should not change (well at least in winter)', function() {

      var oTs = "2016-09-18T11:17:44+02:00"
      var eTs = "2016-09-18T09:17:44.000Z"
      var cTs = moment.convertToTimezone(oTs, "Europe/Budapest").toISOString()
      assert.equal(cTs, eTs);

    })

})
