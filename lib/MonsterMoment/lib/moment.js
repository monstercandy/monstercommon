/*

var bud    = tzMoment.tz("2016-09-29 07:54", "Europe/Budapest");
var lon = bud.clone().tz("Europe/London");
console.log("LONDON",lon.format()) // outputs: LONDON 2016-09-29T06:54:00+01:00

var bud2    = tzMoment("2016-09-29T07:55:01+02:00");
var lon2 = bud2.clone().tz("Europe/London");
console.log("LONDON",lon2.format()) // outputs: LONDON 2016-09-29T06:55:01+01:00

*/

var originalMoment = require("moment")
var thisMoment = module.exports = function() {
	  var re = originalMoment.apply(null, arguments)

    applyExtraFeatures(re)

    return re
 }

 function applyExtraFeatures(re) {
    re.toRaw = function(){
      return re.format("YYYYMMDDHHmmss")
    }

    re.toClassic = function(){
      return re.format("YYYY-MM-DD HH:mm:ss")
    }

    re.toISOStringWithTimeZone = function(){
      return re.format()
    }

    re.toUtcDBISOString = function(){
      // see the docs, they are the very same
      return re.toISOString()
    }
 }

thisMoment.base = originalMoment;
thisMoment.getTimezoneOffsetForTimezone = function(targetTimezone, utcTimestampStr){
  var tzMoment = require("moment-timezone")

  if((!utcTimestampStr)||(!utcTimestampStr.match(/(Z|\+\d\d:\d\d)$/))) utcTimestampStr = thisMoment()

  return tzMoment.tz.zone(targetTimezone).parse(utcTimestampStr)
}


thisMoment.convertToTimezone = function(timestampStrWithOffsetSuffix, targetTimezone){
  var tzMoment = require("moment-timezone")
//Classic
//console.log("parsing:", timestampStrWithOffsetSuffix)
  var m = tzMoment(timestampStrWithOffsetSuffix).clone().tz(targetTimezone)
  applyExtraFeatures(m)
  return m
}

thisMoment.utc = originalMoment.utc
thisMoment.ApplyTimezoneOnArrayOfHashes = function(rows, fieldNames, timezone) {
  if(!timezone) return rows

//2016-09-18 09:17:44
  rows.forEach(function(row){
      fieldNames.forEach(function(field){
         if(!row[field]) return;

         var m = thisMoment.convertToTimezone(row[field], timezone)
         row[field] = m.toClassic()
         // console.log(row)
      })
  })

}
thisMoment.ApplyTimezoneOffsetOnArrayOfHashes = function(rows, fieldNames, timezoneOffset) {
  if(!timezoneOffset) return rows

//2016-09-18 09:17:44
  rows.forEach(function(row){
      fieldNames.forEach(function(field){
         if(!row[field]) return;

         var d = thisMoment(row[field])
         d.subtract(timezoneOffset, 'minutes')
         row[field] = d.toClassic()
         // console.log(row)
      })
  })

}

Date.prototype.unixtime = function(){
  return Math.round( this.getTime() / 1000 );
}

thisMoment.nowRaw = function(){
  return module.exports().toRaw()
}

thisMoment.now = function(){
	return module.exports().toISOString()
}
thisMoment.nowUnixtime = function(){
  return new Date().unixtime()
}
