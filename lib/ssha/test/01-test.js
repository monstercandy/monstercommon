
var sshaLib = require( "../ssha.js")
var assert = require("chai").assert

describe('tests', function() {    

  const secret = "secret"

  var prev 
  it("simple call should be sha1", function(){
     prev = sshaLib.ssha.create(secret)
	 // console.log("FOO", prev, prev.length)
	 assert.equal(prev.length, 54)
   assert.ok(prev.match(/^{SSHA}/))
  })
  
  it("and verify should match", function(){
	 // console.log("FOO", prev, prev.length)
	 assert.ok(sshaLib.ssha.verify(secret, prev))
  })
  
  it("second call with the same secret should return something else", function(){
     var newHash = sshaLib.ssha.create(secret)
	 
	   assert.notEqual(prev, newHash)	 
  })
  
  it("specifying sha512 should work and return long stuff", function(){
     prev = sshaLib.ssha512.create(secret)
	   assert.equal(prev.length, 117)
     assert.ok(prev.match(/^{SSHA512}/))
  })  
  
  it("and verify should match with sha512 as well", function(){
     assert.ok(sshaLib.ssha512.verify(secret, prev))
  })  
})
