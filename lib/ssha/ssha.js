// Backported from Node.js 0.8 for Node 0.6 support.
bufferConcat = function(list, length) {
  if (!Array.isArray(list)) {
    throw new Error('Usage: Buffer.concat(list, [length])');
  }

  if (list.length === 0) {
    return new Buffer(0);
  } else if (list.length === 1) {
    return list[0];
  }

  if (typeof length !== 'number') {
    length = 0;
    for (var i = 0; i < list.length; i++) {
      var buf = list[i];
      length += buf.length;
    }
  }

  var buffer = new Buffer(length);
  var pos = 0;
  for (var i = 0; i < list.length; i++) {
    var buf = list[i];
    buf.copy(buffer, pos);
    pos += buf.length;
  }
  return buffer;
};


var crypto = require("crypto");

const globalRandomLengthBytes = 16

module.exports = {}

Array(
{
  name: "ssha",
  prefix: "SSHA",
  algo: "sha1",
  length: 160
},
{
  name: "ssha256",
  prefix: "SSHA256",
  algo: "sha256",
  length: 256
},
{
  name: "ssha512",
  prefix: "SSHA512",
  algo: "sha512",
  length: 512
}
).forEach(stuff=>{

   var n = module.exports[stuff.name] = {}
   

	/**
	* @param {String} secret string
	* @param {Buffer} Predefined salt buffer (optional)
	* @return {String} salted string hash
	*/
	n.create = function(secret, salt) {
	  var buf, digest, hash;
	  if (!salt) {
		salt = crypto.randomBytes(stuff.randomLengthBytes || globalRandomLengthBytes);
	  }
	  secret = new Buffer(secret);
	  hash = crypto.createHash(stuff.algo);
	  hash.update(secret);
	  hash.update(salt);
	  digest = new Buffer(hash.digest("base64"), "base64");
	  buf = bufferConcat([digest, salt]);
	  return "{"+stuff.prefix+"}" + buf.toString("base64");
	};


	/**
	* @param {String} secret string
	* @param {String} salted string hash
	* @return {Boolean}
	*/
	n.verify = function(secret, sshaHash) {
	  var base64, buf, salt;
	  base64 = sshaHash.slice(stuff.prefix.length+2);
	  buf = new Buffer(base64, "base64");
	  salt = buf.slice(stuff.length/8);
	  return n.create(secret, salt) === sshaHash;
	};
   
})
