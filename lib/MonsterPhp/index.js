var lib = module.exports = {

   php_base_port_offsets: {
      "test": 11000,
      "5.2":  12000,
      "5.3":  13000,
      "5.4":  14000,
      "5.5":  15000,
      "5.6":  16000,
      "7.0":  17000,
      "7.1":  18000,
      "7.2":  19000,
      "7.3":  20000,
   },

  phpport: function(phpversion, storage_id, model) {

      if(!phpversion) return
      if(!storage_id) return

      if(model == "multi_ip") 
      {
         var ip = "127.0.0."+phpversion.replace(".", "");
         return {
            host: ip,
            port: storage_id
         };
      }
      else // falling back to legacy mode
      {
        var base_offset = lib.php_base_port_offsets[phpversion];
        if(!base_offset) return

        var th_offset = storage_id - 16386;
        return {
          host: "127.0.0.1",
          port: base_offset + th_offset,
        }
      }

  }

}
