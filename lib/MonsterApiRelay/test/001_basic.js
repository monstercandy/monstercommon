require( "../../MonsterDependencies")(__dirname)

var apilib = require( "../lib/index.js")

var apiparams = {
    scheme: "https",
    host: "127.0.0.1",
    auth_token: "12345",
}
var pingExpectation = {
    "method": "GET",
    "url": "https://127.0.0.1/ping",
    "returnValue": {"result":"pong"},
    "header": "12345"
}

var assert = require("chai").assert

describe('relay', function() {
    it('api call', function(done) {

         var api = new apilib(apiparams)
         api.requestLib = mockedRequestLib(
             pingExpectation
         )


         api.doRequest("GET", "/ping", "", function(err, res){
            assert.equal(res, "pong")
            done(err)  
         })
    })


})


function mockedRequestLib(expect, mockReadyCallback) {

  return function(params) {
            assert.equal(params.method, expect.method)
            assert.equal(params.url, expect.url)
            if(expect.body)
              assert.equal(params.body, JSON.stringify(expect.body))

            if((expect.header)&&((!params.headers)||(!params.headers["MonsterToken"])||(params.headers["MonsterToken"]!=expect.header)))
                assert.fail("TOKEN_NOT_SENT")

            var re = {}
            re.on = function(event, requestCallback) {

                if(event == "response"){
                    var response = {}
                    response.on = function(event, responseCallback){
                        if(event == "data")
                            responseCallback(JSON.stringify(expect.returnValue))
                        if(event == "end") {
                            if(mockReadyCallback)
                                mockReadyCallback()

                            responseCallback(null)
                        }
                        return response
                    }
                    requestCallback(response)
                }
                return re
            }
            return re
  }
}
