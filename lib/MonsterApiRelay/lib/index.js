
var MError = require("MonsterError")

const util = require("util")

var lib = module.exports = function(serviceOptions, config) {
  var host, port, auth_token, scheme, timeout, maxSockets

  if(!config) config = {}

  if(typeof(serviceOptions) !== "object")
      throw new MError("INVALID_PARAMETER")

  host = serviceOptions["ip"] || serviceOptions["host"]
  auth_token = serviceOptions["auth_token"]
  scheme = serviceOptions["scheme"] || "http"
  port = serviceOptions["port"]
  timeout = serviceOptions["timeout"] || config["timeout"] || (config.get ? config.get("timeout") : null)

/*
  maxSockets = Infinity
  if((config.get)&&(config.get("maxSockets")))
    maxSockets =config.get("maxSockets")
*/



  function getFullUrl(uri){
      var x =`${scheme}://${host}`
      if(port)
          x += `:${port}`
      x += uri
      return x
  }


  var re = {}

  re.getApiType = lib.getApiType

  re.SetAuthToken = function(token){
    auth_token = token
  }

  re.requestLib = require("RequestCommon")

  re.doRequestStream = function(method, uri, payload, readyCallback) {
      var payloadStr = payload
      if((payload)&&(!util.isString(payload)))
        payloadStr = JSON.stringify(payload)

      var fullUrl = getFullUrl(uri)

      var params = {
          method: method,
          url: fullUrl,
          // we dont do this anymore, as https globalAgent CA would be discarded then by request lib
          // pool: {maxSockets: maxSockets},
          body: payloadStr,
          headers: {}
      }
      if(timeout) { // msecs issue
          params.timeout = timeout;
      }
      if(auth_token != '-')
        params.headers.MonsterToken = auth_token;

      if(re.extraHeaders)
         Object.keys(re.extraHeaders).forEach((k)=>{
            params.headers[k] = re.extraHeaders[k]
         })


      var req = re.requestLib(params)
      .on('error', function(err) {
          return readyCallback(new MError("NETWORK_ERROR", null, err))
      })


      return req

  }


  re.doRequest = function(method, uri, payload, readyCallback) {

      var raw = serviceOptions.raw || false
      if(method == "RAWGET") {
           raw = true
           method = "GET"
      }

      var str = ''
      var req = re.doRequestStream(method, uri, payload, readyCallback)
      req.on('response', function(response) {
          response.on('data', function(chunk) {
                str += chunk;
          })
          response.on('end', function () {

              // console.log("ProxyTapi str result:", str)
              var j
              try {

                 if(raw)
                    return readyCallback(null, str, response)

                 j = JSON.parse(str)

                 if((j.result)&&(j.result.error)) {
                    return readyCallback(new MError(j.result.error.message, j.result.error.params), j["result"], response);
                 }

                 // everything is fine
                 return readyCallback(null, j["result"], response)

              }catch(ex){
                 // console.log("exception was throwm", ex)
                 return readyCallback(new MError("INTERNAL_ERROR", null, ex))
              }

          })
      })

      return req
  }


  return re
}

lib.getApiType = function() {
    return "relay"
}
