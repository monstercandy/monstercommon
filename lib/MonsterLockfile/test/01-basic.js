require( "../../MonsterDependencies")(__dirname)


    var lockfile =require( "../index.js")

    const assert = require("chai").assert

    const fs = require("fs")

  describe('basic tests', function() {
    "use strict"


    it('basic functionality with promise', function() {

        var fullFn
        return lockfile.logic({lockfile:"foo.txt", callback: function(lockfilePath){
            assert.ok(lockfilePath)
            fullFn = lockfilePath
            shouldExist(fullFn)
            return Promise.resolve("foo")
        }})
        .then(x=>{
            assert.equal(x, "foo", "should propagate the return value")

            return shouldNotExist(fullFn)
        })
    })

    it('basic functionality without promise', function() {

        var fullFn
        return lockfile.logic({lockfile:"foo.txt", callback: function(lockfilePath){
            assert.ok(lockfilePath)
            fullFn = lockfilePath
            shouldExist(fullFn)
            return "hello"
        }})
        .then(x=>{
            assert.equal(x, "hello", "should propagate the return value")

            return shouldNotExist(fullFn)
        })
    })

    it('basic functionality without returning anything', function() {

        var fullFn
        return lockfile.logic({lockfile:"foo.txt", callback: function(lockfilePath){
            assert.ok(lockfilePath)
            fullFn = lockfilePath
            shouldExist(fullFn)
            return
        }})
        .then(x=>{
            assert.isUndefined(x)

            return shouldNotExist(fullFn)
        })
    })

    it('should work well with exceptions in promise', function() {

        var fullFn
        return lockfile.logic({lockfile:"foo.txt", callback: function(lockfilePath){
            fullFn = lockfilePath
            shouldExist(fullFn)
            return Promise.reject(new Error("crap"))
        }})
        .catch(x=>{
            assert.propertyVal(x, "message", "crap")
        })
        .then(()=>{
            return shouldNotExist(fullFn)
        })
    })

    it('should work well with exceptions', function() {

        var fullFn
        return lockfile.logic({lockfile:"foo.txt", callback: function(lockfilePath){
            fullFn = lockfilePath
            shouldExist(fullFn)
            throw new Error("crap")
        }})
        .catch(x=>{
            assert.propertyVal(x, "message", "crap")
        })
        .then(()=>{
            return shouldNotExist(fullFn)
        })
    })
})

describe('locking tests', function() {

    it('locking test', function(done) {

        this.timeout(10000)

        var fn = "foobar-locking.txt"
        var ready1 = false
        var ready2 = false

        lockfile.logic({lockfile:fn, params:{wait: 2000}, callback: function(lockfilePath){
            return new Promise((resolve,reject)=>{
                setTimeout(function(){
                    ready1 = true
                    resolve()
                }, 1500)
            })
        }})
        .then(checkBothDone)

        lockfile.logic({lockfile:fn, params:{wait: 2000}, callback: function(lockfilePath){
            return new Promise((resolve,reject)=>{
                setTimeout(function(){
                    ready2 = true
                    resolve()
                }, 1500)
            })
        }})
        .then(checkBothDone)


        function checkBothDone(){
            if(!ready1) return
            if(!ready2) return
                // both good!
            done();
        }

    })

    it('timeout test', function(done) {

        this.timeout(10000)

        var fn = "foobar-timeout.txt"
        var ready1 = false
        var ready2called = false
        var ready2ex = false

        lockfile.logic({lockfile:fn, callback: function(lockfilePath){
            return new Promise((resolve,reject)=>{
                setTimeout(function(){
                    ready1 = true
                    resolve()
                }, 1500)
            })
        }})
        .then(checkBothDone)

        setTimeout(function(){
            lockfile.logic({lockfile:fn, params:{wait: 1000}, callback: function(lockfilePath){
                ready2called = true
            }})
            .catch(ex=>{

                assert.propertyVal(ex, "code", "EEXIST")
                ready2ex = true
            })
            .then(checkBothDone)
        }, 100)


        function checkBothDone(){
            if(ready2called) throw new Error("ready2 should have never been called")
            if(!ready1) return
            if(!ready2ex) return
            // both good!
            done()
        }

    })


})



    function shouldNotExist(fn) {
        try{
            fs.accessSync(fn)
            throw new Error("should not exist")
        }catch(ex){
            assert.equal(ex.code,"ENOENT")
        }
    }
    function shouldExist(fn) {
        fs.accessSync(fn)
    }

