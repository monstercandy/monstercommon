(function(){
  const dotq = require("MonsterDotq")
  const lockFile = require('lockfile')
  dotq(lockFile)

  const path = require("path")
  const os = require("os")


  lockFile.logic = function(mainParams) {
  	 if(!mainParams) throw new Error("no params")
  	 if(typeof mainParams != "object") throw new Error("invalid params")

  	 var lockfile = mainParams.lockfile
  	 if(!lockfile) throw new Error("mandatory param lockfile missing")
  	 if(lockfile.indexOf("/") != 0)
  	 	lockfile = path.join(os.tmpdir(), lockfile)

  	 if(!mainParams.callback) throw new Error("mandatory param callback missing")

     if(!mainParams.params) mainParams.params = {wait: 5000};

  	 return lockFile.lockAsync(lockfile, mainParams.params)
  	   .then(()=>{
  	   	   var thereWasEx
  	   	   var prom
  	   	   var finalResult
  	   	   try{
  	   	   	 prom = mainParams.callback(lockfile)
  	   	   	 if((!prom)||(!prom.then))
  	   	   	 	prom = Promise.resolve(prom)
  	   	   }catch(ex){
  	   	   	  thereWasEx = ex
  	   	   	  prom = Promise.resolve()
  	   	   }
  	   	   return prom
  	   	     .then(re=>{
  	   	     	finalResult = re
  	   	     	return Promise.resolve()
  	   	     })
  	   	     .catch(ex=>{
  	   	     	thereWasEx = ex
  	   	     })
  	   	     .then(()=>{
  	   	     	return lockFile.unlockAsync(lockfile)
  	   	     })
  	   	     .then(()=>{
  	   	     	if(thereWasEx)
  	   	     		throw thereWasEx
  	   	     	return Promise.resolve(finalResult)
  	   	     })
  	   })
  }

  module.exports = lockFile
})()

