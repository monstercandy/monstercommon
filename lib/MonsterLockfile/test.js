    require( "../MonsterDependencies")(__dirname)

var lockfile = require("./index.js");
console.log("Before acquiring the lock");
return lockfile.logic({lockfile: "foobar.hu", params: {wait: 5000}, callback: function(lockfilePath){
  console.log("Locked!", lockfilePath);
  return new Promise(function(resolve,reject){
     setTimeout(function(){
        console.log("releasing");
        resolve();
     }, 10000);
  });
}});
