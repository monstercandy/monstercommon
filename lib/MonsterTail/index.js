module.exports = require('tail');

module.exports.Tail.robust = function(filename, options){
	if(!options) options = {};
	options.follow = false;
	var re = {};
	var theCallback = options.callback;
	var tail;
	re.on= function(type, callback) {
		if(type != "line") throw new Error("unsupported event"+type);
		// console.log("callback saved", callback)
		theCallback = callback;
	}
	re.unwatch = function(){
		if(tail)
		  return tail.unwatch();
	}
	setupTail();
	return re;

	function setupTail(){
		if(tail) {
			tail.unwatch();
			tail = null;
		}

		// console.log("setting up tail", filename, options);
		try{
  		   tail = new module.exports.Tail(filename, options)

		} catch(err){
			return onError(err);
		}
		tail.on("line", function(l){
			// console.log("line fired", l)
			if(theCallback)
				 theCallback(l);
		})
		tail.on("error", onError);

		// console.log("tail configured");
	}

	function onError(err) {
	   if((typeof err == "string")&&(err.indexOf("File not available."))) {
	   	  // file was renamed/deleted/whatever

	   	  setTimeout(setupTail, options.deletedDelayMs || 1000);
	   	  return;
	   }

       if(!options.silent)
          console.error("Error while reading tail", options, err);
       setTimeout(setupTail, options.errorDelayMs || 60000);
	}

}
