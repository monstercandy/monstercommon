const assert = require("assert");

describe("basic", function(){
	const fs = require("fs");
	const Tail = require("../index.js").Tail;



	it("tail should work even when the file did not exist originally", function(done){

		this.timeout(5000);

		const test = "tmp/test1.txt";
		const content = "foobar\n";

        try{
        	fs.unlinkSync(test);
        }catch(x){
        	console.log("error while unlink", x)
        }

    	var tail = new Tail.robust(test, {errorDelayMs: 500})
	  	   .on("line", function(l){
	  	   	  console.log("got final line", l);
	  	   	  assert.equal(l, content.trim());
	  	   	  done();
	  	   });

	  	 setTimeout(function(){
	  	 	console.log("creating empty file");
	  	 	fs.writeFileSync(test, "");
	  	 	setTimeout(function(){
	  	 		console.log("writing file");
	  	 		fs.appendFileSync(test, content);

	  	 	}, 1000)
	  	 }, 1000)


	});


	it("tail should work when the file was removed while watching", function(done){

		this.timeout(5000);

		const test = "tmp/test2.txt";
		const initialContent = "foobar\n";
		const contentAfter = "after\n";

		fs.writeFileSync(test, initialContent);

    	var tail = new Tail.robust(test, {errorDelayMs: 500})
	  	   .on("line", function(l){
	  	   	  console.log("got final line", l);
	  	   	  assert.equal(l, contentAfter.trim());
	  	   	  done();
	  	   });

	  	 setTimeout(function(){
	  	 	console.log("rewriting file with new content");
	  	 	fs.unlinkSync(test);
   	 	    fs.writeFileSync(test, contentAfter);
	  	 	setTimeout(function(){
		  	 	fs.appendFileSync(test, contentAfter);
		  	 	console.log("content saved");

	  	 	}, 1500);
	  	 }, 1000)


	});
})