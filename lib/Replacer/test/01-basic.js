
var replacerLib = require( "../index.js")
var assert = require("chai").assert

describe('tests', function() {    

	const replacement = {"a": "!a!", "b": "!b!"}

    it('undef', function() {
	      var re = replacerLib(undefined, replacement)
		  assert.deepEqual(re, undefined)
    })

    it('undef replacement', function() {
	      var re = replacerLib("foobar")
		  assert.deepEqual(re, "foobar")
    })

	
    it('numbers', function() {
	      var re = replacerLib(123, replacement)
		  assert.deepEqual(re, '123')
    })

    it('strings', function() {
	      var re = replacerLib("x[a]z", replacement)
		  assert.deepEqual(re, "x!a!z")
    })

    it('strings [p[wh_id]]', function() {
	      var re = replacerLib("[p[wh_id]]", {wh_id: 1234})
		  assert.deepEqual(re, "[p1234]")
    })
    

    it('multi (//g) refs in the same string', function() {
	      var re = replacerLib("x[a]y[a]z", replacement)
		  assert.deepEqual(re, "x!a!y!a!z")
    })

    it('multiple relacement objects', function() {
	      var re = replacerLib("some [r1] foo [r2] bar", [{r1:"x1"},{r2:"x2"}])
		  assert.deepEqual(re, "some x1 foo x2 bar")
    })

    it('multiple relacement objects with undef', function() {
	      var re = replacerLib("some [r1] foo [r2] bar", [{r1:"x1"},undefined,{r2:"x2"}])
		  assert.deepEqual(re, "some x1 foo x2 bar")
    })

    it('multiple relacement objects with null', function() {
	      var re = replacerLib("some [r1] foo [r2] bar", [{r1:"x1"},null,{r2:"x2"}])
		  assert.deepEqual(re, "some x1 foo x2 bar")
    })

    it('invalid reference should be left untouched', function() {
	      var re = replacerLib("x[xxx]z", replacement)
		  assert.deepEqual(re, "x[xxx]z")
    })

    it('arrays', function() {
	      var re = replacerLib(["x [a] y","value2", "[b]"], replacement)
		  assert.deepEqual(re, ["x !a! y", "value2", "!b!"])
    })

    it('hashes', function() {
	      var re = replacerLib({"x":"[a] y","y":"value2"}, replacement)
		  assert.deepEqual(re, {"x":"!a! y","y":"value2"})
    })

    it('complex', function() {
	      var re = replacerLib({"x":"[a] y","y":["x [b]"]}, replacement)
		  assert.deepEqual(re, {"x":"!a! y","y":["x !b!"]})
    })

    it('replacement hash is multi layer', function() {
	      var re = replacerLib({"x":"[a] y","y":["x [b.a]"]}, {"a": "!a!", "b":  {"a":"!x!a!b!x!"}})
		  assert.deepEqual(re, {"x":"!a! y","y":["x !x!a!b!x!"]})
    })

    it('real life example (low level)', function() {
	      var re = replacerLib.ReplaceString("alpine-3.7-fileman:[alpine-37-fileman]", [{"alpine-37-fileman": "2018-08-20--1"}]);
		  assert.equal(re, "alpine-3.7-fileman:2018-08-20--1")
    })

    it('real life example', function() {
	      var re = replacerLib({args:["alpine-3.7-fileman:[alpine-37-fileman]"]}, {"alpine-37-fileman": "2018-08-20--1"});
		  assert.deepEqual(re, {args:["alpine-3.7-fileman:2018-08-20--1"]})
    })

})
