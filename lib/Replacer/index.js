var lib = module.exports = function(src, replacement) {
    "use strict"

  if(!Array.isArray(replacement)) replacement = [replacement]
	
	if(typeof replacement != "object")
	  return src

	if(Array.isArray(src))
	  return lib.ReplaceArray(src, replacement)

	var t = typeof src 
	if(t == "undefined")
	  return
	
	if(t == "object")
	  return lib.ReplaceObject(src, replacement)
	 
	if(t == "string")
	  return lib.ReplaceString(src, replacement)
	  
	if(t == "number")
	  return lib.ReplaceString(""+src, replacement)
	  
	return src
}

lib.ReplaceArray = function(src, replacement) {
  var re = []
  src.forEach(asrc => {
    re.push(lib(asrc, replacement))
  })
  return re
}


lib.ReplaceObject = function(src, replacement) {
  var re = {}
  Object.keys(src).forEach(akey => {
    re[akey] = lib(src[akey], replacement)
  })
  return re
}



lib.ReplaceString = function(src, replacement) {
  return src.replace(/\[([^\]\[]+)\]/g, function(match, p1) {
     // console.log("test", match, p1)
     var re = "["+p1+"]"
     replacement.some(function(r){
       var v = lib.deepFind(r, p1)
       if(v !== undefined) {
          re = v
          return true
       }
     })
     return re
  })

}

lib.deepFind = function(obj, path) {
  var paths = path.split('.')
    , current = obj
    , i;
  var t = typeof current;
  if((t == "undefined")||((t == "object")&&(!current))) return;

  for (i = 0; i < paths.length; ++i) {
    if (current[paths[i]] == undefined) {
      return undefined;
    } else {
      current = current[paths[i]];
    }
  }
  return current;
}
