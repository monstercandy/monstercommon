
var MError = require("MonsterError")

const oldHmacAuth = false;

const util = require("util")

var lib = module.exports = function(serviceOptions, config) {
  var host, port, api_secret, scheme, timeout, maxSockets

  if(!config) config = {}

  if(typeof(serviceOptions) !== "object")
      throw new MError("INVALID_PARAMETER")

  host = serviceOptions["ip"] || serviceOptions["host"]
  port = serviceOptions["port"]
  api_secret = serviceOptions["api_secret"]
  scheme = serviceOptions["scheme"] || "http"
  timeout = serviceOptions["timeout"] ||  config["timeout"] ||  (config.get ? config.get("timeout") : null)

/*
  maxSockets = Infinity
  if((config.get)&&(config.get("maxSockets")))
    maxSockets =config.get("maxSockets")
*/

  if(!api_secret)
      throw new MError("API_SECRET_MISSING")

  function getFullUrl(uri){
      var x =`${scheme}://${host}`
      if(port)
          x += `:${port}`
      x += uri
      return x
  }



  var re = {}

  re.getApiType = lib.getApiType


  re.requestLib = require("RequestCommon")


  re.calculateSignature = function(ts, path, payload) {

    const hmac = require("crypto").createHmac('sha256', api_secret);

    hmac.update(ts+ " "+ path +" ");
    hmac.update(util.isString(payload) ? payload : JSON.stringify(payload) );
    return  hmac.digest('hex');
  }


  re.doRequestStream = function(method, uri, payload, readyCallback) {

      var payloadStr = payload
      if((payload)&&(!util.isString(payload)))
        payloadStr = JSON.stringify(payload)

      var fullPath = uri
      var ts = re.MonsterTime || new Date().unixtime()
      fullPath+= "?ts="+ts;
      if(oldHmacAuth) {
         fullPath+="&sig="+re.calculateSignature(ts, uri, payloadStr)        
      }

      var fullUrl = getFullUrl(fullPath)


      var requestParams = {
          method: method,
          url: fullUrl,
          // note: we do not do maxSockets here as request lib would
          //pool: {maxSockets: maxSockets},
          body: payloadStr,
          headers: oldHmacAuth ? {} : { "X-Mc-Secret": api_secret }
      }
	  if(timeout) { // note: if timeout is null, you will get: The "msecs" argument must be of type number. Received type object
          requestParams.timeout = timeout;
      }


      if(re.extraHeaders)
         Object.keys(re.extraHeaders).forEach((k)=>{
            requestParams.headers[k] = re.extraHeaders[k]
         })


      var req = re.requestLib(requestParams)
      .on('error', function(err) {
          return readyCallback(new MError("NETWORK_ERROR", null, err))
      })

      return req

  }



  re.doRequest = function(method, uri, payload, readyCallback) {

      var str = ''
      var req = re.doRequestStream(method, uri, payload, readyCallback)

      req.on('response', function(response) {
          response.on('data', function(chunk) {
                str += chunk;
          })
          response.on('end', function () {

               // console.log("ProxyTapi str result:", str)
               var j
               try {

                   j = JSON.parse(str)
               }catch(ex){
                  console.error(ex,  host, port, str)
                  return readyCallback(new MError("INTERNAL_ERROR", null, ex))
               }


               // signature verification here
               if(oldHmacAuth)
               {
                 var ts = new Date().unixtime()
                 if(!j["ts"])
                    return readyCallback(new MError("API_RESPONSE_MISSING_TS"))
                 if(j["ts"] < ts - 30)
                    return readyCallback(new MError("API_RESPONSE_TOO_OLD"))                
               }

               var payload = util.isString(j["result"]) ? j["result"] : JSON.stringify(j["result"])

               if(oldHmacAuth) {
                 var sig = re.calculateSignature(j["ts"], uri, payload)
                 if(sig != j['sig'])
                    return readyCallback(new MError("API_RESPONSE_INVALID_SIGNATURE"));                
               }

               if(j.result.error) {
                  console.error("Error during internal API call", method, uri, j.result.error);
                  return readyCallback(new MError(j.result.error.message, j.result.error.params), j["result"], response);
               }


               // everything is fine
               return readyCallback(null, j["result"], response)


          })
      })

      return req

  }



  return re
}

lib.getApiType = function() {
    return "internal"
}
